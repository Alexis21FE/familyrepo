    @extends('layouts.app')
    @section('content')
    @php
    $x=0;
    $fecha_ini=date('Y-m-1');
    @endphp

    <div class="section">
        {{Form::open(['route'=>'reporte.diario','method'=>'post','role'=>'form','id'=>'calculaReporte'])}}
        <center>
            
        <p style="font-family: 'Bree Serif', serif; font-size: 2em; margin: 0 !important;">Corte Diaro</p>
        </center>
            <div class="row" style="margin:auto;">
        
        <div class="col">
{{Form::label('fecha_ini','Fecha Inicial')}}
    {{Form::date('fecha_ini','0000-00-00',['value'=>$fecha_ini,'class'=>'form-control','type'=>'date','required'=>'required','onChange'=>'calculaReporte()'])}}
        </div>
        <div class="col"></div>
        <div class="col">
            {{Form::label('fecha_fin','Fecha final')}}
    {{Form::date('fecha_fin',date('Y-m-d'),['class'=>'form-control','type'=>'date','required'=>'required','onChange'=>'calculaReporte()'])}}
            
        </div>
        <div class="col"></div>
    </div>
    {{Form::close()}}
    <hr>
 <div id="response">
     
 </div>
</div>
    <script type="text/javascript">
    	
        $("#exportButton").click(function(){

        $("#table2excel").table2excel({

    // exclude CSS class

    exclude: ".noExl",

   name: "Worksheet Name",

    filename: "SomeFile.xls" //do not include extension

  });

});
        function calculaReporte(){
            var form=$("#calculaReporte").serialize();
            $.ajax({
                url:"{{route('reporte.diario')}}",
                data:form,
                datatype:"json",
                type:"post",
                success:function(response){
                    $("#bar").css("width",100+"%");
                    $("#response").empty();
                    $("#response").html(response);
$("#gif").remove();
                },
                error:function(){}
            })
        }

    </script>
    @endsection