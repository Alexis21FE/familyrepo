    @extends('layouts.app')
    @section('content')
    @php
    $x=0;
    if(isset($isResponse)){
        $consultor_id=$consultor;
        $fecha_ini=$inicio;
      
    }else{
        $consultor_id=null;
        $fecha_ini=null;
    }
    @endphp

<center>
            
        <p style="font-family: 'Bree Serif', serif; font-size: 2em; margin: 0 !important;">Detalle por Consultor</p>
        </center>
    <div class="section">
        {{Form::open(['route'=>'reporte.calculaConsultor','method'=>'post','role'=>'form','id'=>'calculaReporte'])}}
        <div class="row" style="margin:auto;">
            <div class="col"></div>
            <div class="col">
                {{Form::label('fecha_ini','Fecha Inicial')}}
                {{Form::date('fecha_ini',$fecha_ini,['class'=>'form-control','type'=>'date','required'=>'required','id'=>'fecha_ini','onChange'=>'calculaReporte()'])}}
            </div>
            
            <div class="col">
                {{Form::label('fecha_fin','Fecha final')}}
                {{Form::date('fecha_fin',date('Y-m-d'),['class'=>'form-control','type'=>'date','required'=>'required','onChange'=>'calculaReporte()'])}}

            </div>
            <div class="col">{{Form::label('consultor','Consultor')}}
                {{Form::select('consultor',$consultores->pluck('nombrecompleto','id'),$consultor_id,['class'=>'form-control','id'=>'consultor_select','placeholder'=>'Selecciona un consultor','onChange'=>'calculaReporte()'])}}</div>
            <div class="col"></div>
        </div>
        {{Form::close()}}
        <hr>
        <div id=response>
        @if(isset($isResponse))
        
        @php
        $suma_ingreso_consultor=0;
        $suma_ingreso_family=0;
        $suma_costo_final=0;
        $suma_costo_inicial=0;
        $suma_ingreso_consultor_ind=0;
        $suma_ingreso_family_ind=0;
        $suma_costo_final_ind=0;
        $suma_costo_inicial_ind=0;
        $suma_becas=array();
        $suma_becas_=array();
        $consul_ant=null;
        @endphp
        <table class="table" id="table2excel" style="text-align: center;">
          <thead class="thead-dark">
           <tr>
            <th scope="col">Cita Pagada</th>
            <th scope="col">Fecha cita</th>
            <th scope="col">Fecha Pago</th>
            <th scope="col">Consultor</th>
            <th scope="col">Paciente</th>
                <th scope="col">Colegio</th>
            <th scope="col">Ingreso Consultor</th>
            <th scope="col">Ingreso Family</th>
            <th scope="col">Costo Inicial</th>
            <th scope="col">Costo Final</th>

            @foreach($becas as $beca)
            @php 
            
            $suma_becas[$beca->descripcion]=0;
            $suma_becas_[$beca->descripcion]=0;

            @endphp
            @if($beca->monto_porcentaje!='monto')
            <th scope="col" colspan="1">{{$beca->descripcion}}</th>
            <th scope="col" colspan="1">$</th>
            @else
            <th scope="col" colspan="2">{{$beca->descripcion}}</th>
            @endif
            @endforeach
        </tr>
    </thead>
    <tbody>
@if($citas_pagadas->count()==0)
    <tr><td colspan="5"><center><h1>Hay datos</h1></center> </td></tr>
    @else
       @foreach($citas_pagadas as $cita)
       @if($consul_ant!=$cita->consultor_id)
       @if($consul_ant!=null)

       <tr style="background: #2aaad8">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <th>${{$suma_ingreso_consultor_ind}}</th>
        <th>${{$suma_ingreso_family_ind}}</th>
        <th>${{$suma_costo_final_ind}}</th>
        <th>${{$suma_costo_inicial_ind}}</th>
         @foreach($becas as $beca)
        @if($beca->monto_porcentaje!='monto')
        <th scope="col" colspan="1"></th>
        <th scope="col" char="" colspan="1">{{"$".$suma_becas_[$beca->descripcion]}}</th>
        @else
        <th scope="col" colspan="2">{{"$".$suma_becas_[$beca->descripcion]}}</th>
        @endif
        @endforeach
        @php $suma_ingreso_consultor_ind=0;
        $suma_ingreso_family_ind=0;
        $suma_costo_final_ind=0;
        $suma_costo_inicial_ind=0;
        $suma_beca_=array();
          @endphp 
                 @foreach($becas as $beca)
            @php 
            
            $suma_becas_[$beca->descripcion]=0;

            @endphp
            @endforeach

   
</tr>
    @endif
    @endif
    @php
    $consul_ant=$cita->consultor_id;
    @endphp



    <tr>
        <th scope="row">{{$cita->id}}</th>
        @php

        $fecha=$cita->start;
        $fecha_actual = new DateTime($fecha);
        $cadena_fecha_actual = $fecha_actual->format("d/m/Y");
                $fecha_=$cita->fecha_pago;
    $fecha_actual_ = new DateTime($fecha_);
    $cadena_fecha_actual_ = $fecha_actual_->format("d/m/Y");
        @endphp
        <td>{{$cadena_fecha_actual}}</td>
        <td>{{$cadena_fecha_actual_}}</td>
        <td>{{$cita->consultor->nombre." ".$cita->consultor->apellido_pa}}</td>
        <td>{{empty($cita->paciente->alias)?$cita->paciente->nombre." ".$cita->consultor->apellido_pa:$cita->paciente->alias}}</td>
    <td>{{$cita->paciente->colegio}}</td>
        <td>${{$cita->ingreso_consultor}}</td>
        <td>${{$cita->ingreso_family}}</td>
        <td>${{$cita->monto_inicial}}</td>
        <td>${{$cita->monto_final}}</td>
        @php
        $suma_ingreso_consultor=$suma_ingreso_consultor+$cita->ingreso_consultor;
        $suma_ingreso_family=$suma_ingreso_family+$cita->ingreso_family;
        $suma_costo_inicial=$suma_costo_inicial+$cita->monto_inicial;
        $suma_costo_final=$suma_costo_final+$cita->monto_final;
        $suma_ingreso_consultor_ind=$suma_ingreso_consultor_ind+$cita->ingreso_consultor;
        $suma_ingreso_family_ind=$suma_ingreso_family_ind+$cita->ingreso_family;
        $suma_costo_final_ind=$suma_costo_final_ind+$cita->monto_inicial;
        $suma_costo_inicial_ind=$suma_costo_inicial_ind+$cita->monto_final;
        @endphp
        @foreach($becas as $bec_aux)
        @php
        $x=0;
        @endphp
        @foreach($cita->becas as $beca_cita)
        @if($beca_cita->Becas_Agregadas->beca_id==$bec_aux->id)

        @if($beca_cita->monto_porcentaje!='monto')
        <td colspan="1" >{{$beca_cita->Becas_Agregadas->cantidad*100}}%</td>
        <td colspan="1">{{"$".$beca_cita->Becas_Agregadas->equivalente}}
            @php 
            $aux=$suma_becas[$bec_aux->descripcion];
            $suma_becas[$bec_aux->descripcion]=$aux+$beca_cita->Becas_Agregadas->equivalente;
            $aux_=$suma_becas_[$bec_aux->descripcion];
            $suma_becas_[$bec_aux->descripcion]=$aux_+$beca_cita->Becas_Agregadas->equivalente;
           
            @endphp
        </td>
        @php $x=0; break; @endphp
        @else
        <td colspan="2">${{$beca_cita->Becas_Agregadas->equivalente}}</td>
        @php 
        $suma_becas[$bec_aux->descripcion]=$suma_becas[$bec_aux->descripcion]+$beca_cita->Becas_Agregadas->equivalente;
        $suma_becas_[$bec_aux->descripcion]=$suma_becas_[$bec_aux->descripcion]+$beca_cita->Becas_Agregadas->equivalente;
        

        $x=0; break;

        @endphp
        @endif
        @else

        @php $x=1; @endphp
        @endif
        @endforeach
        @if($x==1)
           @if($bec_aux->monto_porcentaje!='monto')
        <td colspan="1"></td>
        <td colspan="1"></td>
        @else
        <td colspan="2"></td>
        @endif
        
        @endif
        @endforeach
    </tr>

    @endforeach
  <tr style="background: #2aaad8">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <th>${{$suma_ingreso_consultor_ind}}</th>
        <th>${{$suma_ingreso_family_ind}}</th>
        <th>${{$suma_costo_final_ind}}</th>
        <th>${{$suma_costo_inicial_ind}}</th>
         @foreach($becas as $beca)
        @if($beca->monto_porcentaje!='monto')
        <th scope="col" colspan="1"></th>
        <th scope="col" char="" colspan="1">{{"$".$suma_becas_[$beca->descripcion]}}</th>
        @else
        <th scope="col" colspan="2">{{"$".$suma_becas_[$beca->descripcion]}}</th>
        @endif
        @endforeach

   
</tr>
    <tr style="background: #2368b3">
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col">${{$suma_ingreso_consultor}}</th>
        <th scope="col">${{$suma_ingreso_family}}</th>
        <th scope="col">${{$suma_costo_inicial}}</th>
        <th scope="col">${{$suma_costo_final}}</th>

        @foreach($becas as $beca)
        @if($beca->monto_porcentaje!='monto')
        <th scope="col" colspan="1"></th>
        <th scope="col" char="" colspan="1">{{"$".$suma_becas[$beca->descripcion]}}</th>
        @else
        <th scope="col" colspan="2">{{"$".$suma_becas[$beca->descripcion]}}</th>
        @endif
        @endforeach
    </tr>
    @endif
     @php
        $suma_ingreso_consultor=0;
        $suma_ingreso_family=0;
        $suma_costo_final=0;
        $suma_costo_inicial=0;
        $suma_ingreso_consultor_ind=0;
        $suma_ingreso_family_ind=0;
        $suma_costo_final_ind=0;
        $suma_costo_inicial_ind=0;
        $suma_becas=array();
        $suma_becas_=array();
        $consul_ant=null;
        @endphp
           <tr class="thead-dark">
            <th scope="col">Cita sin pago</th>
            <th scope="col">Fecha cita</th>
            <th scope="col">Consultor</th>
            <th scope="col">Paciente</th>
                <th scope="col">Colegio</th>
            <th scope="col">Ingreso Consultor</th>
            <th scope="col">Ingreso Family</th>
            <th scope="col">Costo Inicial</th>
            <th scope="col">Costo Final</th>

            @foreach($becas as $beca)
            @php 
            
            $suma_becas[$beca->descripcion]=0;
            $suma_becas_[$beca->descripcion]=0;

            @endphp
            @if($beca->monto_porcentaje!='monto')
            <th scope="col" colspan="1">{{$beca->descripcion}}</th>
            <th scope="col" colspan="1">$</th>
            @else
            <th scope="col" colspan="2">{{$beca->descripcion}}</th>
            @endif
            @endforeach
        </tr>

@if($citas_sin_paga->count()==0)
    <tr><td colspan="5"><center><h1>Hay datos</h1></center> </td></tr>
    @else
       @foreach($citas_sin_paga as $cita)
       @if($consul_ant!=$cita->consultor_id)
       @if($consul_ant!=null)

       <tr style="background: #2aaad8">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <th>${{$suma_ingreso_consultor_ind}}</th>
        <th>${{$suma_ingreso_family_ind}}</th>
        <th>${{$suma_costo_final_ind}}</th>
        <th>${{$suma_costo_inicial_ind}}</th>
         @foreach($becas as $beca)
        @if($beca->monto_porcentaje!='monto')
        <th scope="col" colspan="1"></th>
        <th scope="col" char="" colspan="1">{{"$".$suma_becas_[$beca->descripcion]}}</th>
        @else
        <th scope="col" colspan="2">{{"$".$suma_becas_[$beca->descripcion]}}</th>
        @endif
        @endforeach
        @php $suma_ingreso_consultor_ind=0;
        $suma_ingreso_family_ind=0;
        $suma_costo_final_ind=0;
        $suma_costo_inicial_ind=0;
        $suma_beca_=array();
          @endphp 
                 @foreach($becas as $beca)
            @php 
            
            $suma_becas_[$beca->descripcion]=0;

            @endphp
            @endforeach

   
</tr>
    @endif
    @endif
    @php
    $consul_ant=$cita->consultor_id;
    @endphp



    <tr>
        <th scope="row">{{$cita->id}}</th>
        @php

        $fecha=$cita->start;
        $fecha_actual = new DateTime($fecha);
        $cadena_fecha_actual = $fecha_actual->format("d/m/Y");
        @endphp
        <td>{{$cadena_fecha_actual}}</td>
        <td>{{$cita->consultor->nombre." ".$cita->consultor->apellido_pa}}</td>
        <td>{{empty($cita->paciente->alias)?$cita->paciente->nombre." ".$cita->consultor->apellido_pa:$cita->paciente->alias}}</td>
    <td>{{($cita->paciente->colegio!=0)?$cita->paciente->colegio_s->first()->descripcion:''}}</td>
        <td>${{$cita->ingreso_consultor}}</td>
        <td>${{$cita->ingreso_family}}</td>
        <td>${{$cita->monto_inicial}}</td>
        <td>${{$cita->monto_final}}</td>
        @php
        $suma_ingreso_consultor=$suma_ingreso_consultor+$cita->ingreso_consultor;
        $suma_ingreso_family=$suma_ingreso_family+$cita->ingreso_family;
        $suma_costo_inicial=$suma_costo_inicial+$cita->monto_inicial;
        $suma_costo_final=$suma_costo_final+$cita->monto_final;
        $suma_ingreso_consultor_ind=$suma_ingreso_consultor_ind+$cita->ingreso_consultor;
        $suma_ingreso_family_ind=$suma_ingreso_family_ind+$cita->ingreso_family;
        $suma_costo_final_ind=$suma_costo_final_ind+$cita->monto_inicial;
        $suma_costo_inicial_ind=$suma_costo_inicial_ind+$cita->monto_final;
        @endphp
        @foreach($becas as $bec_aux)
        @php
        $x=0;
        @endphp
        @foreach($cita->becas as $beca_cita)
        @if($beca_cita->Becas_Agregadas->beca_id==$bec_aux->id)

        @if($beca_cita->monto_porcentaje!='monto')
        <td colspan="1" >{{$beca_cita->Becas_Agregadas->cantidad*100}}%</td>
        <td colspan="1">{{"$".$beca_cita->Becas_Agregadas->equivalente}}
            @php 
            $aux=$suma_becas[$bec_aux->descripcion];
            $suma_becas[$bec_aux->descripcion]=$aux+$beca_cita->Becas_Agregadas->equivalente;
            $aux_=$suma_becas_[$bec_aux->descripcion];
            $suma_becas_[$bec_aux->descripcion]=$aux_+$beca_cita->Becas_Agregadas->equivalente;
           
            @endphp
        </td>
        @php $x=0; break; @endphp
        @else
        <td colspan="2">${{$beca_cita->Becas_Agregadas->equivalente}}</td>
        @php 
        $suma_becas[$bec_aux->descripcion]=$suma_becas[$bec_aux->descripcion]+$beca_cita->Becas_Agregadas->equivalente;
        $suma_becas_[$bec_aux->descripcion]=$suma_becas_[$bec_aux->descripcion]+$beca_cita->Becas_Agregadas->equivalente;
        

        $x=0; break;

        @endphp
        @endif
        @else

        @php $x=1; @endphp
        @endif
        @endforeach
        @if($x==1)
           @if($bec_aux->monto_porcentaje!='monto')
        <td colspan="1"></td>
        <td colspan="1"></td>
        @else
        <td colspan="2"></td>
        @endif
        
        @endif
        @endforeach
    </tr>

    @endforeach
  <tr style="background: #2aaad8">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <th>${{$suma_ingreso_consultor_ind}}</th>
        <th>${{$suma_ingreso_family_ind}}</th>
        <th>${{$suma_costo_final_ind}}</th>
        <th>${{$suma_costo_inicial_ind}}</th>
         @foreach($becas as $beca)
        @if($beca->monto_porcentaje!='monto')
        <th scope="col" colspan="1"></th>
        <th scope="col" char="" colspan="1">{{"$".$suma_becas_[$beca->descripcion]}}</th>
        @else
        <th scope="col" colspan="2">{{"$".$suma_becas_[$beca->descripcion]}}</th>
        @endif
        @endforeach

   
</tr>
    <tr style="background: #2368b3">
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col">${{$suma_ingreso_consultor}}</th>
        <th scope="col">${{$suma_ingreso_family}}</th>
        <th scope="col">${{$suma_costo_inicial}}</th>
        <th scope="col">${{$suma_costo_final}}</th>

        @foreach($becas as $beca)
        @if($beca->monto_porcentaje!='monto')
        <th scope="col" colspan="1"></th>
        <th scope="col" char="" colspan="1">{{"$".$suma_becas[$beca->descripcion]}}</th>
        @else
        <th scope="col" colspan="2">{{"$".$suma_becas[$beca->descripcion]}}</th>
        @endif
        @endforeach
    </tr>
@endif
</tbody>

</table>
<button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="far fa-file-excel"></span> Export to Excel</button>
</div>
@endif
</div>
<script type="text/javascript">

    $("#exportButton").click(function(){

        $("#table2excel").table2excel({

  
    exclude: ".noExl",

    name: "Worksheet Name",

    filename: "SomeFile.xls" //do not include extension

});

    });
    
function calculaReporte(){
       if($("#fecha_ini").val()!=null){
        if($("#consultor_select").val()!=''){
                    $("#response").empty();
                 $('<div class="progress" id="gif" style="width:30%;display: none;">'+
  '<div id="bar" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 40%">Espera un momento...</div>').insertBefore("#response");
            $("#gif").css("display","flex");
            for (var i=0; i<750; i+=2) {
            x=i/10;
            i=i-1;

             $("#bar").css("width",x+"%");
}
 $("#calculaReporte").submit();
        }else{

                swal('Seleccione consultor por favor');
        }
       }else{

                swal('Seleccione una fecha por favor');
       }
}
   
</script>
@endsection