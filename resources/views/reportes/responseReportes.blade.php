  @if($reporte=='Casos')
  <table class="table" id="table2excel">

    <tbody>


    
      @if(!empty($casos_diagnosticos))
     

<thead class="thead btn-primary">
      <tr id="casos_diagnosticos"  onClick="zoom('casos_diagnosticos')">
        <th scope="col">Diagnosticos Nuevos</th>
        <th scope="col">No de casos {{$casos_diagnosticos->count()}}</th>
        <th scope="col-2"><i class="fas fa-plus-square 2x"></i></th>
      </tr>
    </thead>
    <thead class="thead-dark" >
      <tr  style="display:none;" class="_casos_diagnosticos show">
        <th scope="col">Caso</th>
        <th scope="col">Paciente</th>
        <th scope="col">Consultor</th>
        <th scope="col">Clasificacion</th>
        <th scope="col">Procedencia</th>
      </tr>
    </thead>

      @foreach($casos_diagnosticos as $caso)
      <tr style="display:none;" class="_casos_diagnosticos show">
        <td>{{$caso->id}}</td>
        <td>{{$caso->paciente->nombrecompleto}}</td>
        <td>{{$caso->consultor->nombrecompleto}}</td>
        <td>{{($caso->paciente->diagnosticos->clasificacion_id==0)?'No hay clasificacion':$caso->paciente->diagnosticos->clasificacion->descripcion}}</td>
        <td>{{$caso->paciente->colegio_s->descripcion}}</td>
      </tr>
      @endforeach 
      @endif
        @if(!empty($casos_seguimientos))
      <thead class="thead btn-primary" onClick="zoom('casos_seguimientos')">
      <tr id="casos_seguimientos">
      <th scope="col">Casos atendidos</th>
        <th scope="col">No de casos {{$casos_seguimientos->count()}}</th>
        <th scope="col-2"><i class="fas fa-plus-square"></i></th>
      </tr>
      <thead class="thead-dark">
      <tr style="display:none;" class="_casos_seguimientos show">
        <th scope="col">Caso</th>
        <th scope="col">Paciente</th>
        <th scope="col">Consultor</th>
        <th scope="col">Clasificacion</th>
      </tr>
    </thead>
      @foreach($casos_seguimientos as $paciente)

      <tr style="display:none;" class="_casos_seguimientos show">
        <td>{{$paciente->caso}}</td>
        <td>{{empty($paciente->alias)?$paciente->nombre." ".$paciente->apellido_pa:$paciente->alias}}</td>
        <td>{{$paciente->diagnosticos->consultor_asignado->nombrecompleto}}</td>
        <td>{{($paciente->diagnosticos->clasificacion_id==0)?' ':$paciente->diagnosticos->clasificacion->descripcion}}</td>
      
      </tr>
      @endforeach 
      @endif
      @if(!empty($casos_altas))
<thead class="thead btn-primary" onClick="zoom('casos_altas')">
      <tr id="casos_altas">
      <th scope="col">Casos dados de alta</th>
        <th scope="col">No de casos {{$casos_altas->count()}}</th>
        <th scope="col-2"><i class="fas fa-plus-square"></i></th>
      </tr>
      <thead class="thead-dark">
      <tr style="display:none;" class="_casos_altas show">
        <th scope="col">Caso</th>
        <th scope="col">Paciente</th>
        <th scope="col">Consultor</th>
        <th scope="col">Clasificacion</th>
      </tr>
          
    </thead>
      @foreach($casos_altas as $caso)
      <tr style="display:none;" class="_casos_altas show">
        <td>{{$caso->id}}</td>
        <td>{{empty($caso->paciente->alias)?$caso->paciente->nombre." ".$caso->paciente->apellido_pa:$caso->paciente->alias}}   </td>
        <td>{{$caso->Consultor->nombre}}</td>
      <td>{{($caso->diagnostico->clasificacion_id==0)?' ':$caso->diagnostico->clasificacion->descripcion}}</td>
      
      </tr>
      @endforeach
      @endif
      @if(!empty($disg_sin_seg))
      <thead class="thead btn-primary" onClick="zoom('disg_sin_seg')">
      <tr id="disg_sin_seg">
      <th scope="col">Diagnosticos sin seguimiento</th>
        <th scope="col">No de casos {{$disg_sin_seg->count()}}</th>
        <th scope="col-2"><i class="fas fa-plus-square"></i></th>
      </tr><thead class="thead-dark">
      <tr style="display:none;" class="_disg_sin_seg show">
        <th scope="col">Caso</th>
        <th scope="col">Paciente</th>
        <th scope="col">Consultor</th>
        <th scope="col">Clasificacion</th>
      </tr>
    </thead>
      @foreach($disg_sin_seg as $paciente)

      <tr style="display:none;" class="_disg_sin_seg show">
        <td>{{$paciente->caso}}</td>
        <td>{{empty($paciente->alias)?$paciente->nombre." ".$paciente->apellido_pa:$paciente->alias}}</td>
        <td>{{$paciente->diagnosticos->Consultor->nombre}}</td>
    <td></td>
      
      </tr>
      @endforeach
      @endif



    </tbody>
  </table>
  <button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="far fa-file-excel"></span> Export to Excel</button>
  <script type="text/javascript">

    $("#exportButton").click(function(){

        $("#table2excel").table2excel({

    // exclude CSS class

    exclude: ".noExl",

    name: "Worksheet Name",

    filename: "SomeFile.xls" //do not include extension

});

    });
function zoom(divv){
                event.preventDefault();
                id_div=divv;
                tdd="_".concat(id_div);

                $is_show=$("."+tdd).css("display");
                if($is_show!="none"){
                $("."+tdd).css("display","none");   
                }else{
                $(".show").css("display","none");
                $("."+tdd).css("display","table-row");
    }
}
  </script>
  @elseif($reporte=='Diagnosticos')
  <div>

  </div>
  @elseif($reporte=='Citas_turno')
  <div>

        <table class="table" id="table2excel" style="text-align: center;">
 
    <tbody>
        <thead class="thead-dark">
           <tr>
            <th scope="col">Fecha Cita mañana {{$citas_manana->count()}}</th>
        </tr>
    </thead>
      <thead class="thead-dark">
           <tr>
            <th scope="col">Fecha Cita tarde {{$citas_tarde->count()}}</th>
            
        </tr>
    </thead>
    </tbody>
  </table>  
  </div>
   <button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="far fa-file-excel"></span> Export to Excel</button>
  <script type="text/javascript">

    $("#exportButton").click(function(){

        $("#table2excel").table2excel({

    // exclude CSS class

    exclude: ".noExl",

    name: "Worksheet Name",

    filename: "SomeFile.xls" //do not include extension

});

    });
  </script>
  @elseif($reporte=='Horas_Consultor')
  <div>
   <table class="table" id="table2excel" style="text-align: center;">
  <thead class="thead-dark">
           <tr>
            <th scope="col">Consultor</th>
            <th scope="col">Horas trabajadas</th>
            <th scope="col">Horas Posibles</th>
            <th scope="col">Diferencia</th>
        </tr>
    </thead>
    <tbody>
       
      @foreach($consultores as $consultor)
      <tr>
        @php
        $h_trabajadas=$consultor->citas_rr->where('fecha_cita','>',$inicio)->where('fecha_cita','<',$fin)->count();
        $h_disponible=$consultor->horario_consultor->horas_disponible;
        $dif=$h_disponible-$h_trabajadas;
        @endphp
      <td>{{$consultor->nombre." ".$consultor->apellido_pa." ".$consultor->apellido_ma}}</td>
      <td>{{$h_trabajadas}}</td>
     <td>{{$h_disponible}}   </td>
      <td>{{$dif}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>  
  </div>
   <button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="far fa-file-excel"></span> Export to Excel</button>
     @elseif($reporte=='seguimiento_consultor')

     <table id="table2excel" class="table">
      <thead class="thead-dark">
        <tr>
          <th>Consultor</th>
          <th>Seguimientos</th>
          <th>Citas</th>
          @if(auth()->user()->hasRoles(['direc','consul']))
    @php
    if(auth()->user()->hasRoles(['direc'])){
      $con_si=Session::get('con_si',false);
      if($con_si==true){ @endphp
      <th>Ver cita</th>
      @php } else { @endphp
      <th>Ver más </th>
        @php } }else { @endphp
      <th>Ver cita</th>
      @php }
    @endphp
@else

    <th>Ver más </th>
    @endif
        </tr>
      </thead>
  <tbody>
  
     @foreach($consultor_citas as $cc)
  <thead class="thead btn-primary"  onClick="zoom('{{$cc->consultor_id}}')">
    @php
      $x=0;
    @endphp
       <tr>
         <th> {{$cc->nombrecompleto}}  </th>
         
           @foreach ($seguimientos as $element)
       {{-- expr --}}
       @if ($element->consultor_id==$cc->consultor_id)
         @php
         $x++;
       @endphp
       @endif
         @endforeach
     <th>{{$x}}</th>
     <th>
           {{$cc->citas}}
         </th>
          <th scope="col-2"><i class="fas fa-plus-square 2x"></i></th>
 
       </tr>
     
</thead>
  <thead class="thead-dark">
    <tr style="display:none;" class="_{{$cc->consultor_id}} show">
      
    <th>Paciente</th>
    <th>Fecha seguimientos</th>
    <th>Fecha citas</th>
   @if(auth()->user()->hasRoles(['direc','consul']))
    @php
    if(auth()->user()->hasRoles(['direc'])){
      $con_si=Session::get('con_si',false);
      if($con_si==true){ @endphp
      <th>Ver cita</th>
      @php } else { @endphp
      <th>Monto</th>
        @php } }else { @endphp
      <th>Ver cita</th>
      @php }
    @endphp
@else

    <th>monto</th>
    @endif
    </tr>
  </thead>     

    @foreach ($pacientes_cita as $pacientes)
    @if ($cc->consultor_id==$pacientes->consultor_id)
      @php
        $paciente_aux=$pacientes->paciente_id;
      @endphp
      <tr style="display:none;" class="_{{$cc->consultor_id}} show">
        
     <td>{{$pacientes->nombrecompleto}}</td>
       @php
           $aux=0;
           $aux1=0;
         @endphp
     @foreach ($seguimientos as $element)
       @if ($element->paciente_id==$paciente_aux)
         @php
           $aux=1;
         @endphp
         @if($element->cita_id==$pacientes->cita )
 @php
           $aux1=1;
         @endphp
     <td>{{$element->created_at}}</td>
        @else
        @php 
$aux=0;
        @endphp
         @endif

       @endif
     @endforeach

   @if ($aux==0)
   @if($aux1!=1)
     <td>No hay registro </td>
   @endif
   @endif
     <td>{{$pacientes->fecha_cita}}</td>
        @if(auth()->user()->hasRoles(['direc','consul']))
    @php
    if(auth()->user()->hasRoles(['direc'])){
      $con_si=Session::get('con_si',false);
      if($con_si==true){ @endphp
      <th> <a id="ir_cita" name="post" type="post" href="{{route('consultores.pacientes',['id'=>$pacientes->cita])}}" class="btn btn-primary">Ir a Cita</a></th>
      @php } else { @endphp
 <td>{{$pacientes->ingreso_consultor}}</td>
        @php } }else { @endphp
      <th> <a id="ir_cita" name="post" type="post" href="{{route('consultores.pacientes',['id'=>$pacientes->cita])}}" class="btn btn-primary">Ir a Cita</a></th>
      @php }
    @endphp
@else
 <td>{{$pacientes->ingreso_consultor}}</td>
    @endif
    
      </tr>
    @endif
    @endforeach
     @endforeach
  </tbody>
     </table>
        <button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="far fa-file-excel"></span> Export to Excel</button>
     <hr>
      <div id="barchart_material" style="width: 900px; height: 500px;"></div>
     <script type="text/javascript">
       function zoom(divv){
                event.preventDefault();
                id_div=divv;
                tdd="_".concat(id_div);

                $is_show=$("."+tdd).css("display");
                if($is_show!="none"){
                $("."+tdd).css("display","none");   
                }else{
                $(".show").css("display","none");
                $("."+tdd).css("display","table-row");
    }
}
     </script>
     <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data_s={!!$json_res!!};
        var data = google.visualization.arrayToDataTable(data_s);

        var options = {
          chart: {
            title: 'Citas y seguimientos por consultor',
          },
          bars: 'horizontal' // Required for Material Bar Charts.
        };

        var chart = new google.charts.Bar(document.getElementById('barchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
      <script type="text/javascript">

    $("#exportButton").click(function(){

        $("#table2excel").table2excel({

    // exclude CSS class

    exclude: ".noExl",

    name: "Worksheet Name",

    filename: "Seguimientos_Consultor.xls" //do not include extension

});

    });
  </script>
  @endif