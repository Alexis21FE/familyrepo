    @extends('layouts.app')
    @section('content')
    @php
    $x=0;
    if(isset($isResponse)){
      $fecha_ini=$inicio;
    }else{
      $fecha_ini=null;
    }
    @endphp

    <div class="section">
        <center>
            
        <p style="font-family: 'Bree Serif', serif; font-size: 2em; margin: 0 !important;">Registro de altas</p>
        </center>
        {{Form::open(['route'=>'reporte.consultaAltas','method'=>'post','role'=>'form','id'=>'calculaReporte'])}}
            <div class="row" style="margin:auto;">
        
        <div class="col">
{{Form::label('fecha_ini','Fecha Inicial')}}
    {{Form::date('fecha_ini',$fecha_ini,['class'=>'form-control','type'=>'date','required'=>'required','id'=>'fecha_ini'])}}
        </div>
        <div class="col"></div>
        <div class="col">
            {{Form::label('fecha_fin','Fecha final')}}
    {{Form::date('fecha_fin',date('Y-m-d'),['class'=>'form-control','type'=>'date','required'=>'required','onChange'=>'calculaReporte()'])}}
            
        </div>
        <div class="col"></div>
    </div>
    {{Form::close()}}
    <hr>
    <div id="response">

   @if(isset($isResponse))
   @if($pacientes->count()==0)
   <center><h1>No hay datos</h1></center>
   @else
   <table class="table" id="table2excel">
      <thead class="thead-dark">
         <tr>
            <th scope="col">Nombre</th>
            <th scope="col">Fecha ingreso</th>
            <th scope="col">Direccion</th>
            <th scope="col">Telefono</th>
        </tr>
    </thead>
    <tbody>
     @foreach($pacientes as $paciente)
     <tr>
        <td>{{empty($paciente->alias)?$paciente->nombre:$paciente->alias}}</td>
        <td>{{$paciente->created_at->format('d/m/Y')}}</td>
        <td>{{$paciente->direccion}}</td>
        <td>{{$paciente->celular_paciente}}</td>
        
    </tr>
   @endforeach
</tbody>
</table>
<button id="exportButton" class="btn btn-lg btn-danger clearfix"><span class="far fa-file-excel"></span> Export to Excel</button>
</div>
@endif
@endif
</div>
    <script type="text/javascript">
    	
        $("#exportButton").click(function(){

        $("#table2excel").table2excel({

    // exclude CSS class

    exclude: ".noExl",

   name: "Worksheet Name",

    filename: "SomeFile.xls" //do not include extension

  });

});
 $(document).ready(function(){

            $("#fecha_ini").change(function(){
      $("#response").empty();
                 $('<div class="progress" id="gif" style="width:30%;display: none;">'+
  '<div id="bar" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 40%">Espera un momento...</div>').insertBefore("#response");
            $("#gif").css("display","flex");
            for (var i=0; i<750; i+=2) {
            x=i/10;
            i=i-1;

             $("#bar").css("width",x+"%");
}
           $("#calculaReporte").submit();
            
    
            });
        
        });

    </script>
    @endsection