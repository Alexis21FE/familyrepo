@extends('../../layouts/app')
@section('content')
<div class="container">
	<div id="categs">
    
  {{Form::open(['route'=>'paciente.add','method'=>'post','role'=>'form'])}}
  <div class="row">
    <div class="col">
       {{ Form::label('categoria','Categoria') }}
    </div>
    <div class="col">
       {{ Form::label('Porcentaje_consultor','Porcentaje Consultor') }}
    </div>
    <div class="col">
      {{ Form::label('Porcentaje_family','Porcentaje family') }}
    </div>
    <div class="col"></div>
  </div>
  @foreach($categorias as $categoria)
 <div class="row row-cat" id="{{$categoria->id}}">
                      {{Form::hidden('categoria_id',$categoria->id,['type'=>'hidden'])}}
                    <div class="col">

                     
                      {{Form::text('categoria',$categoria->descripcion,['class'=>'form-control','readonly'=>true])}}
                    </div>
                    <div class="col">
                     
                      {{Form::text('Porcentaje_consultor',$categoria->porcentaje_consultor,['class'=>'form-control','readonly'=>true])}}
                    </div>

                    <div class="col">
                     
                      {{Form::text('Porcentaje_family',$categoria->porcentaje_family,['class'=>'form-control','readonly'=>true])}}
                    </div>
                     <div class="col">
                      {{Form::button('Editar',['class'=>'btn btn-warning','onClick'=>'editar('.$categoria->id.')'])}}
                      {{Form::button('Eliminar',['class'=>'btn btn-danger'])}}
                      {{Form::button('Guardar',['class'=>'btn btn-success','onClick'=>'guardar('.$categoria->id.')'])}}
                    </div>
                  </div>
                  @endforeach
  </div>
                  <hr>

                   <div class="row" id="agrega">

                   <div class="col">

                      {{ Form::label('categoria','Categoria') }}
                      {{Form::text('categoria',null,['class'=>'form-control','onlyread'=>true])}}
                    </div>
                    <div class="col">
                      {{ Form::label('Porcentaje_consultor','Porcentaje Consultor') }}
                      {{Form::text('Porcentaje_consultor',null,['class'=>'form-control','onlyread'=>true])}}
                    </div>

                    <div class="col">
                     {{ Form::label('Porcentaje_family','Porcentaje family') }}
                      {{Form::text('Porcentaje_family',null,['class'=>'form-control','onlyread'=>true])}}
                    </div>

                    <div class="col button-becas">
                    {{Form::button('Agregar',['class'=>'form-control btn btn-primary','id'=>'agrega_categoria'])}}
                    </div>
                  </div>
                  {{Form::close()}}
  
</div>
<script type="text/javascript">
  $(document).ready(function(){

   $("#agrega_categoria").click(function(){
    var form=$("#agrega").find("input").serialize();
    $.ajax({
      url:"{{route('categoria.new')}}",
      data:form,
      datatype:"json",
      type:"post",
      success:function(response){
        if(response.status==true){
          $("#agregar").find("input").attr("val","");
           campo='<div class="row row-cat" id="'+response.categoria.id+'">'+
                      '<input type="hidden" name="categoria_id" value="'+response.categoria.id+'">'+
                    '<div class="col">'+

                      '<label for="categoria">Categoria</label>'+
                      '<input class="form-control" readonly="" name="categoria" type="text" value="'+response.categoria.descripcion+'" id="categoria">'+
                    '</div>'+
                   ' <div class="col">'+
                     ' <label for="Porcentaje_consultor">Porcentaje Consultor</label>'+
                     ' <input class="form-control" readonly="" name="Porcentaje_consultor" type="text" value="'+response.categoria.Porcentaje_consultor+'" id="Porcentaje_consultor">'+
                    '</div>'+

                    '<div class="col">'+
                     '<label for="Porcentaje_family">Porcentaje family</label>'+
                     ' <input class="form-control" readonly="" name="Porcentaje_family" type="text" value="'+response.categoria.Porcentaje_family+'" id="Porcentaje_family">'+
                    '</div>'+
                     '<div class="col">'+
                     ' <button class="btn btn-warning" onclick="editar('+response.categoria.id+')" type="button">Editar</button>'+
                     ' <button class="btn btn-danger" type="button">Eliminar</button>'+
                     ' <button class="btn btn-success" onclick="guardar('+response.categoria.id+')" type="button">Guardar</button>'+
                    '</div>'+
                 ' </div>';
                 $("#categs").append(campo);
        }else{
          alert(response.mensaje);
        }
      },
      error:function(){}
    });
   });


  });
      function editar(id){
        $("#"+id).find('input').prop('readonly',false);
    }
    function guardar(id){
      var categoria_id=id;
      var form=$("#"+id).find("input").serialize();
      console.log(form);
      $.ajax({
        url:"{{route('categoria.actulizar')}}",
        data:form,
        datatype:"json",
        type:"post",
        success:function(response){
          if(response.status==true){
     $("#"+id).find("input").prop('readonly',true);
     alert(response.mensaje);
          }
        },
        error:function(){

        }
      });
    }
 
</script>
@endsection