@extends('../../layouts/app')
@section('content')
@php
$x=0; 
  @endphp
<div class="container">
  <div id="categs">

    {{Form::open(['route'=>'paciente.add','method'=>'post','role'=>'form'])}}
    @foreach($sedes as $sede)
    @php
    $x++;
    @endphp
    <div class="row" id="{{$sede->id}}" style="margin-top: .8%;">
      {{Form::hidden('sede_id',$sede->id,['type'=>'hidden'])}}
      <div class="col" style="text-align: end;">

        {{ Form::label('sede','sede') }}

      </div>
      <div class="col">
       {{Form::text('sede',$sede->descripcion,['class'=>'form-control','readonly'=>true,'id'=>$x])}}
     </div>
     <div class="col">
      {{Form::button('Editar',['class'=>'btn btn-warning','onClick'=>'editar('.$sede->id.')'])}}
      {{Form::button('Eliminar',['class'=>'btn btn-danger'])}}
      {{Form::button('Guardar',['class'=>'btn btn-success','onClick'=>'guardar('.$sede->id.')'])}}
    </div>
  </div>
  @endforeach
</div>
<hr>

<div class="row" id="agrega">

 <div class="col" style="text-align: end;">

  {{ Form::label('sede','sede') }}

</div>
<div class="col">
 {{Form::text('sede',null,['class'=>'form-control'])}}
</div>

<div class="col">
  {{Form::button('Agregar',['class'=>'form-control btn btn-primary','id'=>'agrega_sede'])}}
</div>

</div>
{{Form::close()}}

</div>
<script type="text/javascript">
  $(document).ready(function(){

   $("#agrega_sede").click(function(){
    var form=$("#agrega").find("input").serialize();
    $.ajax({
      url:"{{route('sede.new')}}",
      data:form,
      datatype:"json",
      type:"post",
      success:function(response){
        if(response.status==true){
          $("#agregar").find("input").attr("val","");
          campo='<div class="row" id="'+response.sede.id+'" style="margin-top: .8%;">'+
          '<input type="hidden" name="sede_id" value="'+response.sede.id+'">'+
          '<div class="col" style="text-align: end;">'+
          '<label for="sede">sede</label>'+

          '</div>'+
          '<div class="col">'+
          '<input class="form-control" name="sede" type="text" value="'+response.sede.descripcion+'" id="sede_new" readonly="readonly">'+
          '</div>'+
          '<div class="col">'+
          '<button class="btn btn-warning" onclick="editar('+response.sede.id+')" type="button">Editar</button>'+
          '<button class="btn btn-danger" type="button">Eliminar</button>'+
          ' <button class="btn btn-success" onclick="guardar('+response.sede.id+')" type="button">Guardar</button>'+
          '</div>'+
          '</div>';
          $("#categs").append(campo);
        }else{
          alert(response.mensaje);
        }
      },
      error:function(){}
    });
  });


 });
  function editar(id){
    $("#"+id).find('input').prop('readonly',false);
  }
  function guardar(id){
    var sede_id=id;
    var form=$("#"+id).find("input").serialize()
    console.log(form);
    $.ajax({
      url:"{{route('sede.update')}}",
      data:form,
      datatype:"json",
      type:"post",
      success:function(response){
        if(response.status==true){
         $("#"+id).find("input").prop('readonly',true);
         alert(response.mensaje);
       }
     },
     error:function(){

     }
   });
  }

</script>
@endsection