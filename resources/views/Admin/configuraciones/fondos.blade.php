@extends('../../layouts/app')
@section('content')

<div class="container">
    {{Form::open(['route'=>'fondo.abonar','method'=>'post','role'=>'form','id'=>'abonarfondo'])}}
  <div id="modalEvent" class="modal" tabindex="-1" data-backdrop="static" >
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <h4>Abonar Fondo</h4>

        </div>
        <div class="modal-body">
         <div class="row">
          <div class="col">
           {{ Form::label('descripcion','Nombre') }}
         </div>
         <div class="col">
           {{ Form::label('saldo_actual','Saldo Actual') }}
         </div>
         <div class="col">
          {{ Form::label('saldo_abonar','Monto a abonar') }}
        </div>
        <div class="col-3"></div>
      </div>
      <div class="row row-cat">
  {{Form::hidden('fondo_id',null,['type'=>'hidden','id'=>'fondo_id'])}}
  {{Form::hidden('beca_id',null,['type'=>'hidden','id'=>'beca_id'])}}
  <div class="col">


    {{Form::text('fondo',null,['class'=>'form-control','readonly'=>true,'id'=>'fondo'])}}
  </div>
  <div class="col">

    {{Form::text('saldo_actual',null,['class'=>'form-control','readonly'=>true,'id'=>'saldo_actual'])}}
  </div>

  <div class="col">

    {{Form::text('saldo_abonar',null,['class'=>'form-control','id'=>'saldo_abonar'])}}
  </div>
  <div class="col-3">
      {{Form::button('guardar',['class'=>'btn btn-primary','id'=>'saveFondo'])}}
          <button type="button" class="btn btn-danger" id="close_modal" data-dismiss="modal">Cancelar</button>
  </div>
</div>
      
    </div>
  </div>
</div>
</div>

{{Form::close()}}

<div id="categs">


  <div class="row">
    <div class="col">
     {{ Form::label('descripcion','Nombre') }}
   </div>
   <div class="col">
     {{ Form::label('Saldo_inicial','Saldo inicial') }}
   </div>
   <div class="col">
    {{ Form::label('Saldo_final','Saldo final') }}
  </div>
  <div class="col-3"></div>
</div>
@foreach($fondos as $fondo)
<div class="row row-cat" id="{{$fondo->id}}">
  {{Form::hidden('fondo_id',$fondo->id,['type'=>'hidden'])}}
  {{Form::hidden('beca_id',$fondo->registro->first()->beca_id,['type'=>'hidden'])}}
  <div class="col">


    {{Form::text('fondo',$fondo->descripcion,['class'=>'form-control','readonly'=>true])}}
  </div>
  <div class="col">

    {{Form::text('Saldo_inicial',"$".number_format($fondo->registro->first()->saldo_inicial,2),['class'=>'form-control','readonly'=>true])}}
  </div>

  <div class="col">

    {{Form::text('Saldo_final',"$".number_format($fondo->registro->first()->saldo_final,2),['class'=>'form-control','readonly'=>true])}}
  </div>
  <div class="col">
    {{Form::button('Abonar',['class'=>'btn btn-warning','onClick'=>'abonar('.$fondo->id.')'])}}

  </div>
</div>
@endforeach
</div>
<hr>

<div class="row" id="agrega">

<div class="col"></div>

 <div class="col">

  {{ Form::label('fondo','Nombre del fondo') }}
  {{Form::text('fondo',null,['class'=>'form-control','id'=>'nombre_f'])}}
</div>
<div class="col">
  {{ Form::label('Saldo_inicial','Monto Inicial') }}
  {{Form::text('Saldo_inicial',null,['class'=>'form-control','id'=>'cantidad_f'])}}
</div>
<div class="col  button-becas">
  {{Form::button('Agregar',['class'=>'form-control btn btn-primary','id'=>'agrega_fondo'])}}
</div>
</div>

</div>
<script type="text/javascript">
  $(document).ready(function(){

    $("#close_modal").click(function(){

      $("#modalEvent").hide();
    });

    $("#agrega_fondo").click(function(){
      var form=$("#agrega").find("input").serialize();
      if(confirm("Estas seguro de crear este fondo "+$("#nombre_f").val()+" con "+$("#cantidad_f").val()))
      $.ajax({
        url:"{{route('fondo.add')}}",
        data:form,
        datatype:"json",
        type:"post",
        success:function(response){
          if(response.status==true){
            $("#agregar").find("input").attr("val","");
           campo='<div class="row row-cat" id="'+response.fondo.id+'">'
  +'<input type="hidden" name="fondo_id" value="'+response.fondo.id+'">'
  +'<input type="hidden" name="beca_id" value="'+response.fondo.id+'">'
  +'<div class="col">'


  +'  <input class="form-control" readonly="" name="fondo" type="text" value="'+response.fondo.descripcion+'">'
  +'</div>'
  +'<div class="col">'

  +'  <input class="form-control" readonly="" name="Saldo_inicial" type="text" value="'+response.fondo_registro.saldo_inicial+'" id="Saldo_inicial">'
 +' </div>'

 +' <div class="col">'

   +' <input class="form-control" readonly="" name="Saldo_final" type="text" value="'+response.fondo_registro.saldo_final+'" id="Saldo_final">'
  +'</div>'
  +'<div class="col">'
  +'  <button class="btn btn-warning" onclick="abonar('+response.fondo.id+')" type="button">Abonar</button>'

  +'</div>'
+'</div>';
            $("#categs").append(campo);
          }else{
            alert(response.mensaje);
          }
        },
        error:function(){}
      });
    });

    $("#saveFondo").click(function(){
      if(confirm("Estas seguro de abonar a "+$("#fondo").val()+" Cantidad:"+$("#saldo_abonar").val() )){
        $("#abonarfondo").submit();
      }else{
        revertFunc();
      }
    });

  });
  function editar(id){
    $("#"+id).find('input').prop('readonly',false);
  }
  function guardar(id){
    var fondo_id=id;
    var form=$("#"+id).find("input").serialize()
    $.ajax({
      data:form,
      datatype:"json",
      type:"post",
      success:function(response){
        if(response.status==true){
         $("#"+id).find("input").prop('readonly',true);
         alert(response.mensaje);
       }
     },
     error:function(){

     }
   });
  }
  function abonar(id){
    var form=$("#"+id).find("input").serializeArray();
   $("#fondo_id").val(form[0].value);
   $("#beca_id").val(form[1].value);
$("#fondo").val(form[2].value);
$("#saldo_actual").val(form[4].value);
    $("#modalEvent").show();
  }
</script>
@endsection
