@extends('../../layouts/app')
@section('content')
<div class="container">
	
	{{Form::open(['route'=>'paciente.add','method'=>'post','role'=>'form'])}}
<div class="row">
  <div class="col"> {{ Form::label('Descripcion','Descripcion') }}</div>
  <div class="col"> {{ Form::label('Porcentaje_monto','Porcentaje o monto') }}</div>
  <div class="col"> {{ Form::label('afecta','Afecta') }}</div>
  <div class="col"> {{ Form::label('tiene_fondo','Tiene fondo') }}</div>
  <div class="col"> {{ Form::label('nombre_fondo','Nombre del fondo') }}</div>
  <div class="col"> {{ Form::label('orden','Orden') }}</div>
  <div class="col-3"></div>
</div>
 <div id="becas">
  @foreach($becas_apoyos as $beca_apoyo)
 
 <div class="row" style="margin-top: .8%;" id="{{$beca_apoyo->id}}">
                    <div class="col">
                      {{Form::hidden('beca_id',$beca_apoyo->id,['type'=>'hidden'])}}
                     
                      {{Form::text('Descripcion',$beca_apoyo->descripcion,['class'=>'form-control','readonly'=>true])}}
                    </div>
                    <div class="col">
                     
                      {{Form::select('Porcentaje_monto',['porcentaje'=>'Porcentaje','monto'=>'Monto'],$beca_apoyo->monto_porcentaje,['class'=>'form-control','readonly'=>true,'placeholder'=>'Ejie una opcion','required'=>true])}}
                    </div>
<div class="col">
                      {{Form::select('afecta',['institucion'=>'Institucion','consultor'=>'Consultor','ambos'=>'Ambos'],$beca_apoyo->afecta,['class'=>'form-control','readonly'=>true,'placeholder'=>'Ejie una opcion','required'=>true])}}
                    </div>
                    <div class="col">
                    
                      {{Form::select('tiene_saldo',['S'=>'Si','N'=>'No'],$beca_apoyo->tiene_saldo,['class'=>'form-control','readonly'=>true,'placeholder'=>'Elije una opcion','required'=>true])}}
                    </div>

                   	<div class="col">
                    
                    @if($beca_apoyo->tiene_saldo=='S')
                    @foreach($apoyos as $apoyo)
                    @if($beca_apoyo->id==$apoyo->registro->first()->beca_id)

                   {{Form::text('nombre_fondo',$apoyo->descripcion,['class'=>'form-control','readonly'=>true])}}
           
                     @endif
                    @endforeach
                    @else
                    {{Form::text('nombre_fondo','No tiene saldo',['class'=>'form-control','readonly'=>true])}}
                    @endif
                     
                    </div>
                    <div class="col">
                      {{Form::text('orden',$beca_apoyo->orden,['class'=>'form-control','readonly'=>true])}}
                    </div>
                     <div class="col-3 ">
                     {{Form::button('Editar',['class'=>'btn btn-warning','onClick'=>'editar('.$beca_apoyo->id.')'])}}
                       {{Form::button('Eliminar',['class'=>'btn btn-danger'])}}
                        {{Form::button('Guardar',['class'=>'btn btn-success','onClick'=>'guardar('.$beca_apoyo->id.')'])}}
                    </div>
                    
                   
                  </div>
                  @endforeach
                  </div>
                  <hr>

                   <div class="row" id="agrega">

                    <div class="col">

                      {{ Form::label('Descripcion','Descripcion') }}
                      {{Form::text('Descripcion',null,['class'=>'form-control','onlyread'=>true,'id'=>'descripcion_'])}}
                    </div>
                    <div class="col">
                      {{ Form::label('Porcentaje_monto','Porcentaje o monto') }}
                      {{Form::select('Porcentaje_monto',['porcentaje'=>'Porcentaje','monto'=>'Monto'],null,['class'=>'form-control','onlyread'=>true,'placeholder'=>'Ejie una opcion','required'=>true])}}
                    </div>
<div class="col">
  {{ Form::label('afecta','Afecta') }}
                      {{Form::select('afecta',['institucion'=>'Institucion','consultor'=>'Consultor','ambos'=>'Ambos'],$beca_apoyo->afecta,['class'=>'form-control','placeholder'=>'Ejie una opcion','required'=>true])}}
                    </div>
                    <div class="col">
                     {{ Form::label('tiene_fondo','Tiene fondo') }}
                      {{Form::select('tiene_saldo',['S'=>'Si','N'=>'No'],null,['class'=>'form-control','onlyread'=>true,'placeholder'=>'Elije una opcion','required'=>true])}}
                    </div>
                    <div class="col">
                     {{ Form::label('nombre_fondo','Nombre del fondo') }}
                      {{Form::select('nombre_fondo',$apoyos_sin_beca->pluck('descripcion','id'),null,['class'=>'form-control','placeholder'=>'Elije una opcion','required'=>true])}}
                    </div>
                    <div class="col">
                     {{ Form::label('orden','Orden') }}
                      {{Form::text('orden',null,['class'=>'form-control','placeholder'=>'Inserta el orden Ejm: 1,2,3 etc','required'=>true,'id'=>'orden_'])}}
                    </div>
                    
                    <div class="col button-becas">
                    {{Form::button('Agregar',['class'=>'form-control btn btn-primary','id'=>'agrega_beca'])}}
                    </div>
                  </div>
                  {{Form::close()}}
	
</div>
<script type="text/javascript">
  $(document).ready(function(){

   $("#agrega_beca").click(function(){
    var form=$("#agrega").find(":input").serialize();
    $.ajax({
      url:"{{route('beca.new')}}",
      data:form,
      datatype:"json",
      type:"post",
      success:function(response){
      
          alert("Exito al Agregar La nueva Beca");
          location.reload();
 /*         $("#agregar").find(":input").attr("val","");
          if(response.beca_apoyo.tiene_saldo=='S'){
            opcion='Si';
          }else{
            opcion='No';
          }
          campo='<div class="row" style="margin-top: .8%;">'
                    +'<div class="col">'
                    +'<input type="hidden" value="'+response.beca_apoyo.id+'" name="beca_id">'
                     
                   +' <input class="form-control" readonly="" name="Descripcion" type="text" value="'+response.beca_apoyo.descripcion+'" id="Descripcion">'
                  +'</div>'
                 +'<div class="col">'
                     
                     +' <input class="form-control" readonly="" type="text" required="" value="'+response.beca_apoyo.monto_porcentaje+'" id="Porcentaje_monto" name="Porcentaje_monto">'   +' </div>'

                  +'<div class="col">'
                    
                   +' <input class="form-control" readonly="" required="" name="tiene_saldo"><option value="'+response.beca_apoyo.tiene_saldo+'">'+opcion+'</option></select>'
                  +'</div>'

                  +'<div class="col">'
                    
                                     +' <input class="form-control" readonly="" name="'+response.nombre_apoyo+'" type="text" value="No tiene saldo" id="nombre_fondo">'
                                         
                  +'</div>'
                  +'<div class="col">'
                   +' <input class="form-control" readonly="" name="orden" type="text" value="'+response.beca_apoyo.orden+'" id="orden">'
                  +'</div>'
                  +' <div class="col-3 ">'
                   +'<button class="btn btn-warning" onclick="editar('+response.beca_apoyo.id+')" type="button">Editar</button>'
                    +' <button class="btn btn-danger" type="button">Eliminar</button>'
                    +'  <button class="btn btn-success" onclick="guardar('+response.beca_apoyo.id+')" type="button">Guardar</button>'
                  +'</div>'
                    
                   
                  +'</div>';
                 $("#becas").append(campo);*/
     
      },
      error:function(){}
    });
   });


  });
      function editar(id){
        $("#"+id).find(':input').removeAttr('readonly');
    }
    function guardar(id){
      var categoria_id=id;
      var form=$("#"+id).find(":input").serialize()
      $.ajax({
        url:"{{route('beca.update')}}",
        data:form,
        datatype:"json",
        type:"post",
        success:function(response){
          if(response.status==true){
     $("#"+id).find("input").attr('readonly',true);
     $("#"+id).find("select").attr('readonly','readonly');
     alert(response.mensaje);
          }
        },
        error:function(){

        }
      });
    }
 
</script>
@endsection