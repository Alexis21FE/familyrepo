@if(isset($response))
	<nav>
		<div class="nav nav-tabs" id="nav-tab" role="tablist">
			<a class="nav-item nav-link nav-link_ active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Datos</a>
			<a class="nav-item nav-link nav-link_" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Diagnostico</a>
			<a class="nav-item nav-link nav-link_" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Citas</a>
		</div>
	</nav>

	<div class="tab-content" id="nav-tabContent">
		<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

			{{Form::open(['route'=>'paciente.store','method'=>'post','role'=>'form'])}}


			<div class="row">

				<div class="col">
					{{Form::hidden('paciente_id',$paciente->id)}}
					{{ Form::label('infante','Infante') }}
					{{Form::select('infante', ['S' => 'Si', 'N' => 'No'],$paciente->infante,['class'=>'form-control','placeholder' => 'Elija una opcion'])}}
				</div>
				<div class="col">

				</div>
				<div class="col">

				</div>
			</div>
			<div class="row" id="estutor" style="display: none;">
				<div class="col">
					{{ Form::label('nombre_tutor','Nombre Tutor') }}

					{{Form::text('nombre_tutor',$paciente->infante=='S'?$paciente->nombre_tutor:'',['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('ap_tutor','Apellido Paterno Tutor') }}
					{{Form::text('ap_tutor',$paciente->infante=='S'?$paciente->ap_tutor:'',['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('am_tutor','Apellido Materno Tutor') }}
					{{Form::text('am_tutor',$paciente->infante=='S'?$paciente->am_tutor:'',['class'=>'form-control'])}}
				</div>
			</div>
		
			<h5 style="text-align: center;margin-top: .5%" class="font-weight-light">Datos Paciente</h5>                                           
			<hr>
			<div class="row">

				<div class="col">
					{{ Form::label('name','Nombre') }}
					{{Form::text('name',$paciente->nombre,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('ap_pa','Apellido Paterno') }}
					{{Form::text('ap_pa',$paciente->apellido_pa,['class'=>'form-control'])}}
				</div>

				<div class="col">
					{{ Form::label('ap_ma','Apellido Materno') }}
					{{Form::text('ap_ma',$paciente->apellido_ma,['class'=>'form-control'])}}
				</div>
			</div>
			<div class="row">
<div class="col">
					{{ Form::label('alias','alias') }}
					{{Form::text('alias',$paciente->alias,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('correo_paciente','Correo') }}
					{{Form::email('correo_paciente', $paciente->correo_paciente, ['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('contacto_family','Contacto Family') }}
					{{Form::text('contacto_family',$paciente->contacto_family,['class'=>'form-control'])}}
				</div>
				

			</div>
			<div class="row">
<div class="col">
					{{ Form::label('celular_paciente','Celular') }}
					{{Form::text('celular_paciente',$paciente->celular_paciente,['class'=>'form-control'])}}
				</div>

				<div class="col">
					{{ Form::label('telefono_paciente','Telefono') }}
					{{Form::text('telefono_paciente',$paciente->telefono_paciente,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('colegio_','Colegio') }}
					{{Form::select('colegio_',$sedes->pluck('descripcion','id'),$paciente->colegio,['class'=>'form-control','placeholder'=>'Selecciona una sede'])}}
				</div>
				
				
			</div>
			<div class="row">
				<div class="col">
					{{ Form::label('estado_civil','Estado Civil') }}
					{{Form::select('estado_civil', ['S' => 'Soltero', 'C' => 'Casado'],$paciente->estado_civil,['class'=>'form-control','placeholder' => 'Elija un estado civil'])}}
				</div><div class="col">
					{{ Form::label('tiempo_estado_civil','Tiempo de estado civil') }}
					{{Form::text('tiempo_estado_civil',$paciente->tiempo_estado_civil,['class'=>'form-control'])}}
				</div>
				<div class="col"></div>
			</div>
			<div class="row">
				<div class="col">
					{{ Form::label('direccion_paciente','Direccion') }}
					{{Form::text('direccion_paciente',$paciente->direccion,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('numero_ext','Numero Ext.') }}
					{{Form::text('numero_ext',$paciente->numero_ext,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('numero_int','Numero Int.') }}
					{{Form::text('numero_int',$paciente->numero_int,['class'=>'form-control'])}}
				</div>

			</div>
			<div class="row">
				<div class="col">
					{{ Form::label('colonia','Colonia') }}
					{{Form::text('colonia',$paciente->colonia,['class'=>'form-control'])}}

				</div>
				<div class="col" style="margin-top:auto; "><!--button type="button" id="pass" class="btn btn-dark" data-toggle="modal" data-target=".bd-example-modal-sm">Datos Facturacion</button--></div>
			<div class="col"></div>
			<div class="col" style="margin-top:auto; ">
				{{Form::submit('guardar',['class'=>'btn btn-success'])}}
			</div>
			<div class="col"></div>
			</div>
			{{Form::close()}}
			<hr>
		</div>
		<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">


			{{Form::open(['route'=>'diagnostico.guardar','method'=>'post','role'=>'form'])}}
			{{Form::hidden('diagnostico_id',!empty($paciente->diagnosticos->id)?$paciente->diagnosticos->id:'')}}
			{{Form::hidden('consultor_diagnostico',!empty($paciente->diagnosticos->consultor)?$paciente->diagnosticos->consultor_id:'')}}
			{{Form::hidden('paciente_id',$paciente->id)}}
			<div class="row row-diag" >
				<div class="col col-diag">
					{{Form::label('razon_ingreso','Razon ingreso')}}
					<textarea name="razon_ingreso">{{($paciente->diagnosticos==null) ?'' :$paciente->diagnosticos->razon_ingreso}}</textarea>

				</div>
				

			</div>
			<hr>
			<div class="row row-diag">
				<div class="col col-diag">
					{{Form::label('Hipotesis','Hipotesis')}}
					<textarea name="diagnostico">{{!empty($paciente->diagnosticos->hipotesis)?$paciente->diagnosticos->hipotesis:''}}</textarea>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col">
					{{Form::label('tiene_medicamento','Tiene medicamento')}}
					{{Form::select('tiene_medicamento',['S'=>'Si','N'=>'No'],!empty($paciente->diagnosticos->medicamento)?$paciente->diagnosticos->medicamento:'',['class'=>'form-control','placeholder'=>'¿Tiene medicamento?'])}}
					
				</div>
				<div class="col">
					{{Form::label('causa_medicamento','Causa Medicamento')}}
					{{Form::text('causa_medicamento',!empty($paciente->diagnosticos->causa_medicamento)?$paciente->diagnosticos->causa_medicamento:'',['class'=>'form-control'])}}
				</div>
					<div class="col">
					{{Form::label('clasificacion','Clasificacion')}}
					{{Form::select('clasificacion_id',$clasificaciones->pluck('descripcion','id'),!empty($paciente->diagnosticos->causa_medicamento)?$paciente->diagnosticos->causa_medicamento:'',['class'=>'form-control','placeholder'=>'Elija una clasificacion'])}}
				</div>
				<div class="col"></div>
			</div>
			<div class="row">
				<div class="col">
					{{Form::label('Numero_sesiones','Numero de Sesiones')}}	
					{{Form::text('Numero_sesiones',!empty($paciente->diagnosticos->num_sesiones)?$paciente->diagnosticos->num_sesiones:'',['class'=>'form-control'])}}	
				</div>
				<div class="col">

					{{Form::label('consultor_diagnostico','Consultor de Diagnostico')}}	
					{{Form::select('consultor_diagnostico',$consultores->pluck('nombre','id'),!empty($paciente->diagnosticos->consultor_id)?$paciente->diagnosticos->consultor_id:'',['class'=>'form-control','placeholder' => 'Elija una opcion','readonly'=>true,'disabled'=>'true'])}}	

				</div>
				<div class="col">

					{{Form::label('consultor_asignado','Consultor asignado')}}	
					{{Form::select('consultor_asignado',$consultores->pluck('nombre','id'),!empty($paciente->diagnosticos->consultor_asig_id)?$paciente->diagnosticos->consultor_asig_id:'',['class'=>'form-control','placeholder' => 'Elija una opcion','required'=>'required'])}}	

				</div>

			</div>
			<hr>
			<div class="row">
				<div class="col">
					
				</div>
				<div class="col">
					
				</div>
				<div class="col">
					{{Form::label('guardar','Guardar Diagnostico')}}
					{{Form::submit('guardar',['class'=>'btn btn-primary'])}}
					
				</div>
			</div>
			{{Form::close()}}

			
		</div>




		<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
			<hr>
			@if(!isset($cita))
			{{Form::open(['route'=>'seguimiento.add','method'=>'post','role'=>'form'])}}
			
			@endif
			<div class="row row-diag">
				<div class="col">

					{{Form::label('fecha_actual','Fecha de Sesion')}}
				</div>
				<div class="col">

					{{Form::date('fecha',date("Y-m-d"),['class'=>'form-control','type'=>'date'])}}
				</div>
				<div class="col">
					{{Form::label('numero_sesion','No. Sesion')}}

				</div>
				<div class="col">
					{{Form::text('Numero de sesion',$consultas->count()+1,['class'=>'form-control'])}}

				</div>

			</div>
			<hr>
			<div class="row row-diag">
				<div class="col col-diag">
					{{Form::label('descripcion','descripcion')}}
					<textarea name="descripcion"></textarea>
				</div>
			</div>


			


			<hr>
			<div class="row">
				<div class="col">
					{{auth()->user()->consultor->nombre." ".auth()->user()->consultor->apellido_pa." ".auth()->user()->consultor->apellido_ma}}
				</div>
				<div class="col"></div>
				<div class="col" style="text-align: right;">
					{{Form::submit('guardar',['class'=>'btn btn-primary'])}}
				</div>
			</div>
			{{Form::close()}}
			@if(!empty($consultas))
			@foreach($consultas as $consulta)
			@php  
			$fecha=$consulta->fecha_modificacion;
			$fecha=date('Y-m-d',strtotime($fecha));
			@endphp
			<hr>
			<div style="width: 100%;" class="btn btn-primary"  onclick="verCita('{{$consulta->paciente_id.$consulta->consultor_id.$consulta->id}}')">
				Cita del {{$fecha}} {{$consulta->consultor->nombre}}
			</div>
			<div style="display: none; width:100% " class="fadequery" id="cita-{{$consulta->paciente_id.$consulta->consultor_id.$consulta->id}}">

				<div class="row row-diag">
					<div class="col">

						{{Form::label('fecha_actual','Fecha de Sesion')}}
					</div>
					<div class="col">

						{{Form::date('fecha',$fecha,['class'=>'form-control','type'=>'date'])}}
					</div>
					<div class="col">
						{{Form::label('numero_sesion','No. Sesion')}}

					</div>
					<div class="col">
						{{Form::text('Numero de sesion',$consulta->no_sesion,['class'=>'form-control'])}}

					</div>

				</div>
				<hr>
				<div class="row row-diag">
					<div class="col col-diag">
						{{Form::label('descripcion','descripcion')}}
						<textarea id="editor_ID{{$consulta->paciente_id.$consulta->consultor_id.$consulta->id}}" name="descripcion" readonly="readonly">{{$consulta->observaciones}}</textarea>
					</div>

				</div>
			</div>
		</div>
		@endforeach
		@endif
	</div>
	<script type="text/javascript">
		tinymce.init({
  selector: 'textarea',
  resize:true,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help wordcount'
  ],
  toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css']
});
		$("#infante").change(function(){
	 			var val=$("#infante").val();
	 			if(val=='S'){
	 				$("#estutor").css('display','');
	 			}else{
	 				$("#estutor").css('display','none');
	 			}
	 		});
	</script>
	@endif