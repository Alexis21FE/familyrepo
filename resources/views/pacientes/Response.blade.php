{{Form::open(['route'=>'paciente.store','method'=>'post','role'=>'form'])}}
<div id="modalEvent" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" >
	<div class="modal-dialog modal-sm" >
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="mySmallModalLabel">Datos de facturacion</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
						<div class="modal-body">
				<div class="row">
					<div class="col">{{Form::label('RFC','RFC')}} {{Form::text('RFC',empty($paciente->factura)?null:$paciente->factura->RFC,['class'=>'form-control'])}}</div>
					<div class="col">{{Form::label('facturar_a','Facturar a')}} {{Form::text('facturar_a',empty($paciente->factura)?null:$paciente->factura->facturar_a,['class'=>'form-control'])}}</div>
					<div class="col">{{Form::label('domicilio_factura','Domicilio factura')}} {{Form::text('domicilio_factura',empty($paciente->factura)?null:$paciente->factura->domicilio_factura,['class'=>'form-control'])}}</div>
				</div> 
				<div class="row">
					<div class="col">{{Form::label('cp_factura','Codigo postal de factura')}} {{Form::text('cp_factura',empty($paciente->factura)?null:$paciente->factura->cp_factura,['class'=>'form-control'])}}</div> 
					<div class="col">{{Form::label('ciudad_factura','Ciudad de factura')}} {{Form::text('ciudad_factura',empty($paciente->factura)?null:$paciente->factura->ciudad_factura,['class'=>'form-control'])}}</div> 
					<div class="col">{{Form::label('colonia_factura','Colonia factura')}} {{Form::text('colonia_factura',empty($paciente->factura)?null:$paciente->factura->colonia_factura,['class'=>'form-control'])}}</div> 
				</div>
				<div class="row">
					<div class="col">{{Form::label('uso_cfdi','Uso de cfdi')}} {{Form::text('uso_cfdi',empty($paciente->factura)?null:$paciente->factura->uso_cfdi,['class'=>'form-control'])}}</div> 
					<div class="col">
						{{Form::label('metodo_pago','Metodo de pago')}}
						{{Form::text('metodo_pago',empty($paciente->factura->metodo_pago)?'':$paciente->factura->metodo_pago,['class'=>'form-control'])}}
					</div>
					<div class="col">
						{{Form::label('forma_pago','Forma de pago')}}
						{{Form::text('forma_pago',empty($paciente->factura->forma_pago)?'':$paciente->factura->forma_pago,['class'=>'form-control'])}}
					</div>
				</div>
			</div>
			<div class="modal-footer">
				{{Form::button('cancelar',['class'=>'btn btn-danger',' data-dismiss'=>'modal'])}}
				{{Form::button('guardar',['class'=>'btn btn-success','id'=>'change_pass'])}}
			</div>
		</div>
	</div>
</div>
<div >

	<div>

		<div class="">
			<div class="row">

				<div class="col">
					{{Form::hidden('paciente_id',$paciente->id,['class'=>'form-control','id'=>'paciente_id'])}}
					{{ Form::label('infante','Infante') }}
					{{Form::select('infante', ['S' => 'Si', 'N' => 'No'],$paciente->infante,['class'=>'form-control','placeholder' => 'Elija una opcion','id'=>'infante'])}}
				</div>
				    @if(auth()->user()->hasRoles(['recep','direc']))
				<div class="col" style="margin-top:auto;">
<button type="button" class="btn btn-warning" id="Dar_de_alta">Dar de Alta</button>
				</div>
				<!--div class="col" style="margin-top:auto;">
<button type="button" class="btn btn-danger" id="Eliminar_Paciente" >Eliminar Paciente</button>
				</div-->
<div class="col" style="margin-top:auto;">
<button type="button" class="btn btn-default" id="Abandono_paciente">Abandono de Terapia</button>
				</div>
				@elseif(auth()->user()->hasRoles(['recep']))
				<div class="col">

				</div>
				<div class="col">

				</div>
<div class="col">
				@endif
			</div>
			<div class="row" style="{{$paciente->infante=='S'?' ':'display: none;'}}" id="estutor">
				<div class="col">
					{{ Form::label('nombre_tutor','Nombre Tutor') }}
					{{Form::text('nombre_tutor',$paciente->nombre_tutor,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('ap_tutor','Apellido Paterno Tutor') }}
					{{Form::text('ap_tutor',$paciente->ap_tutor,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('am_tutor','Apellido Materno Tutor') }}
					{{Form::text('am_tutor',$paciente->am_tutor,['class'=>'form-control'])}}
				</div>
			</div>

			<h5 style="text-align: center;margin-top: .5%" class="font-weight-light">Datos Paciente</h5>                                           
			<hr>
			<div class="row">

				<div class="col">
					{{ Form::label('name','Nombre*') }}
					{{Form::text('name',$paciente->nombre,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('ap_pa','Apellido Paterno') }}
					{{Form::text('ap_pa',$paciente->apellido_pa,['class'=>'form-control'])}}
				</div>

				<div class="col">
					{{ Form::label('ap_ma','Apellido Materno') }}
					{{Form::text('ap_ma',$paciente->apellido_ma,['class'=>'form-control'])}}
				</div>
			</div>
			<div class="row">

				<div class="col">
					{{ Form::label('alias','Alias (opcional)') }}
					{{Form::text('alias',$paciente->alias,['class'=>'form-control'])}}
				</div>

				<div class="col">
					{{Form::label('consultor','Consultor')}}
					{{Form::select('consultor',$consultores->pluck('nombrecompleto','id'),$paciente->consultor,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{Form::label('caso','Caso')}}
					<input class="form-control" {{($paciente->caso>=9000)?'':'readonly'}} name="caso" type="text" value="{{$paciente->caso}}" id="caso">
				
				</div>

			</div>
			<div class="row">

				<div class="col">
					{{ Form::label('celular_paciente','Celular') }}
					{{Form::text('celular_paciente',$paciente->celular_paciente,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('telefono_paciente','Telefono') }}
					{{Form::text('telefono_paciente',$paciente->telefono_paciente,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('colegio_','Colegio') }}
					{{Form::select('colegio_',$sedes->pluck('descripcion','id'),$paciente->colegio,['class'=>'form-control','placeholder'=>'Nombre del colegio que pertenece'])}}
				</div>
			</div>
			<div class="row">

				<div class="col">
					{{ Form::label('estado_civil','Estado Civil') }}
					{{Form::select('estado_civil', ['S' => 'Soltero', 'C' => 'Casado'],$paciente->estado_civil,['class'=>'form-control','placeholder' => 'Elija un estado civil'])}}
				</div>
				<div class="col">
					{{ Form::label('tiempo_estado_civil','tiempo estado civil') }}
					{{Form::text('tiempo_estado_civil',old('tiempo_estado_civil'),['class'=>'form-control'])}}
				</div>
				<div class="col">

				</div>
			</div>
			<div class="row">
				<div class="col">
					{{Form::label('direccion_paciente','Direccion') }}
					{{Form::text('direccion_paciente',$paciente->direccion,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('numero_ext','Numero Ext.') }}
					{{Form::text('numero_ext',$paciente->numero_ext,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('numero_int','Numero Int.') }}
					{{Form::text('numero_int',$paciente->numero_int,['class'=>'form-control'])}}
				</div>

			</div>
			<div class="row">
				<div class="col">

					{{ Form::label('colonia','Colonia') }}
					{{Form::text('colonia',$paciente->colonia,['class'=>'form-control'])}}
					
				</div>
					<div class="col">
						{{ Form::label('correo_paciente','Correo') }}
						{{Form::email('correo_paciente', $paciente->correo_paciente, ['class'=>'form-control'])}}
					</div>
					<div class="col">
						{{ Form::label('contacto_family','Contacto Family') }}
						{{Form::text('contacto_family',$paciente->contacto_family,['class'=>'form-control'])}}
					</div>
	</div>
	
				<hr>
				<div class="col">
					<button type="button" id="pass" class="btn btn-dark" data-toggle="modal" data-target=".bd-example-modal-sm">Datos Facturacion</button>
					</div>
				</div>



				<div style="margin-top:1.5%">
					{{Form::submit('guardar',['class'=>'btn btn-success'])}}
				</div>
			</div>
		</div>
		{{Form::close()}}
		<script type="text/javascript">
					$("#infante").change(function(){
	 			var val=$("#infante").val();
	 			if(val=='S'){
	 				$("#estutor").css('display','');
	 			}else{
	 				$("#estutor").css('display','none');
	 			}
	 		});
							$("#Dar_de_alta").click(function(){
				swal({
  title: 'Estas seguro de dar de alta este paciente',
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, dar de alta'
}).then((result) => {
  if (result.value) {
       	var paciente_id=$("#paciente_id").val();
		       	$.ajax({
		       		url:"{{route("paciente.alta")}}",
		       		datatype:"json",
		       		type:"post",
		       		data:{paciente_id:paciente_id},
		       		success:function(response){
		       			if(response.status==true){
		       				swal(response.msg);
		       				$("#gif").remove();
		       			}
		       		},
		       		error:function(){}
		       	});
  }else{
  		revertFunc();
  }
});
});
			$("#Eliminar_Paciente").click(function(){
				swal({
  title: 'Estas seguro de eliminar este paciente',
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Eliminar'
}).then((result) => {
  if (result.value) {
       	var paciente_id=$("#paciente_id").val();
		       	$.ajax({
		       		url:"{{route("paciente.Eliminar")}}",
		       		datatype:"json",
		       		type:"post",
		       		data:{paciente_id:paciente_id},
		       		success:function(response){
		       			if(response.status==true){
		       				swal(response.msg);
		       				$("#gif").remove();
		       			}
		       		},
		       		error:function(){}
		       	});
  }else{
  		revertFunc();
  }
});
		
		});
			$("#Abandono_paciente").click(function(){
				 	var paciente_id=$("#paciente_id").val();
								swal({
  title: '¿Estas seguro de que este paciente ha abandonado su tratamiento?',
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, dar de baja'
}).then((result) => {
  if (result.value) {

		       	$.ajax({
		       		url:"{{route("paciente.abandono")}}",
		       		datatype:"json",
		       		type:"post",
		       		data:{paciente_id:paciente_id},
		       		success:function(response){
		       			if(response.status==true){
		       				swal(response.msg);
		       				$("#gif").remove();
		       			}
		       		},
		       		error:function(){}
		       	});
  }else{
  		revertFunc();
  }
});
			});
		</script>