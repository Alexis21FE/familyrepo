	 @extends('../layouts.app')
	 @section('content')

	 <body>

<div id="modalEvent" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" >
	<div class="modal-dialog modal-sm" >
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="mySmallModalLabel">Datos de facturacion</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col">{{Form::label('RFC','RFC')}} {{Form::text('RFC',old('RFC'),['class'=>'form-control'])}}</div>
					<div class="col">{{Form::label('facturar_a','Facturar a')}} {{Form::text('facturar_a',old('facturar_a'),['class'=>'form-control'])}}</div>
					<div class="col">{{Form::label('domicilio_factura','Domicilio factura')}} {{Form::text('domicilio_factura',old('domicilio_factura'),['class'=>'form-control'])}}</div>
				</div> 
				<div class="row">
					<div class="col">{{Form::label('cp_factura','Codigo postal de factura')}} {{Form::text('cp_factura',old('cp_factura'),['class'=>'form-control'])}}</div> 
					<div class="col">{{Form::label('ciudad_factura','Ciudad de factura')}} {{Form::text('ciudad_factura',old('ciudad_factura'),['class'=>'form-control'])}}</div> 
					<div class="col">{{Form::label('colonia_factura','Colonia factura')}} {{Form::text('colonia_factura',old('colonia_factura'),['class'=>'form-control'])}}</div> 
				</div>
				<div class="row">
					<div class="col">{{Form::label('uso_cfdi','Uso de cfdi')}} {{Form::text('uso_cfdi',old('uso_cfdi'),['class'=>'form-control'])}}</div> 
				<div class="col">
					{{Form::label('metodo_pago','Metodo de pago')}}
					{{Form::text('metodo_pago',old('metodo_pago'),['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{Form::label('forma_pago','Forma de pago')}}
					{{Form::text('forma_pago',old('forma_pago'),['class'=>'form-control'])}}
				</div>
				</div>
			</div>
			<div class="modal-footer">
				{{Form::button('cancelar',['class'=>'btn btn-danger',' data-dismiss'=>'modal'])}}
				{{Form::button('guardar',['class'=>'btn btn-success','id'=>'change_pass'])}}
			</div>
		</div>
	</div>
</div>
	 	{{Form::open(['route'=>'paciente.add','method'=>'post','role'=>'form'])}}
	 	<div class="container" >
	 		<div >
	 			<div >

	 				<div style="align-content: center;">
	 					<h4 style="text-align: center;">Registro de nuevo Paciente</h4>

	 				</div>
	 				<hr>
	 				<div class="">
	 					<div class="row">
	 						
	 						<div class="col">

	 							{{ Form::label('infante','Infante') }}
	 							{{Form::select('infante', ['S' => 'Si', 'N' => 'No'],'N',['class'=>'form-control','placeholder' => 'Elija una opcion'])}}
	 						</div>
	 						<div class="col">
	 							
	 						</div>
	 						<div class="col">
	 							
	 						</div>
	 					</div>
	 					<div class="row" style="display: none;" id="estutor">
	 						<div class="col">
	 							{{ Form::label('nombre_tutor','Nombre Tutor') }}
	 							{{Form::text('nombre_tutor',old('name'),['class'=>'form-control'])}}
	 						</div>
	 						<div class="col">
	 							{{ Form::label('ap_tutor','Apellido Paterno Tutor') }}
	 							{{Form::text('ap_tutor',old('ap_tutor'),['class'=>'form-control'])}}
	 						</div>
	 						<div class="col">
	 							{{ Form::label('am_tutor','Apellido Materno Tutor') }}
	 							{{Form::text('am_tutor',old('am_tutor'),['class'=>'form-control'])}}
	 						</div>
	 					</div>
	 					
	 					<h5 style="text-align: center;margin-top: .5%" class="font-weight-light">Datos Paciente</h5>                                           
	 					<hr>
	 					<div class="row">

	 						<div class="col">
	 							{{ Form::label('name','Nombre') }}
	 							{{Form::text('name',old('name'),['class'=>'form-control'])}}
	 						</div>
	 						<div class="col">
	 							{{ Form::label('ap_pa','Apellido Paterno') }}
	 							{{Form::text('ap_pa',old('ap_pa'),['class'=>'form-control'])}}
	 						</div>

	 						<div class="col">
	 							{{ Form::label('ap_ma','Apellido Materno') }}
	 							{{Form::text('ap_ma',old('ap_ma'),['class'=>'form-control'])}}
	 						</div>
	 					</div>
	 					<div class="row">

	 						<div class="col">
	 							{{ Form::label('alias','Alias (opcional)') }}
	 							{{Form::text('alias',old('alias'),['class'=>'form-control'])}}
	 						</div>
	 						<div class="col">
	 							{{Form::label('consultor','Consultor')}}
	 							{{Form::select('consultor',$consultores->pluck('nombrecompleto','id'),null,['class'=>'form-control'])}}
	 						</div>
	 						<div class="col">
	 							{{Form::label('caso','Caso')}}
	 							{{Form::text('caso',$caso,['class'=>'form-control','readonly'=>'readonly'])}}
	 						</div>
	 						

	 					</div>
	 					<div class="row">
	 						
	 						<div class="col">
	 							{{ Form::label('celular_paciente','Celular') }}
	 							{{Form::text('celular_paciente',old('celular_paciente'),['class'=>'form-control'])}}
	 						</div>
	 						<div class="col">
	 							{{ Form::label('telefono_paciente','Telefono') }}
	 							{{Form::text('telefono_paciente',old('telefono_paciente'),['class'=>'form-control'])}}
	 						</div>
	 						<div class="col">
	 							{{ Form::label('colegio_','Colegio') }}
	 							{{Form::select('colegio_',$sedes->pluck('descripcion','id'),null,['class'=>'form-control','placeholder'=>'Nombre del colegio que pertenece','required'=>true])}}
	 						</div>
	 					</div>
	 					<div class="row">
	 						
	 						<div class="col">
	 							{{ Form::label('estado_civil','Estado Civil') }}
	 							{{Form::select('estado_civil', ['S' => 'Soltero', 'C' => 'Casado'],null,['class'=>'form-control','placeholder' => 'Elija un estado civil'])}}
	 						</div>
	 						<div class="col">
	 							{{ Form::label('tiempo_estado_civil','tiempo estado civil') }}
	 							{{Form::text('tiempo_estado_civil',old('tiempo_estado_civil'),['class'=>'form-control'])}}
	 						</div>
	 						<div class="col">
	 							
	 						</div>
	 					</div>
	 					<div class="row">
	 						<div class="col">
	 							{{ Form::label('direccion_paciente','Direccion') }}
	 							{{Form::text('direccion_paciente',old('direccion_paciente'),['class'=>'form-control'])}}
	 						</div>
	 						<div class="col">
	 							{{ Form::label('numero_ext','Numero Ext.') }}
	 							{{Form::text('numero_ext',old('numero_ext'),['class'=>'form-control'])}}
	 						</div>
	 						<div class="col">
	 							{{ Form::label('numero_int','Numero Int.') }}
	 							{{Form::text('numero_int',old('numero_int'),['class'=>'form-control'])}}
	 						</div>

	 					</div>
	 					<div class="row">
	 						<div class="col">
	 							{{ Form::label('colonia','Colonia') }}
	 							{{Form::text('colonia',old('Colonia'),['class'=>'form-control'])}}
	 						</div>
	 						<div class="col">
	 							{{ Form::label('correo_paciente','Correo') }}
	 							{{Form::email('correo_paciente', null, ['class'=>'form-control'])}}
	 						</div>
	 						<div class="col">
	 							{{ Form::label('contacto_family','Contacto Family') }}
	 							{{Form::text('contacto_family',old('contacto_family'),['class'=>'form-control'])}}
	 						</div>
	 						

	 					</div>
	 					<hr>
	 					<div class="col">
	 					<button type="button" id="pass" class="btn btn-dark" data-toggle="modal" data-target=".bd-example-modal-sm">Datos Facturacion</button>
	 					</div>
	 				</div>



	 				<div style="margin-top:1.5%">
	 					{{Form::submit('guardar',['class'=>'btn btn-success'])}}
	 					<button type="button" class="btn" id="close_modal" data-dismiss="modal">Cancelar</button>
	 				</div>
	 			</div>
	 		</div>
	 	</div>
	 	{{Form::close()}}
	 </body>
	 <script>
	 	$(document).ready(function(){
	 		$("#infante").change(function(){
	 			var val=$("#infante").val();
	 			if(val=='S'){
	 				$("#estutor").css('display','');
	 			}else{
	 				$("#estutor").css('display','none');
	 			}
	 		});
	 	});
	 </script>
	 @endsection