@extends('../layouts.app')
@section('content')
<div class="container">
  <div id="modalEvent" class="modal" tabindex="-1" data-backdrop="static" >
	 {{Form::hidden('cita_id',null,['type'=>'hidden','id'=>'cita_id'])}}
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <h4>Detalle de la cita</h4>

        </div>
        <div class="modal-body">
          <div class="row">

            <div class="col">

              {{ Form::label('consultor','consultor') }}
              <select id ='consultor_'   class="form-control" name="consultor" required="true" >
                <option value="" disabled selected hidden>Elija un consultor</option>
                @foreach($consultores as $consultor)
                <option value="{{$consultor->id}}">{{$consultor->nombre." ".$consultor->apellido_pa." ".$consultor->apellido_ma }}</option>
                @endforeach
              </select>
            </div>
            <div class="col">
              {{ Form::label('paciente_','Paciente') }}
              {{Form::text('paciente_',old('paciente_'),['class'=>'form-control', 'readonly'=>'true'])}}
            </div>

            <div class="col">
              {{ Form::label('tipo_cita_', 'Tipo de Cita') }}
              {{ Form::select('tipo_cita_', $tipo_citas, null, ['id' => 'tipo_cita_',    'class'=>'form-control multiple ']) }}
            </div>
          </div>
          <div class="row">
            <div class="col">
              {{ Form::label('date_start_','Fecha Cita') }}
              {{Form::text('date_start_',old('date_start_'),['class'=>'form-control','type'=>'date'])}}
            </div>

            <div class="col">
              {{Form::label('time_start_','Hora inicio')}}
              {{Form::time('time_start_',null,['class'=>'form-control'])}}

            </div>
            <div class="col">
              {{Form::label('date_end_','Hora fin')}}
              {{Form::time('date_end_',null,['class'=>'form-control'])}}
            </div>

          </div>
          <div class="row">
            <div class="col">
             {{ Form::label('consultorio_','Consultorios') }}
             {{ Form::select('consultorio_', $consultorios, null, ['id' => 'consultorio_',    'class'=>'form-control multiple ']) }}
           </div>
           <div class="col">
            {{ Form::label('ind_par','Individual Pareja/Familiar') }}
            {{Form::select('individual_pareja_', ['Individual' => 'Individual', 'Pareja' => 'Pareja/Familiar'],null,['class'=>'form-control','placeholder' => 'Elija una opcion','id'=>'individual_pareja_'])}}
          </div>
          <div class="col">
            {{ Form::label('status_cita_','Status Cita') }}
            {{ Form::select('status_cita_', $status->pluck('descripcion','id'), null, ['id' => 'status_cita_',    'class'=>'form-control multiple ']) }}
            {{Form::label('texto_pagada','Fecha de pago ',['style'=>'display:none;','id'=>'text_cita_pagada'])}}
          </div>

        </div>
        <div class="row">

          <div class="col">
           {{ Form::label('becas','Becas') }}
           {{ Form::select('becas_descuentos', $becas_descuentos->pluck('descripcion','id'), null, ['id' => 'beca_descuento_',    'class'=>'form-control ','placeholder' => 'Elija una beca/descuento']) }}
         </div>
         <div class="col">
          {{ Form::label('becas','Porcentaje o Descuento') }}
          {{Form::text('porcentaje',old('porcentaje'),['class'=>'form-control','id' => 'beca_porcentaje_' ])}}

        </div>
        <div class="col" style="margin: auto; margin-top: 3.5%; text-align: center;">
          {!! Html::decode(Form::button('<i class="fas fa-plus-circle "></i>Agregar Beca',['class' => 'btn btn-success','id'=>'agregarBeca_'])) !!}
        </div>
      </div>
      <hr>
      <div id="Becas_agregadas_">
        {{Form::hidden('costo__',null,['type'=>'hidden','id'=>'costo_ini'])}}
        @for($x=1;$x<=$becas_descuentos->max('orden');$x++)
        <div id="orden_{{$x}}"></div>
        @endfor


      </div>
      <div class="row">
        <div class="col">
         {{ Form::label('costo_','Costo Total') }}
         {{ Form::text('costo_final',null,['id' => 'costo_final_','class'=>'form-control','readonly'=>'readonly']) }}
       </div>
       <div class="col">
         {{Form::label('observaciones','Observaciones')}}
         {{Form::text('observaciones',null,['id'=>'observaciones','class'=>'form-control'])}}
       </div>

     </div>         


   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-danger" id="close_modal" data-dismiss="modal">Cancelar</button>
    {{Form::submit('guardar',['class'=>'btn btn-primary','id'=>'updateCita'])}}
  </div>
</div>
</div>
</div>


@foreach ($citas as $cita)
<div class="row">
<div class="col">
{{Form::label('Paciente','Paciente')}}
{{Form::text('paciente',!isset($cita->paciente->alias)?$cita->paciente->alias:$cita->paciente->nombrecompleto,['class'=>'form-control','readonly'=>'readonly'])}}
	
</div>	
<div class="col">
{{Form::label('Consultor','Consultor')}}
{{Form::text('Consultor',$cita->consultor->nombre,['class'=>'form-control','readonly'=>'readonly'])}}
	
</div>	
<div class="col">
{{Form::label('precio','Precio')}}
{{Form::text('precio',$cita->monto_final,['class'=>'form-control','readonly'=>'readonly'])}}
	
</div>	
<div class="col">
{{Form::label('status_cita','Status Cita')}}
{{Form::select('status_cita',$status->pluck('descripcion','id'),$cita->status_cita_id,['class'=>'form-control','readonly'=>'readonly'])}}
	
</div>	
<div class="col" style="margin-top: auto;">
 {{Form::button('Editar',['class'=>'btn btn-warning','onClick'=>'editar('.$cita->id.')'])}}
      {{Form::button('guardar',['class'=>'btn btn-danger','onClick'=>'guardar('.$cita->id.')'])}}
</div>	
</div>
@endforeach
</div>
<script type="text/javascript">
	function editar(cita_id){
		$.ajax({
			url:"{{route('cita.response')}}",
			datatype:"json",
			type:"post",
			data:{cita_id:cita_id},
			success:function(response){
				                        var date_start = $.fullCalendar.moment(event.start).format('YYYY-MM-DD');
                        var time_start = $.fullCalendar.moment(event.start).format('HH:mm');
                        var date_end = $.fullCalendar.moment(event.end).format('HH:mm');
                        //$("#modalEvent #title").val(response.event.descripcion);
                        $("#modalEvent #date_start_").val(date_start);
                        $("#modalEvent #time_start_").val(time_start);
                        $("#modalEvent #date_end_").val(date_end);
                        $("#consultor_").val(response.event.consultor_id);
                        $("#paciente_").val(response.event.paciente.nombre+' '+response.event.paciente.apellido_pa+' '+response.event.paciente.apellido_ma);
                        $("#tipo_cita_").val(response.event.tipo_cita_id);
                        $("#consultorio_").val(response.event.consultorio_id);
                        $("#individual_pareja_").val(response.event.individual_pareja);
                        $("#costo_final_").val(response.event.monto_final);
                        $("#costo_ini").val(response.event.monto_inicial);
                        $("#cita_id").val(response.event.id);
                        $("#Becas_agregadas_").children('div').empty();
                        if(response.event.status_cita.descripcion=='pagada'){
                          $("#modalEvent").find('input').attr('readonly',true);
                          $("#modalEvent").find('select').attr('disabled',true);
                          $("#text_cita_pagada").css('display','block');
                          $("#text_cita_pagada").append(response.event.fecha_pago);
                        }else{
                          $("#modalEvent").find('input').attr('readonly',false);
                          $("#modalEvent").find('select').attr('disabled',false);
                        }
                        $("#costo_final_").attr('readonly',true);
                        $("#paciente_").attr('readonly',true);
                        $.each(response.event.becas,function(key,value){
                          $.each(value,function(ke,val){
                           porcentaje=val.cantidad;
                           if(porcentaje<1){
                            porcentaje=porcentaje*100;
                          }
                        });
                          if(value.monto_porcentaje!='monto'){ 
                           texto_beca_descuento='Porcentaje de Beca';
                           text_beca='porcentaje';
                           position='porcentaje';

                         }else{
                //costo_aux=costo-porcentaje;
                texto_beca_descuento='Monto del descuento';
                text_beca='monto';
                position='monto';
                
              }
              nextinput=nextinput+1;

              campo = '<div class="row" id="eliminarRow'+text_beca+''+nextinput+'">'+

              '<div class="col">'+

              '<label for="'+text_beca+nextinput+'">Becas</label>'+
              '</div>'+
              '<div class="col">'+

              ' <input id="'+text_beca+nextinput+'"name="orden'+value.orden+'[]" type="hidden" value="'+value.id+'">'+

              ' <input id="'+text_beca+nextinput+'"name="becas['+nextinput+'][beca_id]" type="hidden" value="'+value.id+'">'+            
              ' <input id="'+text_beca+nextinput+'"name="becas['+nextinput+'][monto_porcentaje]" type="hidden" value="'+value.monto_porcentaje+'">'+            
              ' <input id="'+text_beca+nextinput+'"name="becas['+nextinput+'][afecta]" type="hidden" value="'+value.afecta+'">'+

              ' <input id="'+text_beca+nextinput+'" class="form-control" readonly="readonly" name="x" type="text" value="'+value.descripcion+'">'+
              ' </div>'+

              ' <div class="col">'+

              ' <label for="'+text_beca+''+nextinput+'">'+texto_beca_descuento+'</label>'+
              ' </div>'+
              ' <div class="col">'+
              '<input class="form-control" id="'+text_beca+''+nextinput+'" name="becas['+nextinput+'][cantidad]" type="text" value="'+porcentaje+'" readonly>'+

              ' </div>'+
              ' <div class="col">'+
              '{!! Html::decode(Form::button('<i class="fas fa-minus-circle "></i>Eliminar Beca',['class' => 'btn btn-danger','id'=>'eliminarBeca_'])) !!}'+
              ' </div>'+
              '</div>';
              $("#orden_"+value.orden).empty();
              $("#orden_"+value.orden).append(campo);
              $("#eliminarBeca_").prop('id','eliminarBeca_'+text_beca+''+nextinput);
              $("#eliminarBeca_"+text_beca+""+nextinput).attr('onclick','eliminarBeca_("eliminarRow'+text_beca+''+nextinput+'")');

            });
                        $("#status_cita_").val(response.event.status_cita_id);
                            //$("#modalEvent #title").val();
                            $("#modalEvent").modal('show');
                          
			},
			error:function(){}
		});
	}
</script>
@endsection