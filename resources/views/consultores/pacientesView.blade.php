@extends('../layouts.app')
@section('content')
<div class="container">
	<select class="form-control " style="width: 30%;" id="select_Paciente">

		  <option value="" disabled selected hidden>Elija un paciente</option>
		@if(!empty($pacientes))
		@foreach($pacientes as $paciente)
		<option value="{{$paciente->id}}">

			{{!empty($paciente->alias)?$paciente->alias." - Pac. ".$paciente->nombreconsultor:$paciente->nombre." - Pac. ".$paciente->nombreconsultor}}
			
		</option>
		@endforeach
		@endif
		
	</select>
	<hr>
<div id="response">
</div>

</div>
<script type="text/javascript">
	$(document).ready(function(){

		$("#select_Paciente").change(function(){
			var paciente_id=$("#select_Paciente").val();
			$.ajax({
				url:"{{route('test.paciente')}}",
				data:{
					paciente_id:paciente_id
				},
				datatype:'json',
				type:'post',
				success:function(response){
					$("#bar").css("width","100%");
					$("#response").empty();
					
					setTimeout($("#response").html(response),10000);
					$("#gif").remove();
						tinymce.remove('textarea');
				},
				error:function(){},

			});
		});
		

		$("#select_Paciente").change(function(){
			var id=$(this).val();
		});

	});


	function verCita(seguimiento_id,cerrar){
		event.preventDefault();

		id_cursot=seguimiento_id;
		if(cerrar==1){	
		tinyMCE.get('editor_ID'+id_cursot).setMode('readonly');
		}

		tdd="cita-".concat(id_cursot);
		$is_show=$("#"+tdd).css("display");
		if($is_show!="none"){
			$(".fadequery").css("display","none");   
		}else{
			$(".fadequery").css("display","none");
			$("#"+tdd).css("display","");
		}
	}

</script>
@endsection