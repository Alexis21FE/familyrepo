@if(isset($response))
	
	<nav>
		<div class="nav nav-tabs" id="nav-tab" role="tablist">
			<a class="nav-item nav-link nav-link_ active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Datos</a>
			<a class="nav-item nav-link nav-link_" id="nav-familia-tab" data-toggle="tab" href="#nav-familia" role="tab" aria-controls="nav-familia" aria-selected="false">Composicion Familiar</a>
			<a class="nav-item nav-link nav-link_" id="nav-Datos-tab" data-toggle="tab" href="#nav-Datos" role="tab" aria-controls="nav-Datos" aria-selected="false">Datos Significativos Recientes</a>
			<a class="nav-item nav-link nav-link_" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Diagnostico</a>
			<a class="nav-item nav-link nav-link_" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Citas</a>
		</div>
	</nav>

	<div class="tab-content" id="nav-tabContent">
		<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

			{{Form::open(['route'=>'paciente.store','method'=>'post','role'=>'form','id'=>'form_paciente'])}}

			{{Form::hidden('paciente_id',$cita->paciente->id,['id'=>'paciente_id'])}}
			<div class="row">

				<div class="col">

					{{ Form::label('infante','Infante') }}
					{{Form::select('infante', ['S' => 'Si', 'N' => 'No'],empty($cita->paciente->infante)?'N':$cita->paciente->infante,['class'=>'form-control','placeholder' => 'Elija una opcion','id'=>'infante_select'])}}
				</div>
				<div class="col" style="margin-top: auto;">
					{{Form::button('Dar de alta paciente',['class'=>'btn btn-warning','id'=>'Dar_de_alta'])}}
				</div>
				<div class="col" style="margin-top: auto;">
<button type="button" class="btn btn-warning" id="Abandono_paciente">Abandono de Terapia</button>
				</div>


				<div class="col">

				</div>
			</div>

		

	<div class="row" style="{{($cita->paciente->infante=='S')?:'display:none;'}}" id="estutor">
				<div class="col">
					{{ Form::label('nombre_tutor','Nombre Tutor') }}

					{{Form::text('nombre_tutor',empty($cita->paciente->nombre_tutor)?'':$cita->paciente->nombre_tutor,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('ap_tutor','Apellido Paterno Tutor') }}
					{{Form::text('ap_tutor',empty($cita->paciente->ap_tutor)?'':$cita->paciente->ap_tutor,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('am_tutor','Apellido Materno Tutor') }}
					{{Form::text('am_tutor',empty($cita->paciente->am_tutor)?'':$cita->paciente->am_tutor,['class'=>'form-control'])}}
				</div>
			</div>
		
			<h5 style="text-align: center;margin-top: .5%" class="font-weight-light">Datos Paciente</h5>                                           
			<hr>
			<div class="row">
				<div class="col">
					{{ Form::label('name','Nombre') }}
					{{Form::text('name',!empty($cita->paciente->alias)?$cita->paciente->alias:$cita->paciente->nombre,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('ap_pa','Apellido Paterno') }}
					{{Form::text('ap_pa',!empty($cita->paciente->apellido_pa)?$cita->paciente->apellido_pa:'',['class'=>'form-control'])}}
				</div>

				<div class="col">
					{{ Form::label('ap_ma','Apellido Materno') }}
					{{Form::text('ap_ma',!empty($cita->paciente->apellido_ma)?$cita->paciente->apellido_ma:'',['class'=>'form-control'])}}
				</div>
			</div>
			<div class="row">

				<div class="col">
					{{ Form::label('correo_paciente','Correo') }}
					{{Form::email('correo_paciente', $cita->paciente->correo_paciente, ['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('contacto_family','Contacto Family') }}
					{{Form::text('contacto_family',$cita->paciente->contacto_family,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('celular_paciente','Celular') }}
					{{Form::text('celular_paciente',$cita->paciente->celular_paciente,['class'=>'form-control'])}}
				</div>

			</div>
			<div class="row">


				<div class="col">
					{{ Form::label('telefono_paciente','Telefono') }}
					{{Form::text('telefono_paciente',$cita->paciente->telefono_paciente,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('estado_civil','Estado Civil') }}
					{{Form::select('estado_civil', ['S' => 'Soltero', 'C' => 'Casado'],$cita->paciente->estado_civil,['class'=>'form-control','placeholder' => 'Elija un estado civil'])}}
				</div>
				<div class="col">
					{{Form::label('fecha_registro','Fecha de registro')}}
					{{Form::text('fecha_registro',$cita->paciente->fecha_registro,['class'=>'form-control','disabled'=>true])}}
				</div>
			</div>

			<div class="row">
				<div class="col">
					{{ Form::label('direccion_paciente','Direccion') }}
					{{Form::text('direccion_paciente',$cita->paciente->direccion,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('numero_ext','Numero Ext.') }}
					{{Form::text('numero_ext',$cita->paciente->numero_ext,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{ Form::label('numero_int','Numero Int.') }}
					{{Form::text('numero_int',$cita->paciente->numero_int,['class'=>'form-control'])}}
				</div>

			</div>
			<div class="row">
				<div class="col">
					{{Form::label('tiene_medicamento','Tiene medicamento')}}
					{{Form::select('tiene_medicamento',['S'=>'Si','N'=>'No'],!empty($cita->paciente->diagnosticos->medicamento)?$cita->paciente->diagnosticos->medicamento:'',['class'=>'form-control','placeholder'=>'¿Tiene medicamento?'])}}
					
				</div>
				<div class="col">
					{{Form::label('causa_medicamento','Causa Medicamento')}}
					{{Form::text('causa_medicamento',!empty($cita->paciente->diagnosticos->causa_medicamento)?$cita->paciente->diagnosticos->causa_medicamento:'',['class'=>'form-control'])}}
				</div>
				<div class="col" style="margin-top: auto;">
					{{Form::button('Guardar',['class'=>'btn btn-success','id'=>'datos_paciente'])}}
				</div>
			</div>
			{{Form::close()}}
			<hr>
		</div>
		<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

			{{Form::open(['route'=>'diagnostico.guardar','method'=>'post','role'=>'form','id'=>'form_diagnostico'])}}
			{{Form::hidden('diagnostico_id',empty($cita->paciente->diagnosticos->id)?'':$cita->paciente->diagnosticos->id)}}
			{{Form::hidden('consultor_diagnostico',$cita->consultor_id)}}
			{{Form::hidden('paciente_id',$cita->paciente_id)}}
			<div class="row row-diag" >
				<div class="col col-diag">
					{{Form::label('razon_ingreso','Razon ingreso')}}
					<textarea name="razon_ingreso">{{!empty($cita->paciente->diagnosticos->razon_ingreso)?$cita->paciente->diagnosticos->razon_ingreso:''}}</textarea>

				</div>
				

			</div>
			<hr>
			<div class="row row-diag">
				<div class="col col-diag">
					{{Form::label('Hipotesis','Hipotesis')}}
					<textarea name="diagnostico">{{!empty($cita->paciente->diagnosticos->hipotesis)?$cita->paciente->diagnosticos->hipotesis:''}}</textarea>
				</div>
			</div>
			<hr>
			<div class="row">
				
				<div class="col">
					{{Form::label('clasificacion','Clasificacion')}}
					{{Form::select('clasificacion_id',$clasificaciones->pluck('descripcion','id'),!empty($cita->paciente->diagnosticos->clasificacion_id)?$cita->paciente->diagnosticos->clasificacion_id:'',['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{Form::label('Numero_sesiones','Numero de Sesiones')}}	
					{{Form::text('Numero_sesiones',!empty($cita->paciente->diagnosticos->num_sesiones)?$cita->paciente->diagnosticos->num_sesiones:'',['class'=>'form-control'])}}	
				</div>
				<div class="col">

					{{Form::label('consultor_diagnostico','Consultor de Diagnostico')}}	
					{{Form::select('consultor_diagnostico',$consultores->pluck('nombrecompleto','id'),$cita->consultor_id,['class'=>'form-control','placeholder' => 'Elija una opcion','readonly'=>true,'disabled'=>'true'])}}	

				</div>
				<div class="col">

					{{Form::label('consultor_asignado','Consultor asignado')}}	
					{{Form::select('consultor_asignado',$consultores->pluck('nombrecompleto','id'),$cita->paciente->diagnosticos->consultor_asig_id,['class'=>'form-control','placeholder' => 'Elija una opcion','required'=>'required'])}}	

				</div>

			</div>
			<hr>
			<div class="row">
				<div class="col">
					
				</div>
				<div class="col">
					
				</div>
				<div class="col">
					{{Form::label('guardar','Guardar Diagnostico')}}
					{{Form::button('Guardar',['class'=>'btn btn-primary','id'=>'guardar_diagnostico'])}}
					
				</div>
			</div>
			{{Form::close()}}

			
		</div>


		<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
			<hr>
			{{Form::open(['route'=>'seguimiento.add','method'=>'post','role'=>'form','id'=>'form0'])}}
			@php
				
			@endphp
			{{Form::hidden('cita_id',$last_date->id)}}
			{{Form::hidden('paciente_id',$cita->paciente_id)}}
			{{Form::hidden('consultor_id',$cita->consultor_id)}}
			{{Form::hidden('caso_id',$cita->paciente->caso)}}
			<div class="row row-diag">
				<div class="col">

					{{Form::label('fecha_actual','Fecha de Sesion')}}
				</div>
				<div class="col">

					{{Form::date('fecha',date("Y-m-d"),['class'=>'form-control','type'=>'date'])}}
				</div>
				<div class="col">
					{{Form::label('numero_sesion','No. Sesion')}}

				</div>
				<div class="col">
					{{Form::text('Numero de sesion',$consultas->count()+1,['class'=>'form-control'])}}

				</div>

			</div>
			<hr>
			<div class="row row-diag">
				<div class="col col-diag">
					{{Form::label('descripcion','descripcion')}}
					<textarea name="descripcion" id="descripcion"></textarea>
				</div>
			</div>


			


			<hr>
			<div class="row">
				<div class="col">
					{{$cita->consultor->nombre." ".$cita->consultor->apellido_pa." ".$cita->consultor->apellido_ma}}
				</div>
				<div class="col" style="text-align: right;">
				{{Form::label('cerrar','Cerrar Sesion')}}
				{{Form::checkbox('Cerrar','1',0)}}</div>
				<div class="col" style="text-align: left;">
					{{Form::button('Guardar',['class'=>'btn btn-primary','id'=>'guardar_session'])}}
				</div>
			</div>
			{{Form::close()}}
			@if(!empty($consultas))
			@foreach($consultas as $consulta)
			@php  
			$fecha=$consulta->fecha_modificacion;
			$fecha=date('Y-m-d',strtotime($fecha));
			@endphp
			<hr>
			<div style="width: 100%;" class="btn btn-primary"  onclick="verCita('{{$consulta->paciente_id.$consulta->consultor_id.$consulta->id}}','{{$consulta->cerrar}}')">
				Cita del {{$fecha}} {{$consulta->paciente->nombre}} No. Sesion {{$consulta->no_sesion}}
			</div>
			<div style="display: none; width:100% " class="fadequery" id="cita-{{$consulta->paciente_id.$consulta->consultor_id.$consulta->id}}">
				{{Form::open(['route'=>'seguimiento.add','method'=>'post','role'=>'form','id'=>'form-'.$consulta->id])}}
			{{Form::hidden('session_id',$consulta->id)}}
						{{Form::hidden('paciente_id',$consulta->paciente_id)}}
			{{Form::hidden('consultor_id',$consulta->consultor_id)}}
			{{Form::hidden('caso_id',$consulta->paciente->caso)}}
				<div class="row row-diag">
					<div class="col">

						{{Form::label('fecha_actual','Fecha de Sesion')}}
					</div>
					<div class="col">

						{{Form::date('fecha',$fecha,['class'=>'form-control','type'=>'date','readonly'=>'readonly'])}}
					</div>
					<div class="col">
						{{Form::label('numero_sesion','No. Sesion')}}

					</div>
					<div class="col">
						{{Form::text('Numero de sesion',$consulta->no_sesion,['class'=>'form-control','readonly'=>'readonly'])}}

					</div>

				</div>
				<hr>
				<div class="row row-diag">
					<div class="col col-diag">
						{{Form::label('descripcion','descripcion')}}
						<textarea {{($consulta->cerrar!='')?'readonly':''}} id="editor_ID{{$consulta->paciente_id.$consulta->consultor_id.$consulta->id}}" name="descripcion"> {{$consulta->observaciones}}</textarea>
					</div>

				</div>
				<div class="row">
				<div class="col">
					{{$consulta->consultor->nombre." ".$consulta->consultor->apellido_pa." ".$consulta->consultor->apellido_ma}}
				</div>
				<div class="col" style="text-align: right;">
				{{Form::label('cerrar','Cerrar Sesion')}}
				{{Form::checkbox('Cerrar','1',($consulta->cerrar!='')?$consulta->cerrar:'0')}}</div>
				<div class="col" style="text-align: left;">
					{{Form::button('Guardar',['class'=>'btn btn-primary','onClick'=>'actualizar_session('.$consulta->id.')'])}}
				</div>
			</div>
			{{Form::close()}}
			</div>
		

		@endforeach
		@endif
	</div>
		<div class="tab-pane fade" id="nav-familia" role="tabpanel" aria-labelledby="nav-familia-tab">
				<div id="div_fam">
				
			@if(!empty($familiares))
			@foreach($familiares as $familiar)
			<div class="row" id="{{$familiar->id}}">
				<div class="col">
					{{Form::hidden('paciente_id',$familiar->paciente_id)}}
					{{Form::hidden('caso_id',$cita->id)}}
					{{Form::hidden('familiar_id',$familiar->id)}}
					{{Form::label('nombre','Nombre')}}
					{{Form::text('nombre',$familiar->nombre,['class'=>'form-control','readonly'=>'readonly'])}}
				</div>
				<div class="col">
					{{Form::label('sexo','Sexo')}}
					{{Form::text('sexo',$familiar->sexo,['class'=>'form-control','readonly'=>'readonly'])}}
				</div>
				<div class="col">
					{{Form::label('edad','Edad')}}
					{{Form::text('edad',$familiar->edad,['class'=>'form-control','readonly'=>'readonly'])}}
				</div>
				<div class="col">
					{{Form::label('Parentesco','Parentesco')}}
					{{Form::text('Parentesco',$familiar->parentesco,['class'=>'form-control','readonly'=>'readonly'])}}
				</div>
				<div class="col-3" style="    margin-top: auto;">
					{{Form::button('Guardar',['class'=>'btn btn-success','onClick'=>'guardar('.$familiar->id.')'])}}
					{{Form::button('Editar',['class'=>'btn btn-warning','onClick'=>'editar('.$familiar->id.')'])}}
					{{Form::button('Eliminar',['class'=>'btn btn-danger','onClick'=>'eliminar('.$familiar->id.')'])}}
				</div>
			
			</div>
			@endforeach
			@endif
			</div>

			{{Form::open(['route'=>'paciente.familiar','method'=>'post','role'=>'form','id'=>'formulario'])}}
			<div class="row" >
				<div class="col">
					{{Form::hidden('paciente_id',$cita->paciente_id)}}
					{{Form::hidden('caso_id',$cita->id)}}
					{{Form::label('nombre','Nombre')}}
					{{Form::text('nombre',null,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{Form::label('sexo','Sexo')}}
					{{Form::select('sexo',['F'=>'Femenino','M'=>'Masculino'],null,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{Form::label('edad','Edad')}}
					{{Form::text('edad',null,['class'=>'form-control'])}}
				</div>
				<div class="col">
					{{Form::label('Parentesco','Parentesco')}}
					{{Form::text('Parentesco',null,['class'=>'form-control'])}}
				</div>
				<div class="col-3" style="    margin-top: auto;">
					{{Form::button('Agregar',['class'=>'btn btn-success','id'=>'agregar_paciente'])}}
				</div>
			
			</div>
			{{Form::close()}}
		</div>
		<div class="tab-pane fade" id="nav-Datos" role="tabpanel" aria-labelledby="nav-Datos-tab">
			{{Form::open(['route'=>'paciente.datos_sig','method'=>'post','role'=>'form','id'=>'form-datos-sig'])}}
			<div class="row">
				{{Form::hidden('paciente_id',$cita->paciente_id)}}
					{{Form::hidden('caso_id',0)}}
				<div class="col">{{Form::label('Cambio de residencia','Cambio de residencia')}}
					{{Form::checkbox('Cambio_de_residencia', '1',isset($datos_significativos->Cambio_de_residencia)?$datos_significativos->Cambio_de_residencia:'0')}}
				</div>
				<div class="col">{{Form::label('accidentes','Accidentes')}}
					{{Form::checkbox('accidentes', '1',isset($datos_significativos->Accidentes)?$datos_significativos->Accidentes:'0')}}
				</div>
				<div class="col">{{Form::label('Muertes','Muertes')}}
					{{Form::checkbox('Muertes', '1',isset($datos_significativos->Muertes)?$datos_significativos->Muertes:'0')}}
				</div>
				<div class="col">{{Form::label('Separacion','Separacion')}}
					{{Form::checkbox('separacion', '1',isset($datos_significativos->Separacion)?$datos_significativos->Separacion:'0')}}
				</div>
			</div>
		<div class="row">
				<div class="col">{{Form::label('Divorcio','Divorcio')}}
					{{Form::checkbox('divorcio', '1',isset($datos_significativos->Divorcio)?$datos_significativos->Divorcio:'0')}}
				</div>
				<div class="col">{{Form::label('Escolar','Escolar')}}
					{{Form::checkbox('escolar', '1',isset($datos_significativos->Escolar)?$datos_significativos->Escolar:'0')}}
				</div>
				<div class="col">{{Form::label('Perdida_empleo','Perdida empleo')}}
					{{Form::checkbox('perdida_empleo', '1',isset($datos_significativos->Perdida_empleo)?$datos_significativos->Perdida_empleo:'0')}}
				</div>
				<div class="col">{{Form::label('Enfermedades','Enfermedades')}}
					{{Form::checkbox('enfermedades', '1',isset($datos_significativos->Enfermedades)?$datos_significativos->Enfermedades:'0')}}
				</div>
			</div>
			<div class="row">
				<div class="col">{{Form::label('Economico','Economico')}}
					{{Form::checkbox('economico', '1',isset($datos_significativos->Economico)?$datos_significativos->Economico:'0')}}
				</div>
				<div class="col">{{Form::label('Asalto','Asalto')}}
					{{Form::checkbox('asalto', '1',isset($datos_significativos->Asalto)?$datos_significativos->Asalto:'0')}}
				</div>
				<div class="col">{{Form::label('carcel','Carcel')}}
					{{Form::checkbox('carcel', '1',isset($datos_significativos->Carcel)?$datos_significativos->Carcel:'0')}}
				</div>
				<div class="col">{{Form::label('Embarazo','Embarazo')}}
					{{Form::checkbox('embarazo', '1',isset($datos_significativos->Embarazo)?$datos_significativos->Embarazo:'0')}}
				</div>
			</div>
				<div class="row">
				<div class="col">{{Form::label('Genetico','Genetico')}}
					{{Form::checkbox('genetico', '1',isset($datos_significativos->Genetico)?$datos_significativos->Genetico:'0')}}
				</div>
				<div class="col">{{Form::label('Otros','Otros')}}
					{{Form::checkbox('otros', '1',isset($datos_significativos->Otros)?$datos_significativos->Otros:'0')}}
				</div>
				<div class="col">
					{{Form::button('Guardar',['class'=>'btn btn-success','id'=>'datos_sig'])}}
				</div>
				<div class="col">
				</div>
			</div>
			{{Form::close()}}
		</div>
		
	<script type="text/javascript">

$(document).ready(function(){
 		tinymce.init({
  selector: 'textarea',
  resize:true,
  menubar: false,
   hidden_input: false,
     setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
    },
  plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help wordcount'
  ],
  toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css']
});

});

			$("#Dar_de_alta").click(function(){
				swal({
  title: 'Estas seguro de dar de alta este paciente',
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, dar de alta'
}).then((result) => {
  if (result.value) {
       	var paciente_id=$("#paciente_id").val();
		       	$.ajax({
		       		url:"{{route("paciente.alta")}}",
		       		datatype:"json",
		       		type:"post",
		       		data:{paciente_id:paciente_id},
		       		success:function(response){
		       			if(response.status==true){
		       				swal(response.msg);
		       				$("#gif").remove();
		       			}
		       		},
		       		error:function(){}
		       	});
  }else{
  		revertFunc();
  }
})
		
		});
		$("#guardar_diagnostico").click(function(e){
		/*if($("#form_diagnostico").submit()){
			swal("Ecito al actualizar los datos");
			e.preventDefault();
		}*/
tinyMCE.triggerSave();

			var form=$("#form_diagnostico").serialize();
			console.log(form);
			$.ajax({
				url:"{{route('diagnostico.guardar')}}",
				datatype:"json",
				type:"post",
				data:form,
				success:function(response){
					$("#gif").remove();
					swal(response.msg);
				},
				error:function(){}
			});
		});
				$("#datos_sig").click(function(){
			var form=$("#form-datos-sig").serialize();
			$.ajax({
				url:"{{route("paciente.datos_sig")}}",
				datatype:"json",
				type:"post",
				data:form,
				success:function(response){
					if(response.status==true){
						$("#gif").remove();
						swal(response.msg);
						$("#gif").remove();
					}
				},
				error:function(){}
			});
		});
						$("#infante_select").change(function(){
			if($(this).val()=='S'){
				$("#estutor").css('display','flex');
			}else{
				$("#estutor").css('display','none');
			}
		});
				$("#datos_paciente").click(function(){
					var form=$("#form_paciente").serialize();
					$.ajax({
						url:"{{route("paciente.store")}}",
						datatype:"json",
						type:"post",
						data:form,
						success:function(response){
							$("#gif").remove();
							swal(
  'Exito al actualizar los datos',
  'success'
)
						},
						error:function(){}
					});
				});
				function editar(familiar_id){

 $("#"+familiar_id).find(':input, select').removeAttr("readonly");
	}
	function eliminar(familiar_id){
	    var form=$("#"+familiar_id).find("input,select").serialize();
		 
		       if (!confirm("Estas seguro de eliminar este familiar")) {
		       	revertFunc();
		       }else{
		    $.ajax({
		    	url:"{{route("paciente.familiar.delete")}}",
		    	datatype:"json",
		    	type:"post",
		    	data:form,
		    	success:function(response){
		    		$("#"+familiar_id).remove();
		    			swal(
  response.msg,
  'success'
);	$("#gif").remove();
	
		    	},
		    	error:function(){}
		    });
		}
	}
	function guardar(familiar_id){
		    var form=$("#"+familiar_id).find("input,select").serialize();
		   $.ajax({
		    	url:"{{route("paciente.familiar")}}",
		    	datatype:"json",
		    	type:"post",
		    	data:form,
		    	success:function(response){

					$("#gif").remove();
		    			swal(
  response.msg,
  'success'
);
		    				$("#gif").remove();
 $("#"+familiar_id).find(':input, select').attr("readonly","readonly");	
		    	},
		    	error:function(){}
		    });
	}	
	

		
	function actualizar_session(id){
		var form=$("#form-"+id).serialize();
		$.ajax({
			url:"{{route("seguimiento.add")}}",
			datatype:"json",
			type:"post",
			data:form,
			success:function(response){
				$("#gif").remove();
				if(response.status==true){
swal(response.msg);
				}
			},
			error:function(){}
		});
	}
	$("#guardar_session").click(function (){
		var data=$("#form0").serialize();
	   if (getStats('descripcion').words < 30) {

        Swal.fire({
  type: 'error',
  text: 'Favor de llenar la informacion con al menos 30 palabras'
});
        return;
    }else{
    	$.ajax({
    		url:"{{route('revisa.cita')}}",
    		data:data,
    		type:"post",
    		datatype:"json",
    		success:function(response){
if(response.status!=true){
	Swal.fire({
  type: 'error',
  text: response.msg
});
	
		       				$("#gif").remove();
}else{
		$("#form0").submit();
    
}
    		},
    		error:function(){

    		}
    	});
    }
	});
	// Returns text statistics for the specified editor by id
function getStats(id) {
    var body = tinymce.get(id).getBody(), text = tinymce.trim(body.innerText || body.textContent);

    return {
        chars: text.length,
        words: text.split(/[\w\u2019\'-]+/).length
    };
}

				$("#agregar_paciente").click(function(){
			var form =$("#formulario").serialize();
			$.ajax({
				url:"{{route("paciente.familiar")}}",
				data:form,
				datatype:"json",
				type:"post",
				success:function(response){
						$("#gif").remove();
						if(response.familiar.sexo=='M'){
							var sexo_m='selected';
							var sexo_h='';
						}else{
							var sexo_h='selected';
							var sexo_m='';

						}
					div='<div class="row" id="'+response.familiar.id+'">'
					+'<div class="col">'
					+'<label for="nombre">Nombre</label>'
					+'<input type="hidden" value="'+response.familiar.id+'" name="familiar_id">'
					+'<input type="hidden" value="'+response.familiar.caso_id+'" name="caso_id">'
					+'<input type="hidden" value="'+response.familiar.paciente_id+'" name="paciente_id">'
					+'<input class="form-control" name="nombre" type="text" readonly="readonly" id="nombre" value="'+response.familiar.nombre+'">'
					+'</div>'
					+'<div class="col">'
					+'	<label for="edad">Sexo</label>'
					+'<select autofocus="autofocus" class="form-control" id="sexo" name="sexo"><option value="F" '+sexo_h+'>'
					+'Femenino</option><option value="M" '+sexo_m+'>Masculino</option></select>'
					+'</div>'
					+'<div class="col">'
					+'	<label for="edad">Edad</label>'
					+'	<input class="form-control" name="edad" type="text" readonly="readonly" id="edad" value="'+response.familiar.edad+'">'
					+'</div>'
					+'<div class="col">'
					+'	<label for="Parentesco">Parentesco</label>'
					+'	<input class="form-control" name="Parentesco" type="text" readonly="readonly" id="Parentesco" value="'+response.familiar.parentesco+'">'
					+'</div>'
					+'<div class="col-3" style="'
   					+' margin-top: auto;">'
					+'<button class="btn btn-success" type="button" onClick="guardar('+response.familiar.id+')" style="'
   					+' padding: 1;">Guardar</button>'
   					+'<button class="btn btn-warning" type="button" onclick="editar('+response.familiar.id+')" style="'
   					+' padding: 1;">Editar</button>'
   					+'<button class="btn btn-danger" type="button" onclick="eliminar('+response.familiar.id+')" style="'
   					+' padding: 1;">Eliminar</button>'
					+'</div>'
					+'</div>';
					$("#div_fam").append(div);
				},
				error:function(){}
			});

		});
				$("#Abandono_paciente").click(function(){
				 	var paciente_id=$("#paciente_id").val();
								swal({
  title: '¿Estas seguro de que este paciente ha abandonado su tratamiento?',
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, dar de baja'
}).then((result) => {
  if (result.value) {

		       	$.ajax({
		       		url:"{{route("paciente.abandono")}}",
		       		datatype:"json",
		       		type:"post",
		       		data:{paciente_id:paciente_id},
		       		success:function(response){
		       			if(response.status==true){
		       				swal(response.msg);
		       				$("#gif").remove();
		       			}
		       		},
		       		error:function(){}
		       	});
  }else{
  		revertFunc();
  }
});
			});
	</script>
	@endif