
@extends('../layouts.app')
@section('content')

<div class="container" >
	{{Form::open(['route'=>'consultores.add','method'=>'post','role'=>'form'])}}
	<div >
		<div >

			<div style="align-content: center;">
				<h4 style="text-align: center;">Registro de nuevo Consultor</h4>

			</div>
			<hr>
			<div class="">

				<div class="row">
					{{Form::hidden('activo','A')}}
					<div class="col">
						{{ Form::label('name','Nombre*') }}
						{{Form::text('name',old('name'),['class'=>'form-control','required'=>'required'])}}
					</div>
					<div class="col">
						{{ Form::label('ap_pa','Apellido Paterno*') }}
						{{Form::text('ap_pa',old('ap_pa'),['class'=>'form-control','required'=>'required'])}}
					</div>

					<div class="col">
						{{ Form::label('ap_ma','Apellido Materno*') }}
						{{Form::text('ap_ma',old('ap_ma'),['class'=>'form-control','required'=>'required'])}}
					</div>
				</div>
				<div class="row">


					<div class="col">
						{{ Form::label('celular_paciente','Celular') }}
						{{Form::text('celular_paciente',old('celular_paciente'),['class'=>'form-control'])}}
					</div>
					<div class="col">
						{{ Form::label('telefono_paciente','Telefono') }}
						{{Form::text('telefono_paciente',old('telefono_paciente'),['class'=>'form-control'])}}
					</div>
					<div class="col">
						{{ Form::label('mail','Correo*') }}
						{{Form::email('mail', null, ['class'=>'form-control','required'=>'required'])}}
					</div>
				</div>



				<div class="row">
					<div class="col">
						{{ Form::label('direccion_paciente','Direccion') }}
						{{Form::text('direccion_paciente',old('direccion_paciente'),['class'=>'form-control'])}}
					</div>
					<div class="col">
						{{ Form::label('numero_ext','Numero Ext.') }}
						{{Form::text('numero_ext',old('numero_ext'),['class'=>'form-control'])}}
					</div>
					<div class="col">
						{{ Form::label('numero_int','Numero Int.') }}
						{{Form::text('numero_int',old('numero_int'),['class'=>'form-control'])}}
					</div>

				</div>
				<div class="row">
					<div class="col">
						{{ Form::label('colonia','Colonia') }}
						{{Form::text('colonia',old('Colonia'),['class'=>'form-control'])}}
					</div>
					<div class="col">
						{{Form::label('color','Color*')}}

						<div >
							<input id="mycp" type="text" class="form-control" name="color" />
						</div>
					</div>
					<div class="col">
					</div>

				</div>
				<div class="row">
					<div class="col">
						{{ Form::label('carrera','Carrera') }}
						{{Form::text('carrera',old('carrera'),['class'=>'form-control'])}}
					</div>
					<div class="col">
						{{ Form::label('tipo_cita', 'Especialidades*') }}
						{{ Form::select('especialidades[]', $especialidades, null, ['id' => 'tipo_cita', 'multiple'=>'multiple','class'=>'form-control multiple ','required'=>'required']) }}
					</div>
					<div class="col">
						{{ Form::label('categoria', 'Cetegoria*') }}
						{{ Form::select('categoria', $categorias, null, ['id' => 'categoria','class'=>'form-control multiple ','required'=>'required']) }}
					</div>



				</div>
				<hr>
				<div class="row">

					<div class="col">
						{{ Form::label('da_diagnostico', 'Diagnostico')}}
						{{Form::checkbox('da_diagnostico', 'value')}}
					</div>
					<div class="col">
						{{ Form::label('diagnostico_costo', 'Diagnostico Costo')}}
						{{Form::text('diagnostico_costo',old('diagnostico_costo'),['class'=>'form-control','disabled'=>'disabled'])}}
					</div>
					<div class="col">
						{{ Form::label('da_voluntariado', 'Voluntariado')}}
						{{Form::checkbox('da_voluntariado', 'value')}}
					</div>
					<div class="col">
						{{ Form::label('voluntariado_costo', 'Voluntariado Costo')}}
						{{Form::text('voluntariado_costo',old('voluntariado_costo'),['class'=>'form-control','disabled'=>'disabled'])}}
					</div>
				</div>
				<div class="row">

					<div class="col">
						{{ Form::label('consultor_cost_ind', 'Costo Individual*')}}
						{{Form::text('consultor_cost_ind',old('consultor_cost_ind'),['class'=>'form-control','required'=>'required'])}}
					</div>
					<div class="col">
						{{ Form::label('consultor_cost_pareja', 'Costo Pareja*')}}
						{{Form::text('consultor_cost_pareja',old('consultor_cost_pareja'),['class'=>'form-control','required'=>'required'])}}
					</div>
					<div class="col">
	 							{{ Form::label('consultor_cost_familiar', 'Costo familiar*')}}
	 							{{Form::text('consultor_cost_familiar',old('consultor_cost_pareja'),['class'=>'form-control','required'=>'required'])}}
	 						</div>
					<div class="col">
						{{ Form::label('porcentaje_consultor', 'Porcentaje Consultor')}}
						{{Form::text('porcentaje_consultor',old('porcentaje_consultor'),['class'=>'form-control','disabled'=>'disabled','id'=>'porcentaje_consultor'])}}
					</div>
					<div class="col">
						{{ Form::label('porcentaje_family', 'Porcentaje Family')}}
						{{Form::text('porcentaje_family',old('porcentaje_family'),['class'=>'form-control','disabled'=>'disabled','id'=>'porcentaje_family'])}}
					</div>
				</div>
				<hr>
				<div id="Horarios">

					<div class="row">

						<div class="col">
							{{ Form::label('Dia', 'Dia*')}}
							{{Form::select('dia0', ['Lunes' => 'Lunes', 'Martes' => 'Martes','Miercoles' => 'Miercoles', 'Jueves' => 'Jueves','Viernes' => 'Viernes', 'Sabado' => 'Sabado'], null,['class'=>'form-control'])}}
						</div>

						<div class="col">
							{{Form::label('hora_inicio','Hora de entrada')}}
							<input id="hora_e0" type="time" name="hora_entrada0" value="00:00" class="form-control">
						</div>
						<div class="col">
							{{Form::label('hora_fin','Hora de ultima cita')}}
							<input  id="hora_s0" type="time" name="hora_salida0" value="00:00" class="form-control" onchange="calcula_horas(0)">
						</div>
						<div class="col">
							{{Form::label('horas_dia','Horas disponible')}}
							{{Form::text('horas_disponible0',null,['class'=>'form-control','id'=>'Horas_disponible0','readonly'=>'readonly'])}}
						</div>
					</div>


				</div>
				<div class="row">
					<div class="col" style="margin-top: 1.5%; text-align: center;">

						{!! Html::decode(Form::button('<i class="fas fa-plus-circle "></i>Agregar Dia',['class' => 'btn btn-success','id'=>'agregarDia'])) !!}

					</div>
				</div>







				<div style="margin-top:1.5%">
					{{Form::submit('guardar',['class'=>'btn btn-success'])}}
					<button type="button" class="btn" id="close_modal" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
	{{Form::close()}}

	<script>
		var nextinput=0;
		$(document).ready(function(){
			

			jQuery('option').mousedown(function(e) {
				e.preventDefault();
				jQuery(this).toggleClass('selected');

				jQuery(this).prop('selected', !jQuery(this).prop('selected'));
				return false;
			});
			$("#agregarDia").click(function(){

				nextinput=nextinput+1;
				campo = '<div class="row">'+'<div class="col">'+
				'<label for="Dia">Dia</label>'+
				'<select class="form-control" name="dia'+nextinput+'"><option value="Lunes">Lunes</option><option value="Martes">Martes</option><option value="Miercoles">Miercoles</option><option value="Jueves">Jueves</option><option value="Viernes">Viernes</option><option value="Sabado">Sabado</option></select>'+
				'</div>'+
				'<div class="col">'+
				'<label for="hora_inicio">Hora de Entrada</label>'+
				'<input type="time" id="hora_e'+nextinput+'" name="hora_entrada'+nextinput+'" value="00:00" class="form-control sum">'+
				'</div>'+
				'<div class="col">'+
				'<label for="hora_fin">Hora de Salida</label>'+
				'<input type="time"  id="hora_s'+nextinput+'" name="hora_salida'+nextinput+'" value="00:00" class="form-control change" onchange="calcula_horas('+nextinput+')">'+
				'</div>'+
				'<div class="col">'+
				'<label for="hora_fin">Horas disponible</label>'+
				'<input id="Horas_disponible'+nextinput+'" type="text" name="horas_disponible'+nextinput+'" readonly="readonly" class="form-control">'+
				'</div>'+
				'</div>';
				$("#Horarios").append(campo);
			});
						
			$('#da_diagnostico').change(function(){
				if ($('#da_diagnostico').prop('checked')) {
					$('#diagnostico_costo').prop('disabled',false);
				}else{
					$('#diagnostico_costo').prop('disabled',true);
				}
			});
			$('#da_voluntariado').change(function(){
				if ($('#da_voluntariado').prop('checked')) {
					$('#voluntariado_costo').prop('disabled',false);
				}else{
					$('#voluntariado_costo').prop('disabled',true);
				}
			});

			$('#mycp').colorpicker();
			$("#categoria").change(function(){
				var categoria_id=$("#categoria").val();
				$.ajax({
					url:"{{route('categoria')}}",
					data:{
						"categoria_id":categoria_id
					},
					dataType:"json",
					method:"Post",
					success:function(response){
						if(response.status==true)    { 

							$("#porcentaje_family").val(response.costo.porcentaje_family);
							$("#porcentaje_consultor").val(response.costo.porcentaje_consultor);
						}else{
							alert(response.costo);
						}
					},fail:function(){

					}

				});
			});



		});
	function calcula_horas(divv){
var he=$("#hora_e"+divv).val();
var hs=$("#hora_s"+divv).val();
inicioMinutos = parseInt(he.substr(3,2));
  inicioHoras = parseInt(he.substr(0,2));
  
  finMinutos = parseInt(hs.substr(3,2));
  finHoras = parseInt(hs.substr(0,2));

  transcurridoMinutos = finMinutos - inicioMinutos;
  transcurridoHoras = finHoras - inicioHoras;
  
  if (transcurridoMinutos < 0) {
    transcurridoHoras--;
    transcurridoMinutos = 60 + transcurridoMinutos;
  }
  
  horas = transcurridoHoras.toString();
  minutos = transcurridoMinutos.toString();
  
  if (horas.length < 2) {
    horas = "0"+horas;
  }
  
  if (horas.length < 2) {
    horas = "0"+horas;
  }
$("#Horas_disponible"+divv).val(horas);
	}
	</script>
	@endsection
