
<div>

	<div id="modalEvent" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="width:25%;margin: auto;">
		<div class="modal-dialog modal-sm" style="width: 100%;">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="mySmallModalLabel">Cambiar Contraseña</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row"><div class="col"> 
						{{Form::label('pass','Nueva Contraseña')}}
						{{Form::text('pass',null,['id'=>'pass','class'=>'form-control','type'=>'password'])}}</div> </div>
						<div class="row"><div class="col"> 
							{{Form::label('pass_','Nueva Contraseña')}}
							{{Form::text('pass_',old('pass_'),['id'=>'pass_','class'=>'form-control','type'=>'password'])}}</div> </div>
						</div>
						<div class="modal-footer">
							{{Form::button('cancelar',['class'=>'btn btn-danger','data-dismiss'=>'modal'])}}
							{{Form::button('guardar',['class'=>'btn btn-success','id'=>'change_pass'])}}
						</div>
					</div>
				</div>
			</div>
			{{Form::open(['route'=>'consultores.add','method'=>'post','role'=>'form','id'=>'formCons'])}}
			<div >
				{{Form::hidden('consultor_id',$consultor->id)}}
				{{Form::hidden('activo',$consultor->activo,['id'=>'baja'])}}
				<div style="align-content: center;">
					<h4 style="text-align: center;">Datos de un Consultor</h4>

				</div>
				<hr>
				<div class="">
					<div class="row">
						<div class="col" style="text-align: right;">
							{{ Form::label('mail','Correo*') }}
						</div>
						<div class="col">
							{{Form::email('mail',$consultor->user->email, ['class'=>'form-control','required'=>'required','readonly'=>'readonly','id'=>'mail'])}}
						</div>
						<div class="col">
							<button type="button" id="pass" class="btn btn-warning" data-toggle="modal" data-target=".bd-example-modal-sm">Cambiar Password</button>
							<button type="button" id="dar_baja" class="btn btn-danger">Dar de baja</button>

						</div>
					</div>
					<div class="row">

						<div class="col">
							{{ Form::label('name','Nombre*') }}
							{{Form::text('name',$consultor->nombre,['class'=>'form-control','required'=>'required'])}}
						</div>
						<div class="col">
							{{ Form::label('ap_pa','Apellido Paterno*') }}
							{{Form::text('ap_pa',$consultor->apellido_pa,['class'=>'form-control','required'=>'required'])}}
						</div>

						<div class="col">
							{{ Form::label('ap_ma','Apellido Materno*') }}
							{{Form::text('ap_ma',$consultor->apellido_ma,['class'=>'form-control','required'=>'required'])}}
						</div>
					</div>
					<div class="row">


						<div class="col">
							{{ Form::label('celular_paciente','Celular') }}
							{{Form::text('celular_paciente',$consultor->celular_paciente,['class'=>'form-control'])}}
						</div>
						<div class="col">
							{{ Form::label('telefono_paciente','Telefono') }}
							{{Form::text('telefono_paciente',$consultor->telefono_paciente,['class'=>'form-control'])}}
						</div>

					</div>



					<div class="row">
						<div class="col">
							{{ Form::label('direccion_paciente','Direccion') }}
							{{Form::text('direccion_paciente',$consultor->direccion_paciente,['class'=>'form-control'])}}
						</div>
						<div class="col">
							{{ Form::label('numero_ext','Numero Ext.') }}
							{{Form::text('numero_ext',$consultor->numero_ext,['class'=>'form-control'])}}
						</div>
						<div class="col">
							{{ Form::label('numero_int','Numero Int.') }}
							{{Form::text('numero_int',$consultor->numero_int,['class'=>'form-control'])}}
						</div>

					</div>
					<div class="row">
						<div class="col">
							{{ Form::label('colonia','Colonia') }}
							{{Form::text('colonia',$consultor->colonia,['class'=>'form-control'])}}
						</div>
						<div class="col">
							{{Form::label('color','Color*')}}

							<div >
								<input id="mycp" type="text" class="form-control" name="color" value="{{$consultor->color}}" />
							</div>
						</div>
						<div class="col">
						</div>

					</div>
					<div class="row">
						<div class="col">
							{{ Form::label('carrera','Carrera') }}
							{{Form::text('carrera',$consultor->carrera,['class'=>'form-control'])}}
						</div>
						<div class="col">
							{{ Form::label('tipo_cita', 'Especialidades*') }}
							<select id="tipo_cita" multiple="multiple" class="form-control multiple " required="required" name="especialidades[]">

								@foreach($especialidades as $especialidad)
								@php
								$selected='';
								@endphp
								@foreach($consultor->Especialidades as $especialidad_aux)

								@php
								if($especialidad_aux->id==$especialidad->id)
								{
									$selected='selected';
								}
								@endphp

								@endforeach
								<option value="{{$especialidad->id}}" {{$selected}}>
									{{$especialidad->descripcion}}</option>
									@endforeach

								</select>
							</div>
							<div class="col">
								{{ Form::label('categoria', 'Cetegoria*') }}
								{{ Form::select('categoria',$categorias,$consultor->categoria_id,['id' => 'categoria','class'=>'form-control multiple ','required'=>'required']) }}
							</div>



						</div>
						<hr>
						<div class="row">

							<div class="col">
								{{ Form::label('da_diagnostico', 'Diagnostico')}}

								{{Form::checkbox('da_diagnostico', 'value',$consultor->diagnostico=='S'?'checked':'')}}
							</div>
							<div class="col">
								{{ Form::label('diagnostico_costo', 'Diagnostico Costo')}}
								{{Form::text('diagnostico_costo',$consultor->diagnostico_costo,['class'=>'form-control',$consultor->diagnostico=='S'?'':'readonly'])}}
							</div>
							<div class="col">
								{{ Form::label('da_voluntariado', 'Voluntariado')}}
								{{Form::checkbox('da_voluntariado', 'value',['id'=>'da_voluntariado'])}}
							</div>
							<div class="col">
								{{ Form::label('voluntariado_costo', 'Voluntariado Costo',$consultor->voluntariado=='S'?'checked':'')}}
								{{Form::text('voluntariado_costo',$consultor->voluntariado_costo,['class'=>'form-control',$consultor->voluntariado=='S'?'':'readonly','id'=>'voluntariado_costo'])}}
							</div>
						</div>
						<div class="row">

							<div class="col">
								{{ Form::label('consultor_cost_ind', 'Costo Individual*')}}
								{{Form::text('consultor_cost_ind',$consultor->costo_individual,['class'=>'form-control','required'=>'required'])}}
							</div>
							<div class="col">
								{{ Form::label('consultor_cost_pareja', 'Costo Pareja*')}}
								{{Form::text('consultor_cost_pareja',$consultor->costo_pareja,['class'=>'form-control','required'=>'required'])}}
							</div>
							<div class="col">
	 							{{ Form::label('consultor_cost_familiar', 'Costo familiar*')}}
	 							{{Form::text('consultor_cost_familiar',$consultor->costo_familiar,['class'=>'form-control','required'=>'required'])}}
	 						</div>
							<div class="col">
								{{ Form::label('porcentaje_consultor', 'Porcentaje Consultor')}}
								{{Form::text('porcentaje_consultor',$consultor->categorias->porcentaje_consultor,['class'=>'form-control','disabled'=>'disabled','id'=>'porcentaje_consultor'])}}
							</div>
							<div class="col">
								{{ Form::label('porcentaje_family', 'Porcentaje Family')}}
								{{Form::text('porcentaje_family',$consultor->categorias->porcentaje_family,['class'=>'form-control','disabled'=>'disabled','id'=>'porcentaje_family'])}}
							</div>
						</div>
						<hr>
						<div id="Horarios">

							@php $conteo=0; @endphp
							@foreach($dias as $dia)
							@php
							$entrada=$consultor->horario_consultor[$dia.'_entrada'];
							$salida=$consultor->horario_consultor[$dia.'_salida'];

							@endphp
							@if($entrada!='00:00:00' && $salida!='00:00:00')
							<div class="row" id="{{$dia}}">
								@php
								$res=0;
								$entrada_hor = substr($entrada,0,2);	
								$salida_hor = substr($salida,0,2);	
								$res=($entrada_hor-$salida_hor)*-1;

								@endphp
								<div class="col">
									{{ Form::label('Dia', 'Dia*')}}
									{{Form::select('dia'.$conteo, ['lunes' => 'Lunes', 'martes' => 'Martes','miercoles' => 'Miercoles', 'jueves' => 'Jueves','viernes' => 'Viernes', 'sabado' => 'Sabado'], $dia,['class'=>'form-control','readonly'=>'readonly'])}}
								</div>

								<div class="col">
									{{Form::label('hora_inicio','Hora de Entrada')}}
									<input id="{{'hora_e'.$conteo}}"type="time" name="{{'hora_entrada'.$conteo}}" value="{{$entrada}}" class="form-control" readonly="readonly">
								</div>
								<div class="col">
									{{Form::label('hora_fin','Hora de Salida')}}
									<input id="{{'hora_s'.$conteo}}"type="time" name="{{'hora_salida'.$conteo}}" value="{{$salida}}" class="form-control" onchange="calcula_horas({{$conteo}}) readonly="readonly">
								</div>
								<div class="col">
									{{Form::label('horas_dia','Horas disponible')}}
									{{Form::text('horas_disponible'.$conteo,$res,['class'=>'form-control','id'=>'Horas_disponible'.$conteo,'readonly'=>'readonly'])}}
								</div>
								<div class="col-3 " style="margin-top: auto;">
									{{Form::button('Editar',['class'=>'btn btn-warning','onClick'=>'editar("'.$dia.'")'])}}
									{{Form::button('Eliminar',['class'=>'btn btn-danger','onClick'=>'eliminar("'.$dia.'")'])}}
								</div>
							</div>
							@php $conteo++; @endphp
							@endif
							@endforeach
							{{Form::hidden('conteo',$conteo,['id'=>'conteo'])}}
						</div>
						<div class="row">
							<div class="col" style="margin-top: 1.5%; text-align: center;">

								{!! Html::decode(Form::button('<i class="fas fa-plus-circle "></i>Agregar Dia',['class' => 'btn btn-success','id'=>'agregarDia'])) !!}

							</div>
						</div>
						<div style="margin-top:1.5%">
							{{Form::submit('guardar',['class'=>'btn btn-success'])}}
							<button type="button" class="btn" id="close_modal" data-dismiss="modal">Cancelar</button>
						</div>
					</div>
				</div>
				{{Form::close()}}
			</div>
		</div>
		<script type="text/javascript">
			$('#mycp').colorpicker();
			$("#dar_baja").click(function(){

				var form=$("#formCons").serialize();
				swal({
					title: 'Estas seguro de dar de baja este consultor?',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					cancelButtonText: 'Cancelar',
					confirmButtonText: 'Si, dar de baja'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							url:"{{route('consultores.baja')}}",
							data:form,
							datatype:"json",
							type:"post",
							success:function(response){
								if(response.status!=true){
									swal({
										type: 'error',
										title: 'Este consultor aun tiene pacientes asignados',
										text:'Debe de reasignar a los pacientes a otro consultor'
									})
								}else{
									swal({
										type: 'success',
										title: 'Se ha dado de baja al consultor',
									})
									$("#baja").val('B');
									$("#formCons").submit();
								}
							},
							error:function(){}
						});
					}
				})
			});
			var nextinput=$("#conteo").val();
			$('#da_voluntariado').change(function(){
				if ($('#da_voluntariado').prop('checked')) {
					$('#voluntariado_costo').prop('readonly',false);
				}else{
					$('#voluntariado_costo').prop('readonly',true);
				}
			});

			$("#pass").click(function(){

				$("#modalEvent").show();
			});
			$("#change_pass").click(function(){
				$pass=$("#pass").val();
				$pass_=$("#pass_").val();
				$mail=$("#mail").val();
				if($pass.match(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/))
				{
					if($pass==$pass_){
						$.ajax({
							url:"{{route('change.Pass')}}",
							type:"post",
							dataType:"json",
							data:{
								pass:$pass,
								mail:$mail
							},
							success:function(response){
								swal(response.messagge);
								$("#modalEvent").modal('toggle');
							},
							error:function(){}
						});
					}else{
						swal();
					}
				}else{
					swal("Debe de contener 8 caracteres mayusculas y numeros ");
				}
			});
			jQuery('option').mousedown(function(e) {
				e.preventDefault();
				jQuery(this).toggleClass('selected');

				jQuery(this).prop('selected', !jQuery(this).prop('selected'));
				return false;
			});
			$("#agregarDia").click(function(){

				campo = '<div class="row">'+'<div class="col">'+
				'<label for="Dia">Dia</label>'+
				'<select class="form-control" name="dia'+nextinput+'"><option value="Lunes">Lunes</option><option value="Martes">Martes</option><option value="Miercoles">Miercoles</option><option value="Jueves">Jueves</option><option value="Viernes">Viernes</option><option value="Sabado">Sabado</option></select>'+
				'</div>'+
				'<div class="col">'+
				'<label for="hora_inicio">Hora de Entrada</label>'+
				'<input id="hora_e'+nextinput+'" type="time" name="hora_entrada'+nextinput+'" value="00:00" class="form-control">'+
				'</div>'+
				'<div class="col">'+
				'<label for="hora_fin">Hora de Salida</label>'+
				'<input id="hora_s'+nextinput+'" type="time" name="hora_salida'+nextinput+'" value="00:00" class="form-control" onchange="calcula_horas('+nextinput+')">'+
				'</div>'+
				'<div class="col">'+
				'<label for="hora_fin">Horas disponible</label>'+
				'<input id="Horas_disponible'+nextinput+'" type="text" name="horas_disponible'+nextinput+'" readonly="readonly" class="form-control">'+
				'</div>'+
				'<div class="col-3"></div>'+
				'</div>';
				$("#Horarios").append(campo);
				nextinput++;

			});
			$("#categoria").change(function(){
				var categoria_id=$("#categoria").val();
				$.ajax({
					url:"{{route('categoria')}}",
					data:{
						"categoria_id":categoria_id
					},
					dataType:"json",
					method:"Post",
					success:function(response){
						if(response.status==true)    { 

							$("#porcentaje_family").val(response.costo.porcentaje_family);
							$("#porcentaje_consultor").val(response.costo.porcentaje_consultor);
						}else{
							swal(response.costo);
						}
					},fail:function(){

					}

				});
			});
			function editar(dia){
				$("#"+dia).find('input').prop('readonly',false);
				$("#"+dia).find('select').prop('readonly',false);
			}
			function eliminar(dia){
				if(confirm("Estas seguro de eliminar este dia" )){
					$("#"+dia).remove();
				}else{
					revertFunc();
				}
			}
			function calcula_horas(divv){
				var he=$("#hora_e"+divv).val();
				var hs=$("#hora_s"+divv).val();
				inicioMinutos = parseInt(he.substr(3,2));
				inicioHoras = parseInt(he.substr(0,2));

				finMinutos = parseInt(hs.substr(3,2));
				finHoras = parseInt(hs.substr(0,2));

				transcurridoMinutos = finMinutos - inicioMinutos;
				transcurridoHoras = finHoras - inicioHoras;

				if (transcurridoMinutos < 0) {
					transcurridoHoras--;
					transcurridoMinutos = 60 + transcurridoMinutos;
				}

				horas = transcurridoHoras.toString();
				minutos = transcurridoMinutos.toString();

				if (horas.length < 2) {
					horas = "0"+horas;
				}

				if (horas.length < 2) {
					horas = "0"+horas;
				}
				$("#Horas_disponible"+divv).val(horas);
			}

		</script>
