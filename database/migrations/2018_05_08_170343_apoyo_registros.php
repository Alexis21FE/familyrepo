<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApoyoRegistros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
              
        Schema::create('apoyo_registros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cita_id');
            $table->integer('beca_id');
            $table->integer('apoyo_id');
            $table->integer('user_id');
            $table->float('saldo_inicial');
            $table->float('abono');
            $table->float('cargo');
            $table->float('saldo_final');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
           Schema::dropIfExists('apoyo_registros');
    }
}
