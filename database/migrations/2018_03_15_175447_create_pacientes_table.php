<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable();
            $table->string('colegio')->nullable();
            $table->datetime('fecha_registro')->nullable();
            $table->string('alias')->nullable();
            $table->enum('estado_civil',['C','S'])->nullable();
            $table->integer('status_id')->unsigened()->nullable();
            $table->string('apellido_pa')->nullable();
            $table->string('apellido_ma')->nullable();
            $table->string('direccion')->nullable();
            $table->integer('numero_ext')->nullable();
            $table->integer('numero_int')->nullable();
            $table->string('telefono_paciente')->nullable();
            $table->string('celular_paciente')->nullable();
            $table->string('correo_paciente')->nullable();
            $table->string('contacto_family')->nullable();
            $table->enum('infante',['S','N'])->nullable();
            $table->string('nombre_tutor')->nullable();
            $table->string('ap_tutor')->nullable();
            $table->string('am_tutor')->nullable();
            $table->integer('datos_factura')->nullable();
            $table->datetime('fecha_modificacion')->nullable();
            $table->string('nombrecompleto')->nullable();
            $table->integer('id_usuario')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
