<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('nombre')->nullable();
            $table->string('apellido_pa')->nullable();
            $table->string('apellido_ma')->nullable();
            $table->string('carrera')->nullable();
            $table->string('color')->nullable()->nullable();
            $table->integer('categoria_id')->nullable();
            $table->enum('diagnostico',  ['S', 'N'])->nullable();
            $table->enum('voluntariado', ['S', 'N'])->nullable();
            $table->enum('activo', ['A', 'B'])->nullable();
            $table->string('celular_paciente')->nullable();
            $table->string('telefono_paciente')->nullable();
            $table->integer('numero_ext')->nullable();
            $table->integer('numero_int')->nullable();
            $table->string('direccion_paciente')->nullable();
            $table->string('colonia')->nullable();
            $table->integer('costo_familiar')->nullable();
            $table->integer('costo_individual')->nullable();
            $table->integer('costo_pareja')->nullable();
            $table->integer('diagnostico_costo')->nullable();
            $table->integer('voluntariado_costo')->nullable();
            $table->datetime('fecha_registro_consultor')->nullable();
            $table->datetime('fecha_modificacion')->nullable();
            $table->integer('usermodificacion_id')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultores');
    }
}
