<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$SypGYgAU45jXmZWuiaTse.E2KH.CXdy/Y3HZkiIvJylTG5yNyFP7u', // secret
        'remember_token' => str_random(10),
        'role_id'=> $faker->numberBetween(1,4),
    ];
});

$factory->define(App\Citas::class, function (Faker $faker) {
    return [
  'title'=>$faker->name,
  'start'=>$faker->dateTimeThisYear(),
  'end'=>$faker->dateTimeThisYear(),
  'color'=>$faker->hexcolor(),
  'descripcion'=>$faker->firstNameMale,
    ];
});


$factory->define(App\Pacientes::class, function(Faker $faker){
  return[
    'nombre'=>$faker->unique()->firstNameMale,
    'nombrecompleto'=>$faker->name,
  ];
});
$factory->define(App\Consultorio::class, function (Faker $faker) {
    return [
        'descripcion'=>$faker->randomLetter,
        'fecha_de_modificacion'=>$faker->dateTime(),
        
    ];
});
