<?php

namespace App;
use App\Pacientes;
use Illuminate\Database\Eloquent\Model;

class datos_factura extends Model
{
    public function paciente(){
    	return $this->hasMany(Pacientes::class,'datos_factura');	
    }
}
