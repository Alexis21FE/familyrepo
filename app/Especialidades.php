<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Consultores;
class Especialidades extends Model
{
	protected $table='especialidades';
	protected $fillable=['descripcion'];
    //
    public function consultores(){
        return $this->belongsToMany(Consultores::class, 'consultores_especialidads','especialidades_id','consultores_id');
    }
}
