<?php

namespace App;
use App\Citas;
use Illuminate\Database\Eloquent\Model;
use App\apoyo_registros;
class Beca_descuento extends Model
{
    public function citas(){
        return $this->belongsToMany(Citas::class, 'beca_citas', 'cita_id', 'beca_id');
    }
    public function apoyos_afecta(){
    	return $this->hasMany(apoyo_registros::class,'beca_id')->orderBy('id','Desc');
    }
}
