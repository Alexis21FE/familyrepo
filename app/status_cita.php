<?php

namespace App;
use App\Citas;
use Illuminate\Database\Eloquent\Model;

class status_cita extends Model
{
     public function cita()
    {
        return $this->hasOne(Citas::Class,'status_cita_id');
    }

}
