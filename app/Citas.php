<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pacientes;
use App\Consultores;
use App\status_cita;
use App\Beca_descuento;
use App\apoyo_registros;
use App\Diagnostico;
use App\Consultorio;
class Citas extends Model
{
	protected $table='citas';
	protected $fillable = [
        'paciente_id', 'caso_id', 'consultor_id','consultorio_id','tipo_cita_id'
        ,'status_cita_id','title','start','end','color','descripcion'
    ];
    //

    public function paciente()
    {
        return $this->belongsTo(Pacientes::class,'paciente_id');
    }

    public function consultor()
    {
        return $this->belongsTo(Consultores::class,'consultor_id');
    }

    public function status_cita()
    {
        return $this->belongsTo(status_cita::Class,'status_cita_id');
    }

    public function becas()
    {
        return $this->belongsToMany(Beca_descuento::class, 'beca_citas', 'cita_id', 'beca_id')->as('Becas_Agregadas')->withPivot('cantidad','equivalente', 'monto');
    }

    public function registro_apoyos()
    {
        return $this->hasMany(apoyo_registros::class,'cita_id');
    }

    public function Diagnostico(){
        return $this->hasOne(Diagnostico::class,'cita_id');
    }

public function consultorio(){
  return $this->belongsTo(Consultorio::class,'consultorio_id');
}

}
