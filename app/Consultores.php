<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Especialidades;
use App\consultor_categoria;
use App\horario_consultor;
use App\User;
use App\Citas;
use App\Caso;
use App\Diagnostico;
class Consultores extends Model
{
	protected $table='consultores';
  public function user(){
  	return $this->belongsTo(User::class);
  }
  public function Especialidades(){

    return $this->belongsToMany(Especialidades::class, 'consultores_especialidads', 'consultores_id', 'especialidades_id');
  }
  public function categorias(){
    return $this->belongsTo(consultor_categoria::class,'categoria_id');
  }
  public function horario_consultor(){
   return $this->hasOne(horario_consultor::class,'consultor_id');
 }
 public function citas_rr()
 {
  return $this->hasMany(Citas::class,'consultor_id');
}
public function caso(){
  return $this->hasMany(Caso::class,'consultor_id');
}

public function diagnostico(){
  return $this->hasMany(Diagnostico::class,'consultor_id');
}
public function Diagnostico_asignado(){
  return $this->hasMany(Diagnostico::class,'consultor_asig_id');
}
}
