<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pacientes;
class sedes extends Model
{
    //
    protected $table='sedes';
    public function paciente(){
    	return $this->hasOne(Pacientes::Class,'colegio');
    }
}
