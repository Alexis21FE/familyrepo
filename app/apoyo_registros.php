<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Beca_descuento;
use App\apoyo;
use App\Citas;
class apoyo_registros extends Model
{
    protected $table='apoyo_registros';
    public function becas_re(){
    	return $this->belongsTo(Beca_descuento::class,'beca_id');
    }
    public function apoyo_re(){
    	return $this->belongsTo(apoyo::class,'apoyo_id');
    }
    public function citas_re()
    {
    	return $this->belongsTo(Citas::class,'cita_id');
    }
}
