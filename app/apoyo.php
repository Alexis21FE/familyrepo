<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\apoyo_registros;

class apoyo extends Model
{
	protected $table='apoyo';
	public function registro(){
		return $this->hasMany(apoyo_registros::class,'apoyo_id')->orderBy('id','desc');
	}
}
