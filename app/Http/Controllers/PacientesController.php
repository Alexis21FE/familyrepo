<?php

namespace App\Http\Controllers;

use App\Pacientes;
use Illuminate\Http\Request;
use App\Status;
use App\datos_factura;
use App\Consultores;
use App\seguimiento;
use App\Citas;
use App\clasificacion;
use App\sedes;
use App\Diagnostico;
use App\Caso;
use Carbon\Carbon;
use DB;
use App\parametro;
class PacientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function __construct()
    {
        $this->middleware(['auth','roles:admin,recep,direc,consul']);
    }
    public function index()
    {
        //
    }
    public function PacientesDirector(){
         $pacientes=Pacientes::orderby('nombrecompleto','asc')->get();
         $consultores=Consultores::orderBy('nombrecompleto','asc')->where('activo','A')->get();
        return view('director.selectPacientes',compact('pacientes','consultores'));
    }
    public function PacienteDirector(Request $request){
        $paciente_id=$request->paciente_id;
        $consultor_id=auth()->user()->consultor->id;
        $response=true;

        $consultores=Consultores::where('activo','A')->get();
        $consultas=seguimiento::where('consultor_id',$consultor_id)->get();
        $cita=Citas::with('paciente','consultor')->where('id',$paciente_id)->first();
        $paciente=Pacientes::with('status','diagnosticos','seguimientos')->where('id',$paciente_id)->first();
        $clasificaciones=clasificacion::orderby('id','desc')->get();
        $sedes=sedes::orderby('id','desc')->get();
        return view('director.response',compact('paciente','clasificaciones','consultores','consultas','response','sedes'));
    }

    public function showFormNew()
    {
        $consultores=Consultores::orderby('nombre','asc')->where('activo','A')->get();
        $sedes=sedes::orderby('id','asc')->get();
        $caso=parametro::orderby('id','desc')->first();
        $caso=$caso->numero_caso+1;

        return view('pacientes/formPacientes',compact('sedes','consultores','caso'));
    }
    public function addNewPaciente(Request $request){
        //dd($request->toArray());
         $now=Carbon::now();
        $now_format=$now->format('Y-m-d');
     $status_id=Status::where('descripcion','Diagnostico')->get(['id']);
     $status_id= $status_id->toArray();
     foreach ($status_id as $key => $value) {
         $status_id=$value['id'];
     }
     $paciente= new Pacientes;
     $paciente->nombre=$request->name;
     $paciente->apellido_pa=$request-> ap_pa;
     $paciente->apellido_ma=$request-> ap_ma;
     $paciente->status_id=$status_id;
     $paciente->colegio=$request->colegio_;
     $paciente->alias=$request-> alias;
     $paciente->estado_civil=$request-> estado_civil;
     //$paciente->tiempo_estado_civil=$request-> tiempo_estado_civil;
     $paciente->direccion=$request-> direccion_paciente;
     $paciente->fecha_registro=date('Y-m-d H:m:s');
     $paciente->id_usuario=auth()->user()->id;
     $paciente->numero_ext=$request-> numero_ext;
     $paciente->numero_int=$request-> numero_int;
     $paciente->telefono_paciente=$request-> telefono_paciente;
     $paciente->celular_paciente=$request-> celular_paciente;
     $paciente->correo_paciente=$request-> correo_paciente;
     $paciente->contacto_family=$request-> contacto_family;
     $paciente->infante  =$request-> infante;
     $paciente->nombre_tutor=$request-> nombre_tutor;
     $paciente->ap_tutor=$request-> ap_tutor;
     $paciente->am_tutor=$request-> am_tutor;
     $paciente->consultor=$request->consultor;
     $paciente->caso=$request->caso;
     if($paciente->alias!=''){
     $paciente->nombrecompleto=$request->alias;

 }else{
     $paciente->nombrecompleto=$request->name.' '.$request-> ap_pa.' '.$request-> ap_ma;
}
     $paciente->save();
     $parametro=parametro::orderBy('id','asc')->first();
     $parametro->numero_caso=$paciente->caso;
     $parametro->save();
     if($request->RFC!=''){

     $datos_factura=new datos_factura;
     $datos_factura->RFC=$request->RFC;
     $datos_factura->facturar_a=$request->facturar_a;
     $datos_factura->domicilio_factura=$request->domicilio_factura;
     $datos_factura->cp_factura=$request->cp_factura;
     $datos_factura->ciudad_factura=$request->ciudad_factura;
     $datos_factura->colonia_factura=$request->colonia_factura;
     $datos_factura->uso_cfdi=$request->uso_cfdi;
     $datos_factura->forma_pago=$request->forma_pago;
     $datos_factura->metodo_pago=$request->metodo_pago;         
     $datos_factura->fecha_modificacion=date();
     $datos_factura->id_usuario=auth()->user()->id;
     $datos_factura->save();
     $paciente->datos_factura=$datos_factura->id;
     $paciente->save();
     }
     $caso=new Caso;
      $diagnostico=new Diagnostico;
        $diagnostico->fecha_diagnostico=$now_format;
        $diagnostico->consultor_id=$request->consultor;
        $diagnostico->paciente_id=$paciente->id;
        $diagnostico->consultor_asig_id=$request->consultor;
        $diagnostico->updated_at=$now;
        $diagnostico->fecha_modificacion=$now_format;
        $diagnostico->save();

     $caso->id=$request->caso;
     $caso->paciente_id=$paciente->id;
     $caso->diagnostico_id=$diagnostico->id;
     $caso->consultor_id=$request->consultor;
     $caso->status='Seguimiento';
     $caso->fecha_modificacion=date('Y-m-d H:m:s');
     $caso->id_usuario=auth()->user()->id;
     $caso->save();
     $diagnostico->caso_id=$caso->id;
     $diagnostico->save();
     return redirect('/home');

 }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $paciente=Pacientes::where('id',$request->paciente_id)->first();
        $diagnostico=Diagnostico::where('paciente_id',$paciente->id)->first();
     
     $paciente->nombre=$request->name;
     $paciente->apellido_pa=$request-> ap_pa;
     $paciente->apellido_ma=$request-> ap_ma;
     $paciente->status_id=$paciente->status_id;
     $paciente->alias=$request-> alias;
     $paciente->estado_civil=$request-> estado_civil;
     //$paciente->tiempo_estado_civil=$request-> tiempo_estado_civil;
     $paciente->direccion=$request-> direccion_paciente;
     $paciente->numero_ext=$request-> numero_ext;

     $paciente->numero_int=$request-> numero_int;
     $paciente->colegio=$request->colegio_;
     $paciente->telefono_paciente=$request-> telefono_paciente;
     $paciente->celular_paciente=$request-> celular_paciente;
     $paciente->correo_paciente=$request-> correo_paciente;
     $paciente->contacto_family=$request-> contacto_family;
     $paciente->infante  =$request-> infante;
     $paciente->nombre_tutor=$request->nombre_tutor;
     $paciente->ap_tutor=$request-> ap_tutor;
     $paciente->am_tutor=$request-> am_tutor;
     $paciente->nombrecompleto=$request->name.' '.$request-> ap_pa.' '.$request-> ap_ma;
     $paciente->save();
        if($paciente->caso>=9000){
        $numero_caso=$request->caso;
        $caso=Caso::where('id',$paciente->caso)->first();
        $caso->id=$numero_caso;
        $caso->save();
        $paciente->caso=$numero_caso;
     }
     if(isset($request->consultor)){
        $paciente->consultor=$request->consultor;
        $caso=Caso::where([['paciente_id',$paciente->id],['id',$request->caso]])->first();
        $caso->consultor_id=$request->consultor;
        $caso->save();
        $diagnostico->consultor_asig_id=$request->consultor;
        $diagnostico->caso_id=$caso->id;
        $diagnostico->save();
     }

     
     if($paciente->datos_factura!=''){
     $datos_factura=datos_factura::where('id',$paciente->datos_factura)->first();
 }else{
    $datos_factura=new datos_factura();
 }

     $datos_factura->RFC=$request->RFC;
     $datos_factura->facturar_a=$request->facturar_a;
     $datos_factura->domicilio_factura=$request->domicilio_factura;
     $datos_factura->cp_factura=$request->cp_factura;
     $datos_factura->ciudad_factura=$request->ciudad_factura;
     $datos_factura->colonia_factura=$request->colonia_factura;
     $datos_factura->uso_cfdi=$request->uso_cfdi;
     $datos_factura->forma_pago=$request->forma_pago;
     $datos_factura->metodo_pago=$request->metodo_pago;
     $datos_factura->fecha_modificacion=date('Y-m-d H:m:s');
     $datos_factura->id_usuario=auth()->user()->id;
     $datos_factura->save();
     $paciente->datos_factura=$datos_factura->id;
     $paciente->save();
     if($request->ajax()){
        $diagnostico->medicamento=$request->tiene_medicamento;
     $diagnostico->causa_medicamento=$request->causa_medicamento;
     $diagnostico->save();
        return response()->json(['status'=>'Ajax request']);
    }else{
     return redirect('/home');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pacientes  $pacientes
     * @return \Illuminate\Http\Response
     */
    public function show()
    {   
        $pacientes=DB::table('pacientes')->select('pacientes.*',DB::raw("concat(consultores.nombre,' ',consultores.apellido_pa) as nombreconsultor"),'consultores.color')->join('consultores','pacientes.consultor','=','consultores.id')
        ->join('casos','pacientes.id','=','casos.paciente_id')->where('casos.status','Seguimiento')
        ->orderBy('nombreconsultor','asc')->orderBy('nombrecompleto','asc')->get();
       $consultores=Consultores::orderBy('nombrecompleto','asc')->where('activo','A')->get();
        return view('pacientes.Select',compact('pacientes','consultores'));
    }
    public function showID(Request $request){
        $paciente_id=$request->paciente_id;
        $sedes=sedes::orderby('id','desc')->get();
        $paciente=Pacientes::with('factura')->where('id',$paciente_id)->first();
        $consultores=Consultores::orderby('nombre','asc')->where('activo','A')->get();
        return view('pacientes.Response',compact('paciente','sedes','consultores'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pacientes  $pacientes
     * @return \Illuminate\Http\Response
     */
    public function edit(Pacientes $pacientes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pacientes  $pacientes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pacientes $pacientes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pacientes  $pacientes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pacientes $pacientes)
    {
        //
    }
    public  function emergente(){
        echo "Aqui si llega ";
    }
}
