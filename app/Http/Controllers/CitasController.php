<?php

namespace App\Http\Controllers;

   define('STDIN', fopen('php://stdin', 'r'));

use App\Citas;
use App\beca_cita;
use Illuminate\Http\Request;
use App\Consultores;
use App\Beca_descuento;
use App\apoyo;
use App\apoyo_registros;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;
use  Google_Service_Calendar_EventReminder;
use Google_Service_Calendar_EventReminders;
use Google_Service_Calendar_EventAttendee;
use DateTime;
use DateTimeZone;

class CitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $client;
    public function __construct(){
        $client = new Google_Client();
        $client->setApplicationName('projecttest');
        $client->setAuthConfigFile(getcwd().'/client-gCalendar.json');
        $client->addScope(Google_Service_Calendar::CALENDAR);
        $this->client=$client;
         $this->middleware(['auth']);

    }

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
 /*public function GetClient()
{
      $client = new Google_Client();
          $client->setApplicationName(config('app.name','family'));
      $client->addScope(Google_Service_Calendar::CALENDAR);
      $client->setClientId('183604032922-9uhcrtii72tro4m3omq1iku9urjpt2rs.apps.googleusercontent.com');
      $client->setClientSecret('4JpnHW9CP_scRZNYEfmj2bJ_'); 
      $client->setRedirectUri(getcwd().'/events');
      if(isset($_GET["code"])){
        $client->authenticate($_GET("code"));
       $toke= $client->getAccessToken();
        $client->setAccessToken($toke);
        dd($toke);
      }
    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
  /*  $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}
*/


    public function index()
    {
        $starts=$_GET['start'];
        $starts=substr($starts,0,10); 
        $ends=$_GET['end'];
        $ends=substr($ends,0,10);
        $data= Citas::with('paciente','consultor','status_cita','becas')->whereBetween('fecha_cita',[$starts,$ends])->get();

      //$data= Citas::with('paciente','consultor','status_cita','becas')->get();
   
      return Response()->json($data); 
    }
    public function citasConsultor($id){
      $data= Citas::with('paciente','consultor','status_cita','becas')->where('consultor_id',$id)->get();

      return Response()->json($data); 
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function updateFecha(Request $request){
     $cita_id=$request->cita_id;
     $event=Citas::where('id',$cita_id)->first();
     $event->start=$request->fecha_nueva.' '.$request->hora_inicio_nueva;
     $event->end=$request->fecha_nueva.' '.$request->hora_fin_nueva;
     $event->fecha_cita=$request->fecha_nueva;
     $event->status_cita_id=4;
     $consultor=Consultores::where('id',$event->consultor_id)->first();
     $service = new Google_Service_Calendar($this->client);
// Crear un nuevo evento
     $event_ = $service->events->get('primary',$event->cita_id_google);
     $nombre_paciente=null;
     if($event->paciente->alias!=''){
        $nombre_paciente=$event->paciente->alias;
    }else{
        $nombre_paciente=$event->paciente->nombre.' '.$event->paciente->apellido_pa.' '.$event->paciente->apellido_ma;

    }
    $event_->setSummary($nombre_paciente);
    $event_->setDescription('null');

// Fecha de inicio (del día actual a las 8am)
    $start = new Google_Service_Calendar_EventDateTime();
    $date_ = new DateTime($event->start, new DateTimeZone('America/Mexico_City'));
    $start->setDateTime($date_->format('Y-m-d').'T'.$date_->format('H:i:s'));

    $start->setTimeZone('America/Mexico_City');
    $event_->setStart($start);

// Fecha de cierre (del día actual a las 6pm)
    $end = new Google_Service_Calendar_EventDateTime();
    $date = new DateTime($event->end, new DateTimeZone('America/Mexico_City'));

    $end->setDateTime($date->format('Y-m-d').'T'.$date->format('H:i:s'));
    $end->setTimeZone('America/Mexico_City');
    $event_->setEnd($end);

    $remindersArray = array();

// Crear recordatorios
    $reminder = new Google_Service_Calendar_EventReminder();
    $reminder->setMethod('email');
    $reminder->setMinutes(10);
    $remindersArray[] = $reminder;

    

    $reminders = new Google_Service_Calendar_EventReminders();
    $reminders->setUseDefault(false);
    $reminders->setOverrides($remindersArray);
    $event_->setReminders($reminders);

    $attendees = array();

// Agregar los Participantes
    $attendee = new Google_Service_Calendar_EventAttendee();
    $attendee->setEmail($consultor->user->email);
    $attendees[] = $attendee;/*
    $attendee = new Google_Service_Calendar_EventAttendee();
    $attendee->setEmail('family.recepcion@gmail.com');
    $attendees[] = $attendee;*/

    $event_->attendees = $attendees;
    $event_->background=$event->color;
    $event_->foreground=$event->color;
    $calendarId='primary';
    if($event->status_cita_id!=2){
       $updatedEvent = $service->events->update('primary', $event->cita_id_google, $event_,array('sendNotifications'=>'all'));

   }else{
    $deleteEvent=$service->events->delete($calendarId,$event->cita_id_google,array('sendNotifications'=>'all'));
}

if($event->save()){
   return response()->json([
    'mensaje' =>'Exito al actulizar la cita',
    'status' => true
]);
}else{
    return response()->json([
        'mensaje' =>'Error al actualizar la cita',
        'status' => false
    ]);
}
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      $monto_consultor=0;
      $monto_family=0;
      $event=new Citas();
      $consultor=Consultores::with('categorias','user')->where('id',$request->consultor)->first();

      $event->consultor_id=$request->consultor;
      $event->paciente_id=$request->paciente;
      $event->consultorio_id=$request->consultorio;
      $event->tipo_cita_id=$request->tipo_cita;
      $event->status_cita_id=1;
      $event->color=$consultor->color;
      $event->monto_inicial=$request->costo;
      $event->monto_final=$request->costo_final;
      $event->fecha_cita=$request->date_start;
      $event->hora=$request->time_start;
      $event->title=$request->citas;
        $event->descripcion=$request->observaciones;
        $event->individual_pareja=$request->individual_pareja;
        $event ->start=$request->date_start.' '.$request->time_start;
        $event ->end=$request->date_start.' '.$request->date_end;
        $event->id_usuario=auth()->user()->id;
        $event -> Save();   
       
//////Nuevo calculo de Precio de la beca
        $preciofinal=$request->costo;
        $porcentaje_consultor=$consultor->categorias->porcentaje_consultor;
        $porcentaje_family=$consultor->categorias->porcentaje_family;
        if($request->becas!=''){
          $monto_consultor_aux=($porcentaje_consultor/100)*$preciofinal;
          $monto_family_aux=($porcentaje_family/100)*$preciofinal;

          foreach ($request->becas as $beca ) {

             $afecta=$beca['afecta'];
             $monto_porcentaje=$beca['monto_porcentaje'];
             if($afecta=="ambos"){
                if($monto_porcentaje=="monto"){
                   $beca_cita=new beca_cita();
                       if($event->tipo_cita_id!=3){
           $beca_cita=new beca_cita();
           $beca_cita->cita_id=$event->id;
           $beca_cita->beca_id=$beca['beca_id'];
           $monto=$beca['cantidad'];
           $preciofinal=$preciofinal-$monto;
           $beca_cita->cantidad=$monto;
           $beca_cita->equivalente=$monto;
           $beca_cita->monto=$preciofinal;
            $beca_cita->user_id=auth()->user()->id;
           $beca_cita->save();

           $monto_consultor=($porcentaje_consultor/100)*$monto;
           $monto_family=($porcentaje_family/100)*$monto;
           $monto_consultor_aux=$monto_consultor_aux-$monto_consultor;
           $monto_family_aux=$monto_family_aux-$monto_family;
 
         }else{
           $beca_cita=new beca_cita();
           $beca_cita->cita_id=$event->id;
           $beca_cita->beca_id=$beca['beca_id'];
           $monto=$beca['cantidad'];
           $preciofinal=$preciofinal-$monto;
           $beca_cita->cantidad=$monto;
           $beca_cita->equivalente=$monto;
           $beca_cita->monto=$preciofinal;
            $beca_cita->user_id=auth()->user()->id;
           $beca_cita->save();

           $monto_consultor=0;
           $monto_family=$preciofinal;
           $monto_consultor_aux=$monto_consultor;
           $monto_family_aux=$monto_family;
         }
               }elseif ($monto_porcentaje=="porcentaje") {

                   $beca_cita=new beca_cita();
                   $beca_cita->cita_id=$event->id;
                   $beca_cita->beca_id=$beca['beca_id'];
                   $porcentaje=$beca['cantidad'];
                if($porcentaje!=100){

           $beca_cita->cantidad=$porcentaje/100;
         }else{
           $beca_cita->cantidad=$porcentaje;

         }
               $beca_cita->equivalente=($porcentaje/100)*$preciofinal;
                   $porcentaje_aux=100-$porcentaje;
                   $porcentaje=$porcentaje_aux/100;
                   $preciofinal=$porcentaje*$preciofinal;
                   $beca_cita->monto=$preciofinal;
                   $beca_cita->user_id=$event->id_usuario;
                   $beca_cita->save();
                   $monto_consultor_aux=($monto_consultor_aux)*$porcentaje;
                   $monto_family_aux=($monto_family_aux)*$porcentaje;
               }

           }elseif ($afecta=="institucion") {
            if ($monto_porcentaje=="porcentaje") {
               $beca_cita=new beca_cita();
               $beca_cita->cita_id=$event->id;
               $beca_cita->beca_id=$beca['beca_id'];
               $porcentaje=$beca['cantidad'];
                        if($porcentaje!=100){

           $beca_cita->cantidad=$porcentaje/100;
         }else{
           $beca_cita->cantidad=$porcentaje;

         }
               $beca_cita->equivalente=($porcentaje/100)*$preciofinal;
               $porcentaje_aux=100-$porcentaje;
               $porcentaje=$porcentaje_aux/100;
               $preciofinal=$porcentaje*$preciofinal;
               $beca_cita->monto=$preciofinal;
               $beca_cita->user_id=auth()->user()->id;
               $beca_cita->save();
             
               $monto_family_aux=($monto_family_aux)-$beca_cita->equivalente;/////Se resta el monto de family al equivalente de la beca
               $beca_des=Beca_descuento::where('id',$beca_cita->beca_id)->first();
               if($beca_des->tiene_saldo!='N'){
                $apoyo_registro=apoyo_registros::where('beca_id',$beca_des->id)->orderBy('id','desc')->first();
                $new_apoyo_registro=new apoyo_registros();
                $saldo_final=$apoyo_registro->saldo_final;
                $saldo_inicial=$apoyo_registro->saldo_inicial;
                $sa_final=$saldo_final-$beca_cita->equivalente;
                $new_apoyo_registro->cita_id=$event->id;
                $new_apoyo_registro->beca_id=$beca_des->id;
                $new_apoyo_registro->apoyo_id=$apoyo_registro->apoyo_id;
                $new_apoyo_registro->saldo_inicial=$saldo_final;
                $new_apoyo_registro->saldo_final=$sa_final;
                $new_apoyo_registro->abono=0;
                $new_apoyo_registro->user_id=$event->id_usuario;
                $new_apoyo_registro->cargo=$beca_cita->equivalente;
                $new_apoyo_registro->save();
            }
        }
    }elseif ($afecta=="consultor") {

       if($monto_porcentaje=="monto"){
           $beca_cita=new beca_cita();
           $beca_cita->cita_id=$event->id;
           $beca_cita->user_id=$event->id_usuario;
           $beca_cita->beca_id=$beca['beca_id'];
           $monto=$beca['cantidad'];
           $preciofinal=$preciofinal-$monto;
           $beca_cita->cantidad=$monto;
           $beca_cita->equivalente=$monto;
           $beca_cita->monto=$preciofinal;
           $beca_cita->save();
           $monto_consultor_aux=$monto_consultor_aux-$monto;

       }
   }

}
$event->ingreso_consultor=$monto_consultor_aux;
$event->ingreso_family=$monto_family_aux;
$event->save();
}else{
     if($event->tipo_cita_id!=3){
$monto_consultor_aux =($porcentaje_consultor/100)*$preciofinal;
   $monto_family_aux =($porcentaje_family/100)*$preciofinal;
   $event->ingreso_consultor=$monto_consultor_aux;
   $event->ingreso_family=$monto_family_aux;
   $event->save();
     }else{
         $monto_family_aux =$preciofinal;
   $event->ingreso_consultor=0;
   $event->ingreso_family=$monto_family_aux;
   $event->save();
     }

 
}
$service = new Google_Service_Calendar($this->client);
// Crear un nuevo evento
$event_ = new Google_Service_Calendar_Event();
$nombre_paciente=$event->paciente->nombrecompleto;

$event_->setSummary($nombre_paciente);
$event_->setDescription('null');

// Fecha de inicio (del día actual a las 8am)
$start = new Google_Service_Calendar_EventDateTime();
$date_ = new DateTime($event->start, new DateTimeZone('America/Mexico_City'));
$start->setDateTime($date_->format('Y-m-d').'T'.$date_->format('H:i:s'));

$start->setTimeZone('America/Mexico_City');
$event_->setStart($start);

// Fecha de cierre (del día actual a las 6pm)
$end = new Google_Service_Calendar_EventDateTime();
$date = new DateTime($event->end, new DateTimeZone('America/Mexico_City'));

$end->setDateTime($date->format('Y-m-d').'T'.$date->format('H:i:s'));
$end->setTimeZone('America/Mexico_City');
$event_->setEnd($end);

$remindersArray = array();

// Crear recordatorios
$reminder = new Google_Service_Calendar_EventReminder();
$reminder->setMethod('email');
$reminder->setMinutes(3600);
$remindersArray[] = $reminder;



$reminders = new Google_Service_Calendar_EventReminders();
$reminders->setUseDefault(false);
$reminders->setOverrides($remindersArray);
$event_->setReminders($reminders);

$attendees = array();

// Agregar los Participantes
$attendee = new Google_Service_Calendar_EventAttendee();
$attendee->setEmail($consultor->user->email);
$attendees[] = $attendee;/*
$attendee = new Google_Service_Calendar_EventAttendee();
$attendee->setEmail('family@colmenares.org.mx');
$attendees[] = $attendee;/*
$attendee = new Google_Service_Calendar_EventAttendee();
$attendee->setEmail('sistemas@colmenares.org.mx');
$attendees[] = $attendee;*/
$event_->background=$event->color;
$event_->attendees = $attendees;
$calendarId='primary';
$createdEvent = $service->events->insert($calendarId, $event_, array(
   'sendNotifications' => true
));
$event->cita_id_google=$createdEvent->id;
$event->save();
return redirect()->route('home');
}
public function update_cita(Request $request)
{
   $monto_consultor=0;
   $monto_family=0;
   $consultor=Consultores::with('categorias','user')->where('id',$request->consultor)->first();
   $event=Citas::with('consultor')->where('id',$request->cita_id)->first();
   $event->consultor_id=$request->consultor;
   $event->consultorio_id=$request->consultorio_;
   $event->tipo_cita_id=$request->tipo_cita_;
   $event->paciente_id=$request->paciente;
   $event->status_cita_id=$request->status_cita_;
   if($request->status_cita_==3){
    $event->fecha_pago=date("Y-m-d H:m:s");
}
$event->color=$consultor->color;
$event->monto_inicial=$request->costo__;
$event->monto_final=$request->costo_final;
$event->fecha_cita=$request->date_start_;
$event->hora=$request->time_start_;
$event->title=$request->citas;
$event->descripcion=$request->observaciones;
$event->individual_pareja=$request->individual_pareja_;
$event -> start=$request->date_start_.' '.$request->time_start_;
$event -> end=$request->date_start_.' '.$request->date_end_;
$event->id_usuario=auth()->user()->id;
$event -> Save();   
//////Nuevo calculo de Precio de la beca
      //dd($request->toArray());
$delete_becas=beca_cita::where('cita_id',$request->cita_id)->delete();
$beca_con_fondo=apoyo_registros::where('cita_id',$request->cita_id)->first();

if($beca_con_fondo!=null){
    $cambios=true;
    if(!empty($request->becas)){

    foreach($request->becas as $beca_aux){

        if($beca_con_fondo->beca_id==$beca_aux['beca_id']){
            $cambios=false;
        }
    } 
    }
    ////Cambios al eliminar o agregar otra beca 
    if($cambios==true){
        $new_beca_fondo=new apoyo_registros;
        $beca_fondo_aux=apoyo_registros::where('beca_id',$beca_con_fondo->beca_id)->orderBy('id','desc')->first();
        $new_beca_fondo->cita_id=$beca_con_fondo->cita_id;
        $new_beca_fondo->beca_id=$beca_con_fondo->beca_id;
        $new_beca_fondo->apoyo_id=$beca_con_fondo->apoyo_id;
        $new_beca_fondo->saldo_inicial=$beca_fondo_aux->saldo_final;
        $new_beca_fondo->abono=0;
        $new_beca_fondo->cargo=-$beca_con_fondo->cargo;
        $new_beca_fondo->saldo_final=$beca_fondo_aux->saldo_final-$new_beca_fondo->cargo;
        $new_beca_fondo->user_id=auth()->user()->id;
        $new_beca_fondo->save();
    }
}
$preciofinal=$request->costo__;
$porcentaje_consultor=$consultor->categorias->porcentaje_consultor;
$porcentaje_family=$consultor->categorias->porcentaje_family;
if($request->becas!=''){

  $monto_consultor_aux=($porcentaje_consultor/100)*$preciofinal;
  $monto_family_aux=($porcentaje_family/100)*$preciofinal;

  foreach ($request->becas as $beca ) {
     $afecta=$beca['afecta'];
     $monto_porcentaje=$beca['monto_porcentaje'];
     if($afecta=="ambos"){
        if($monto_porcentaje=="monto"){
          if($event->tipo_cita_id!=3){
           $beca_cita=new beca_cita();
           $beca_cita->cita_id=$event->id;
           $beca_cita->beca_id=$beca['beca_id'];
           $monto=$beca['cantidad'];
           $preciofinal=$preciofinal-$monto;
           $beca_cita->cantidad=$monto;
           $beca_cita->equivalente=$monto;
           $beca_cita->monto=$preciofinal;
            $beca_cita->user_id=auth()->user()->id;
           $beca_cita->save();

           $monto_consultor=($porcentaje_consultor/100)*$monto;
           $monto_family=($porcentaje_family/100)*$monto;
           $monto_consultor_aux=$monto_consultor_aux-$monto_consultor;
           $monto_family_aux=$monto_family_aux-$monto_family;
 
         }else{
           $beca_cita=new beca_cita();
           $beca_cita->cita_id=$event->id;
           $beca_cita->beca_id=$beca['beca_id'];
           $monto=$beca['cantidad'];
           $preciofinal=$preciofinal-$monto;
           $beca_cita->cantidad=$monto;
           $beca_cita->equivalente=$monto;
           $beca_cita->monto=$preciofinal;
            $beca_cita->user_id=auth()->user()->id;
           $beca_cita->save();

           $monto_consultor=0;
           $monto_family=$preciofinal;
           $monto_consultor_aux=$monto_consultor;
           $monto_family_aux=$monto_family;
         }
           
       }elseif ($monto_porcentaje=="porcentaje") {

           $beca_cita=new beca_cita();
           $beca_cita->cita_id=$event->id;
           $beca_cita->beca_id=$beca['beca_id'];
           $porcentaje=$beca['cantidad'];
           if($porcentaje!=100){

           $beca_cita->cantidad=$porcentaje/100;
         }else{
           $beca_cita->cantidad=$porcentaje;

         }
           $beca_cita->equivalente=($porcentaje/100)*$preciofinal;
           $porcentaje_aux=100-$porcentaje;
           $porcentaje=$porcentaje_aux/100;
           $preciofinal=$porcentaje*$preciofinal;
           $beca_cita->monto=$preciofinal;
            $beca_cita->user_id=auth()->user()->id;
           $beca_cita->save();
           $monto_consultor_aux=($monto_consultor_aux)*$porcentaje;
           $monto_family_aux=($monto_family_aux)*$porcentaje;

       }

   }elseif ($afecta=="institucion") {
    if ($monto_porcentaje=="porcentaje") {
       $beca_cita=new beca_cita();
       $beca_cita->cita_id=$event->id;
       $beca_cita->beca_id=$beca['beca_id'];
       $porcentaje=$beca['cantidad'];
            if($porcentaje!=100){

           $beca_cita->cantidad=$porcentaje/100;
         }else{
           $beca_cita->cantidad=$porcentaje;

         }
       $beca_cita->equivalente=($porcentaje/100)*$preciofinal;
       $porcentaje_aux=100-$porcentaje;
       $porcentaje=$porcentaje_aux/100;
       $preciofinal=$porcentaje*$preciofinal;
       $beca_cita->user_id=auth()->user()->id;
       $beca_cita->monto=$preciofinal;
       $beca_cita->save();
   
       $monto_family_aux=($monto_family_aux)-$beca_cita->equivalente;
       $beca_des=Beca_descuento::where('id',$beca_cita->beca_id)->first();
       if($beca_des->tiene_saldo!='N'){
        $apoyo_registro=apoyo_registros::where('beca_id',$beca_des->id)->orderBy('id','desc')->first();
        $update_beca_saldo=apoyo_registros::where([['beca_id',$beca_des->id],['cita_id',$event->id]])->orderBy('id','Desc')->first();

        if($update_beca_saldo!=null){
          if($update_beca_saldo->cargo!=$beca_cita->equivalente){
             $new_apoyo_registro=new apoyo_registros();
             $saldo_final=$apoyo_registro->saldo_final;
             $saldo_inicial=$apoyo_registro->saldo_inicial;
             $sa_final=$saldo_final-$beca_cita->equivalente;
             $new_apoyo_registro->cita_id=$event->id;
             $new_apoyo_registro->beca_id=$beca_des->id;
             $new_apoyo_registro->apoyo_id=$apoyo_registro->apoyo_id;
             $new_apoyo_registro->saldo_inicial=$saldo_final;
             $new_apoyo_registro->saldo_final=$sa_final;
             $new_apoyo_registro->abono=0;
             $new_apoyo_registro->cargo=$beca_cita->equivalente;
              $new_apoyo_registro->user_id=auth()->user()->id;
             $new_apoyo_registro->save();
         }
     }else{

        $new_apoyo_registro=new apoyo_registros();
        $saldo_final=$apoyo_registro->saldo_final;
        $saldo_inicial=$apoyo_registro->saldo_inicial;
        $sa_final=$saldo_final-$beca_cita->equivalente;
        $new_apoyo_registro->cita_id=$event->id;
        $new_apoyo_registro->beca_id=$beca_des->id;
        $new_apoyo_registro->apoyo_id=$apoyo_registro->apoyo_id;
        $new_apoyo_registro->saldo_inicial=$saldo_final;
        $new_apoyo_registro->saldo_final=$sa_final;
        $new_apoyo_registro->abono=0;
        $new_apoyo_registro->cargo=$beca_cita->equivalente;
        $new_apoyo_registro->user_id=auth()->user()->id;
        $new_apoyo_registro->save();
    }
}
}
            # code...
}elseif ($afecta=="consultor") {

   if($monto_porcentaje=="monto"){
       $beca_cita=new beca_cita();
       $beca_cita->cita_id=$event->id;
       $beca_cita->beca_id=$beca['beca_id'];
       $monto=$beca['cantidad'];
       $preciofinal=$preciofinal-$monto;
       $beca_cita->cantidad=$monto;
       $beca_cita->equivalente=$monto;
       $beca_cita->monto=$preciofinal;
        $beca_cita->user_id=auth()->user()->id;
       $beca_cita->save();
       $monto_consultor_aux=$monto_consultor_aux-$monto;

   }
}

}
$event->ingreso_consultor=$monto_consultor_aux;
$event->ingreso_family=$monto_family_aux;

$event->save();
}else{
      if($event->tipo_cita_id!=3){
$monto_consultor_aux =($porcentaje_consultor/100)*$preciofinal;
   $monto_family_aux =($porcentaje_family/100)*$preciofinal;
   $event->ingreso_consultor=$monto_consultor_aux;
   $event->ingreso_family=$monto_family_aux;
   $event->save();
     }else{
         $monto_family_aux =$preciofinal;
   $event->ingreso_consultor=0;
   $event->ingreso_family=$monto_family_aux;
   $event->save();
     }
}


$service = new Google_Service_Calendar($this->client);
// Crear un nuevo evento
     $event_ = $service->events->get('primary',$event->cita_id_google);
$nombre_paciente=null;
if(!empty($event->paciente->alias)){
    $nombre_paciente=$event->paciente->alias." -Pac ".$event->consultor->nombrecompleto;
}else{
    $nombre_paciente=$event->paciente->nombre." ".$event->paciente->apellido_pa." -Pac ".$event->consultor->nombrecompleto;

}
$event_->setSummary($nombre_paciente);
$event_->setDescription('null');

// Fecha de inicio (del día actual a las 8am)
$start = new Google_Service_Calendar_EventDateTime();
$date_ = new DateTime($event->start, new DateTimeZone('America/Mexico_City'));
$start->setDateTime($date_->format('Y-m-d').'T'.$date_->format('H:i:s'));

$start->setTimeZone('America/Mexico_City');
$event_->setStart($start);

// Fecha de cierre (del día actual a las 6pm)
$end = new Google_Service_Calendar_EventDateTime();
$date = new DateTime($event->end, new DateTimeZone('America/Mexico_City'));

$end->setDateTime($date->format('Y-m-d').'T'.$date->format('H:i:s'));
$end->setTimeZone('America/Mexico_City');
$event_->setEnd($end);

$remindersArray = array();

// Crear recordatorios
$reminder = new Google_Service_Calendar_EventReminder();
$reminder->setMethod('email');
$reminder->setMinutes(10);
$remindersArray[] = $reminder;



$reminders = new Google_Service_Calendar_EventReminders();
$reminders->setUseDefault(false);
$reminders->setOverrides($remindersArray);
$event_->setReminders($reminders);

$attendees = array();

// Agregar los Participantes
$attendee = new Google_Service_Calendar_EventAttendee();
$attendee->setEmail($consultor->user->email);
$attendees[] = $attendee;/*
$attendee = new Google_Service_Calendar_EventAttendee();
$attendee->setEmail('family.recepcion@gmail.com');
$attendees[] = $attendee;*/
$attendee = new Google_Service_Calendar_EventAttendee();
$attendee->setEmail('sistemas@colmenares.org.mx');
$attendees[] = $attendee;

$event_->attendees = $attendees;
$event_->background=$event->color;
$event_->foreground=$event->color;
$calendarId='primary';
if($event->status_cita_id!=2){
   $updatedEvent = $service->events->update('primary', $event->cita_id_google, $event_,array('sendNotifications'=>true));

}else{
    $deleteEvent=$service->events->delete($calendarId,$event->cita_id_google,array('sendNotifications'=>true));
}


return redirect()->route('home');
}
    /**
     * Display the specified resource.
     *
     * @param  \App\Citas  $citas
     * @return \Illuminate\Http\Response
     */
    public function show(Citas $citas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Citas  $citas
     * @return \Illuminate\Http\Response
     */
    public function edit(Citas $citas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Citas  $citas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Citas $citas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Citas  $citas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Citas $citas)
    {
        //
    }
    public function emergente_citas(){
 
  $citas->Citas::where([['consultor_id',17],['fecha_cita','2019-09-27']])->get();
  foreach($citas as $cita){
    $email=$cita->consultor->user->email;
    $client=self::GetClient();
    
    $service = new Google_Service_Calendar($client);
    $event_rastrear=$service->events->get('primary',$cita->$cita_id_google);
    dd($event_rastrear);
// Crear un nuevo evento
/*$event_ = new Google_Service_Calendar_Event();
$nombre_paciente=$cita->paciente->nombrecompleto;

$event_->setSummary($nombre_paciente);
$event_->setDescription('null');

// Fecha de inicio (del día actual a las 8am)
$start = new Google_Service_Calendar_EventDateTime();
$date_ = new DateTime($cita->start, new DateTimeZone('America/Mexico_City'));
$start->setDateTime($date_->format('Y-m-d').'T'.$date_->format('H:i:s'));

$start->setTimeZone('America/Mexico_City');
$event_->setStart($start);

// Fecha de cierre (del día actual a las 6pm)
$end = new Google_Service_Calendar_EventDateTime();
$date = new DateTime($cita->end, new DateTimeZone('America/Mexico_City'));

$end->setDateTime($date->format('Y-m-d').'T'.$date->format('H:i:s'));
$end->setTimeZone('America/Mexico_City');
$event_->setEnd($end);

$remindersArray = array();

// Crear recordatorios
$reminder = new Google_Service_Calendar_EventReminder();
$reminder->setMethod('email');
$reminder->setMinutes(3600);
$remindersArray[] = $reminder;



$reminders = new Google_Service_Calendar_EventReminders();
$reminders->setUseDefault(false);
$reminders->setOverrides($remindersArray);
$event_->setReminders($reminders);

$attendees = array();

// Agregar los Participantes
$attendee = new Google_Service_Calendar_EventAttendee();
$attendee->setEmail($email);
$attendees[] = $attendee;/*
$attendee = new Google_Service_Calendar_EventAttendee();
$attendee->setEmail('family.recepcion@gmail.com');
$attendees[] = $attendee;*/
$event_->background=$cita->color;
$event_->attendees = $attendees;
$calendarId='primary';

$createdEvent = $service->events->insert($calendarId, $event_, array(
   'sendNotifications' => 'all'
));
dd($event_);
$cita->cita_id_google=$createdEvent->id;
$cita->save();

  $cita_emer=Citas::where('id',$cita->id)->first();
  $cita_emer->cita_id_google=$createdEvent->id;
  
  $cita_emer->save();
  }
}
}
