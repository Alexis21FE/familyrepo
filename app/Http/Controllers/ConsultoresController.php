<?php

namespace App\Http\Controllers;
use App\Tipo_citas;
use App\Pacientes;
use App\Consultorio;
use App\Consultores;
use App\Beca_descuento;
use App\consultor_categoria;
use App\status_cita;
use App\Especialidades;
use App\horario_consultor;
use App\User;
use App\Citas;
use App\Caso;
use App\seguimiento;
use App\Diagnostico;
use App\familia;
use App\consultores_especialidad;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;
use App\clasificacion;
use App\Mail\passwordMail;
use DB;
use App\datos_significativos;
class ConsultoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth','roles:admin,consul,direc,recep']);
    }
    public function datos(){
        $user_id=auth()->user()->consultor->id;
        $dias=array('lunes','martes','miercoles','jueves','viernes','sabado');
        $especialidades=Especialidades::orderBy('id')->get();
        $categorias=consultor_categoria::orderBy('id')->pluck('descripcion', 'id');
        $consultor=Consultores::with('Especialidades','categorias')->where([['activo','A'],['id',$user_id]])->first();
        $datos_Consultor=true;
        return view('consultores.consultorDatos',compact('consultor','especialidades','categorias','datos_Consultor','dias'));
        
    }
    public function checkBaja(Request $request){
        $consultor_id=$request->consultor_id;
        $casos=Caso::where([['consultor_id',$consultor_id],['status','Alta']])->get();
   
        if($casos!=null){
            $success=true;
        }else{
            $success=false;
        }
        return response()->json([
    'status' => $success,
]);
    }
    public function calendarDirector(Request $request){
        $calendario_tipo=$request->calendario_type;
        if(auth()->user()->hasRoles(['direc','consul'])){
        if($calendario_tipo=='director'){
            $tipo_citas=Tipo_citas::orderBy('id')->pluck('descripcion', 'id');
    $pacientes=Pacientes::orderBy('id')->get();
    $consultorios=Consultorio::orderBy('id')->pluck('descripcion', 'id');
    $status_cita=status_cita::orderBy('id')->pluck('descripcion','id');
    $consultores=Consultores::with('Especialidades')->get();
    $becas_descuentos=Beca_descuento::orderBy('id')->get();
    $porcentaje_becas=range(0, 100, 10);
    //dd($tipo_citas);
        return view('director/calendario',compact('calendario_tipo','tipo_citas','pacientes','porcentaje_becas','consultorios','consultores','consultores_select','becas_descuentos','status_cita'));

        }elseif($calendario_tipo=='consultor'){
            $consultor_id=auth()->user()->consultor->id;
     $tipo_citas=Tipo_citas::orderBy('id')->pluck('descripcion', 'id');

     $consultorios=Consultorio::orderBy('id')->pluck('descripcion', 'id');

     $status_cita=status_cita::orderBy('id')->pluck('descripcion','id');

     $consultores=Consultores::with('Especialidades')->get();

     $becas_descuentos=Beca_descuento::orderBy('id')->get();

     return view('director/calendario',compact('calendario_tipo','consultor_id','tipo_citas','consultorios','status_cita','consultores','becas_descuentos'));


        }
    }
}
    
    
    public function testes(Request $request){
        $paciente_id=$request->paciente_id;
        $consultor_id=auth()->user()->consultor->id;
        $pacientes=Pacientes::whereHas('diagnosticos', function ($query) use ($consultor_id) {
            $query->where('consultor_asig_id',$consultor_id);
        })->get();
        $pacientes_=Pacientes::whereHas('diagnosticos', function ($query) use ($consultor_id) {
            $query->where([['consultor_id',$consultor_id],['consultor_asig_id','']]);
        })->get();
        $response=true;
        $clasificaciones=clasificacion::orderby('id','asc')->get();
        $consultores=Consultores::get();
        $consultas=seguimiento::where('paciente_id',$paciente_id)->orderBy('id','desc')->get();
        $cita=Caso::with('paciente','Consultor')->where('paciente_id',$paciente_id)->first();
               $familiares=familia::where('paciente_id',$paciente_id)->get();
        $datos_significativos=datos_significativos::where('paciente_id',$paciente_id)->first();
        $last_date=Citas::where([['consultor_id',$consultor_id],['paciente_id',$paciente_id]])->orderBy('id','desc')->first();
        return view('consultores.test',compact('cita','pacientes','pacientes_','consultores','consultas','response','clasificaciones','familiares','datos_significativos','last_date'));
       // dd($request->toArray());
        //return view('consultores.test');
    }
    
    public function index()
    {

     $consultor_id=auth()->user()->consultor->id;
     $tipo_citas=Tipo_citas::orderBy('id')->pluck('descripcion', 'id');

     $consultorios=Consultorio::orderBy('id')->pluck('descripcion', 'id');

     $status_cita=status_cita::orderBy('id')->pluck('descripcion','id');

     $consultores=Consultores::with('Especialidades')->get();

     $becas_descuentos=Beca_descuento::orderBy('id')->get();

     return view('consultores/calendarioConsultores',compact('consultor_id','tipo_citas','consultorios','status_cita','consultores','becas_descuentos'));

 }
 public function showFormNew()
 {
        //dd($especialidades);
    $especialidades=Especialidades::orderBy('id')->pluck('descripcion', 'id');
    $categorias=consultor_categoria::orderBy('id')->pluck('descripcion', 'id');
    return view('consultores/formConsultores',compact('especialidades','categorias'));
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }
    public function updateDiagnostico(Request $request){

        $now=Carbon::now();
        $now_format=$now->format('Y-m-d');
        $diagnostico=Diagnostico::where('id',$request->diagnostico_id)->first();
        $caso=Caso::where('paciente_id',$diagnostico->paciente_id)->first();
        $diagnostico->fecha_diagnostico=$now_format;
        $diagnostico->clasificacion_id=$request->clasificacion_id;
        $diagnostico->consultor_id=$request->consultor_diagnostico;
        $diagnostico->paciente_id=$request->paciente_id;
        $diagnostico->razon_ingreso=$request->razon_ingreso;
        $diagnostico->hipotesis=$request->diagnostico;
        $diagnostico->num_sesiones=$request->Numero_sesiones;
        $diagnostico->consultor_asig_id=$request->consultor_asignado;
        $caso->consultor_id=$request->consultor_asignado;
        $caso->save();
        $diagnostico->updated_at=$now;
        $diagnostico->fecha_modificacion=$now_format;
        if($diagnostico->save()){
            $status=true;
            $msg='Se ha actuliazado el diagnostico';
        }else{
            $status=true;
            $msg='Ha ocurruido un problema al actuliazar el diagnostico';    
        }
  return response()->json([
    'status' => $status,
    'msg'=>$msg
]);// return back();
    }

    public function showPacientes($id){
        $test=Diagnostico::with('consultor','paciente','consultor_asignado')->get();
            //dd($test->toArray());
        $consultor_id=auth()->user()->consultor->id;
            $pacientes=DB::table('pacientes')->select('pacientes.*',DB::raw("concat(consultores.nombre,' ',consultores.apellido_pa) as nombreconsultor"),'consultores.color')->join('consultores','pacientes.consultor','=','consultores.id')
        ->join('casos','pacientes.id','=','casos.paciente_id')->where([['casos.status','!=','Alta'],['consultor_id',$consultor_id]])
        ->orderBy('nombreconsultor','asc')->orderBy('nombrecompleto','asc')->get();

        $clasificaciones=clasificacion::orderBy('id','asc')->get();
        $consultores=Consultores::get();
        $cita=Citas::with('paciente','consultor')->where('id',$id)->first();
        $familiares=familia::where('paciente_id',$cita->paciente_id)->get();
        $datos_significativos=datos_significativos::where('paciente_id',$cita->paciente_id)->first();
        $consultas=seguimiento::where([['consultor_id',$consultor_id],['paciente_id',$cita->paciente_id]])->orderBy('id','desc')->get();
        return view('consultores.Pacientes',compact('familiares','cita','pacientes','pacientes_','clasificaciones','consultores','consultas','test','datos_significativos'));
    }
    public function addSeguimiento(Request $request){
       $now=Carbon::now();
      if($request->ajax()){
        $seguimiento=seguimiento::where('id',$request->session_id)->first();
      }else{
        $seguimiento=new seguimiento;
        $seguimiento->fecha_modificacion=$now;
      }
        $seguimiento->paciente_id=$request->paciente_id;
        $seguimiento->consultor_id=$request->consultor_id;
        $seguimiento->cita_id=($request->cita_id!='')?$request->cita_id:'0';
        $seguimiento->caso_id=$request->caso_id;
        $seguimiento->observaciones=$request->descripcion;
        $seguimiento->id_usuario=$request->consultor_id;
        $seguimiento->no_sesion=$request->Numero_de_sesion;
        $seguimiento->cerrar=($request->Cerrar!='')?$request->Cerrar:'0';

        if($seguimiento->save()){
            $success=true;
            $msg="Exito al actuliazar la cita";
        }else{
            $success=false;
            $msg="Error al actuliazar la cita";
        }
 if($request->ajax()){
  return response()->json([
    'status' => $success,
    'msg'=>$msg
]);
    
      }else{
      return redirect()->route('home'); 
      }
          

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $now=Carbon::now();
        if(isset($request->consultor_id)){
        $consultor_especialidad=consultores_especialidad::where('consultores_id',$request->consultor_id)->delete();
        $usuario=User::where('email',$request->mail)->first();
        $consultor=Consultores::where('id',$request->consultor_id)->first();
             }else{
                 $resultado_name = substr("$request->name", 0,2);
            $resultado_ap_pa = substr("$request->ap_pa", 0,2);
            $resultado_ap_ma = substr("$request->ap_ma", 0,2);
            $psswd = substr( md5(microtime()), 1, 8);
            $info = array();
            $usuario = User::create([
                'name'=>$request->name , 
                'email'=>$request->mail,
                'password'=> bcrypt($psswd),
                'role_id'=>3
            ]);
                $consultor=new Consultores;
         $info = array_add($info, 'pass' , $psswd);
         $info = array_add($info, 'email',$request->mail);
                Mail::to($usuario->email)->bcc("family.recepcion@gmail.com")->send(new passwordMail($info));
        }
        $consultor->user_id=$usuario->id;
        $consultor->color=$request->color;
        $consultor->nombre= $request->name;
        $consultor->apellido_pa= $request->ap_pa;
        $consultor->apellido_ma= $request->ap_ma; 
        $consultor->celular_paciente=$request->celular_paciente;
        $consultor->telefono_paciente=$request->telefono_paciente;
        $consultor->direccion_paciente=$request->direccion_paciente;
        $consultor->numero_ext=$request->numero_ext;
        $consultor->numero_int=$request->numero_int;
        $consultor->colonia=$request->colonia;
        $consultor->color=$request->color;
        $consultor->carrera=$request->carrera;
        $consultor->activo=$request->activo;
        $consultor->categoria_id=$request->categoria;
        //Verifica si existe el diagnostico y voluntariado 
        if(isset($request->da_diagnostico)){
            if($request->da_diagnostico=='value'){
                $request->da_diagnostico='S';
            }else{
                $request->da_diagnostico='N';
            }
        }else{
         $request->da_diagnostico='N';
         $request->diagnostico_costo=0;
     }
     if(isset($request->da_voluntariado)){

        if($request->da_voluntariado=='value'){
            $request->da_voluntariado='S';
        }else{
            $request->da_voluntariado='N';
        }
    }else{
     $request->da_voluntariado='N';
     $request->voluntariado_costo=0;
 }

 $consultor->diagnostico=$request->da_diagnostico;
 $consultor->diagnostico_costo=$request->diagnostico_costo;
 $consultor->voluntariado=$request->da_voluntariado;
 $consultor->voluntariado_costo=$request->voluntariado_costo;
 $consultor->costo_individual=$request->consultor_cost_ind;
 $consultor->costo_pareja=$request->consultor_cost_pareja;
 $consultor->costo_familiar=$request->consultor_cost_familiar;
 $consultor->fecha_registro_consultor=$now;
 $consultor->fecha_modificacion=$now;
 $consultor->save();
       //Inserta en la Tabla horarios
 if(!isset($request->consultor_id)){
     $horario_consultor=new horario_consultor;
 }else{
    $horario_consultor=horario_consultor::where('consultor_id',$request->consultor_id)->delete();
}
$horario_consultor=new horario_consultor;
  $horas_total=0;
for ($i=0; $i <=5 ; $i++) { 
  
    $dia='dia'.''.$i;
    $hora_entrada='hora_entrada'.''.$i;
    $hora_salida ='hora_salida'.''.$i;
    $horas_disponible='horas_disponible'.''.$i;
    $day=$request->$dia;
    if(!isset($day)){
    }else{
        echo $horas_total."<br>";
        $dia_entrada=$day.'_entrada';
        $dia_salida=$day.'_salida';
        $horario_consultor->$dia_entrada=$request->$hora_entrada;
        $horario_consultor-> $dia_salida=$request->$hora_salida;
        $horas_total=$horas_total + $request->$horas_disponible;
        echo $horas_total."<br>";
    }
}
$horario_consultor->consultor_id=$consultor->id;
$horario_consultor->Horas_disponible=$horas_total;
$horario_consultor->save();

//inserta en la tabla consultor_especialidads

foreach ($request->especialidades as $key => $value) {
    $consultor_especialidad= new consultores_especialidad;
    $consultor_especialidad->especialidades_id=$value;
    $consultor_especialidad->consultores_id=$consultor->id;
    $consultor_especialidad->save();
}
return redirect()->route('home');

}

    /**
     * Display the specified resource.
     *
     * @param  \App\Consultores  $consultores
     * @return \Illuminate\Http\Response
     */
    public function show(Consultores $consultores)
    {
        //
    }
    public function peticion(Request $request){
        $success='';
        $costo=0;
       
        if($request->ajax()){
         $consultor_id=$request->consultor_id;
            
            $consultor=Consultores::with('horario_consultor')->where('id',$consultor_id)->first();
            
             if($request->tipo_cita=='seguimiento'){
          $costo=$consultor->costo_individual;
          $success=true;
            if(!empty($request->individual_pareja)){

              if($request->individual_pareja=='Individual'){
                    $costo=$consultor->costo_individual;
                    $success=true;
                }elseif ($request->individual_pareja=='Pareja') {
                 $costo=$consultor->costo_pareja;
                 $success=true;
             }elseif($request->individual_pareja=='Familiar'){
 $costo=$consultor->costo_familiar;
                 $success=true;
             }
         }
        
      }
      elseif ($request->tipo_cita=='diagnostico') {
        if($consultor->diagnostico=='S'){
           $costo=$consultor->diagnostico_costo;
           $success=true;
                       if(!empty($request->individual_pareja)){

                if($request->individual_pareja=='Individual'){
                    $costo=$consultor->costo_individual;
                    $success=true;
                }elseif ($request->individual_pareja=='Pareja') {
                 $costo=$consultor->costo_pareja;
                 $success=true;
             }elseif($request->individual_pareja=='Familiar'){
 $costo=$consultor->costo_familiar;
                 $success=true;
             }
         }
        
       }else{
        $costo='Este consultor no realiza diagnosticos';
        $success='Error';
    }

}elseif ($request->tipo_cita=='voluntariado') {
    if($consultor->voluntariado=='S'){
       $costo=$consultor->voluntariado_costo;
       $success=true;
   }else{
    $costo='Este consultor no es voluntario';
    $success='Error';
}
}elseif($request->tipo_cita=='retoralimentacion'){
    $costo=0;
     $success=true;
}
     $pacientes=DB::table('pacientes')->select('pacientes.*',DB::raw("concat(consultores.nombre,' ',consultores.apellido_pa) as nombreconsultor"),'consultores.color')->join('consultores','pacientes.consultor','=','consultores.id')
        ->join('casos','pacientes.id','=','casos.paciente_id')->where([['casos.status','!=','Alta'],['consultor_id',$consultor->id]])
        ->orderBy('nombreconsultor','asc')->orderBy('nombrecompleto','asc')->get();
 $pacientes_dif=DB::table('pacientes')->select('pacientes.*',DB::raw("concat(consultores.nombre,' ',consultores.apellido_pa) as nombreconsultor"),'consultores.color')->join('consultores','pacientes.consultor','=','consultores.id')
        ->join('casos','pacientes.id','=','casos.paciente_id')->where([['casos.status','!=','Alta'],['consultor_id','!=',$consultor->id]])
        ->orderBy('nombreconsultor','asc')->orderBy('nombrecompleto','asc')->get();
$dispo=false;
$disponible=false;
if(isset($request->fecha)){
    $date=$request->fecha;
    $date=date_create($date);
    $date= date_format($date,"l");
    switch ($date) {
       case 'Monday':
       $dia="lunes";
       break;

       case 'Tuesday':
       $dia="martes";
       break;

       case 'Wednesday':
       $dia="miercoles";
       break;

       case 'Thursday':
       $dia="jueves";
       break;

       case 'Saturday':
       $dia="sabado";
       break;

       case 'Sunday':
       $dia="domingos";
       break;

       case 'Friday':
       $dia="viernes";
       break;

       default:
       break;
   }
   $horario_entrada=$dia."_entrada";
   $horario_salida=$dia."_salida";
   
   $disponible=horario_consultor::where([['consultor_id',$consultor_id],[$horario_entrada,'<=',$request->start],[$horario_salida,'>=',$request->end]])->get();
  
   $start=$request->fecha." ".$request->start;
   $end=$request->fecha." ".$request->end;
   $end=date_create($end);
   date_modify($end,"+1 hour");
   $end=date_format($end,"Y-m-d H:m:s");
   $start=date_create($start);
   date_modify($start,"-1 hour");
   $start=date_format($start,"Y-m-d H:m:s");
   $cita_=Citas::where('consultor_id',$consultor_id)->whereBetween('start',[$start, $end])->get();
   if($disponible->count()>0){
    if($cita_->count()>0)
    {

        $dispo=false;
    }else{
        $dispo=true;
    }
}else{
    $dispo=false;
}
}
return response()->json([
    'costo' => $costo,
    'status' => $success,
    'disponible'=>$dispo,
    'pacientes'=>$pacientes,
    'pacientes_dif'=>$pacientes_dif,
    'horario'=>$disponible,
    'consultor'=>$consultor
]);
}

}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Consultores  $consultores
     * @return \Illuminate\Http\Response
     */
    public function edit(Consultores $consultores)
    {
        //
    }
    public function showAll(){
          // $test=Diagnostico::with('consultor','paciente','consultor_asignado')->get();
            //dd($test->toArray());
    
        $consultor_id=auth()->user()->consultor->id;
      $pacientes=DB::table('pacientes')->select('pacientes.*',DB::raw("concat(consultores.nombre,' ',consultores.apellido_pa) as nombreconsultor"),'consultores.color')->join('consultores','pacientes.consultor','=','consultores.id')
        ->join('casos','pacientes.id','=','casos.paciente_id')->where([['casos.status','=','Seguimiento'],['consultor_id',$consultor_id]])
        ->orderBy('nombreconsultor','asc')->orderBy('nombrecompleto','asc')->get();

/*        $pacientes_=Pacientes::whereHas('diagnosticos', function ($query) use ($consultor_id) {
            $query->where([['consultor_id',$consultor_id],['consultor_asig_id','']]);
        })->orderBy('nombrecompleto','asc')->get();

        $consultores=Consultores::get();
        $consultas=seguimiento::where('consultor_id',$consultor_id)->get();*/
        //$cita=Citas::with('paciente','consultor')->where([['consultor_id',$consultor_id],['paciente_id',]])orderBy('id',DESC)->first();
        $response=false;
        return view('consultores/pacientesView',compact('pacientes','pacientes_'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Consultores  $consultores
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Consultores $consultores)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Consultores  $consultores
     * @return \Illuminate\Http\Response
     */
    public function destroy(Consultores $consultores)
    {
        //
    }
    public function emergente(){
        $consultores=User::where([['role_id',3],['activo','B']])->orderBy('id','asc')->get();
        foreach ($consultores as $value) {
             $psswd = substr( md5(microtime()), 1, 8);
             $consultor=User::where('id',$value->id)->first();
             $consultor->password=bcrypt($psswd);
             $consultor->save();
            $info = array();
             $info = array_add($info, 'pass' , $psswd);
         $info = array_add($info, 'email',$value->email);
                Mail::to($value->email)->bcc("family.recepcion@gmail.com")->send(new passwordMail($info));
            
        }
    }
    public function familia(Request $request){
              if(isset($request->familiar_id)){
            $familiar=familia::where('id',$request->familiar_id)->first();
            $msg="Se ha modificado correctamente un familiar";
        }else{
            
            $familiar=new familia();
            $msg="Se ha agregado un nuevo familiar";

        }

        $familiar->nombre=$request->nombre;
        $familiar->edad=$request->edad;
        $familiar->sexo=$request->sexo;
        $familiar->paciente_id=$request->paciente_id;
        $familiar->caso_id=$request->caso_id;
        $familiar->fecha_modificacion=date('Y-m-d H:m:s');
        $familiar->id_usuario=auth()->user()->consultor->id;
        $familiar->parentesco=$request->Parentesco;
        if($familiar->save()){
            $success=true;
        }else{
            $success=false;
        }
          return response()->json([
    'status' => $success,
    'msg'=>$msg,
    'familiar'=>$familiar
]);

    }
    public function familia_delete(Request $request){
        $familiar_id=$request->familiar_id;
        $exito=familia::where('id',$familiar_id)->delete();
        if($exito){
            $msg='Se ha eliminado correctamente al familiar';
            $success=true;
        }else{
            $msg='ha ocurrido un problema al eliminar al familiar';
            $success=false;
        }
        return response()->json([
    'status' => $success,
    'msg'=>$msg
]);
    }

    public function datos_significativos(Request $request){
      $paciente_id=$request->paciente_id;
    
      $datos_significativos=datos_significativos::where('paciente_id',$paciente_id)->first();
      if(empty($datos_significativos->paciente_id)){
        $datos_significativos=new datos_significativos;
      }
      $msg="Se han actuliazado los datos correctamente";

      $datos_significativos->Cambio_de_residencia=($request->Cambio_de_residencia!='')?$request->Cambio_de_residencia:'0';
      $datos_significativos->accidentes=($request->accidentes=!'')?$request->accidentes:'0';
      $datos_significativos->Muertes=($request->Muertes=!'')?$request->Muertes:'0';
      $datos_significativos->separacion=($request->separacion!='')?$request->separacion:'0';
      $datos_significativos->divorcio=($request->divorcio!='')?$request->divorcio:'0';
      $datos_significativos->escolar=($request->escolar!='')?$request->escolar:'0';
      $datos_significativos->perdida_empleo=($request->perdida_empleo!='')?$request->perdida_empleo:'0';
      $datos_significativos->enfermedades=($request->enfermedades!='')?$request->enfermedades:'0';
      $datos_significativos->economico=($request->economico!='')?$request->economico:'0';
      $datos_significativos->asalto=($request->asalto!='')?$request->asalto:'0';
      $datos_significativos->carcel=($request->carcel!='')?$request->carcel:'0';
      $datos_significativos->embarazo=($request->embarazo!='')?$request->embarazo:'0';
      $datos_significativos->genetico=($request->genetico!='')?$request->genetico:'0';
      $datos_significativos->otros=($request->otros!='')?$request->otros:'0';
      $datos_significativos->fecha_modificacion=date('Y-m-d');
      $datos_significativos->caso_id=$request->caso_id;
      $datos_significativos->paciente_id=$request->paciente_id;
      $datos_significativos->id_usuario=auth()->user()->consultor->id;
      if($datos_significativos->save()){
        $status=true;
      }else{
        $status=false;
      }
       return response()->json([
    'status' => $status,
    'msg'=>$msg
]);

    }
    public function PacienteAlta(Request $request){
        $caso=Caso::where('paciente_id',$request->paciente_id)->first();
        $caso->status='Alta';
        $caso->fecha_modificacion=date('Y-m-d H:m:s');
        $caso->save();
        $paciente=Pacientes::where('id',$request->paciente_id)->first();
        $paciente->status_id=3;
        $paciente->save();
        $status=true;
        $msg="Exito al dar de alta al paciente";
          return response()->json([
    'status' => $status,
    'msg'=>$msg
]);

    }  
      public function pacienteEliminar(Request $request){
        $seguimientos=seguimiento::where('paciente_id',$request->paciente_id)->get();
        if($seguimientos->count()>0){ 
          $msg="Este Paciente no puede ser eliminado";
        }
        $caso=Caso::where('paciente_id',$request->paciente_id)->first();
        $numero_caso=$caso->numero_caso;
        $caso->delete();
        $casos_aux=Caso::where('no_caso','>=',$numero_caso)->orderby('no_caso','asc')->get();
        foreach($casos_aux as $row){
          $row->no_caso=$row->no_caso-1;
          $row->save();
        }
        $caso->fecha_modificacion=date('Y-m-d H:m:s');
        $caso->save();
        $paciente=Pacientes::where('id',$request->paciente_id)->first();
        $paciente->status_id=3;
        $paciente->save();
        $status=true;
        $msg="Exito al dar de alta al paciente";
          return response()->json([
    'status' => $status,
    'msg'=>$msg
]);

    }
    public function pacienteAbandono(Request $request){

      $paciente=Pacientes::where('id',$request->paciente_id)->first();
      $paciente->status_id=4;
      $paciente->fecha_modificacion=date('Y-m-d H:m:s');
      $paciente->id_usuario=auth()->user()->id;
      $paciente->save();
      $caso=Caso::where('paciente_id',$request->paciente_id)->first();
      $caso->status='Abandono';
      $caso->fecha_modificacion=date('Y-m-d H:m:s');
      $caso->id_usuario=auth()->user()->id;
      $caso->save();
      $status=true;
      $msg="Exito al dar de baja este paciente.";
          return response()->json([
    'status' => $status,
    'msg'=>$msg
]);
    }
    public function revisacita(Request $request){
      $cita_id=$request->cita_id;
      $seguimiento=seguimiento::where('cita_id',$cita_id)->get();
      if($seguimiento->count()<1){
         $status=true;
    $msg="Exito";

    }else{
   $status=false;
      $msg="Esta cita ya esta registrada con un seguimiento, favor de verificar si tiene otra cita agendada. de lo contrario no podra registar otro seguimiento";
}
              return response()->json([
    'status' => $status,
    'msg'=>$msg
]);
    }
}
