<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Citas;
use App\status_cita;
use App\Beca_descuento;
use App\Pacientes;
use App\apoyo_registros;
use App\beca_cita;
use App\apoyo;
use App\Caso;
use App\Consultores;
use App\Tipo_citas;
use App\Consultorio; 
use Session;
use App\User;
use DB;
class recepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }
    public function __construct()
    {
        $this->middleware(['auth','roles:admin,direc,recep,consul']);
    }
     public function peticion(Request $request){
        $success='';
        $costo=0;
        if($request->ajax()){
            $consultor_id=$request->consultor_id;
            $consultor=Consultores::with('horario_consultor')->where('id',$consultor_id)->first();
             if($request->tipo_cita=='seguimiento'){
          $costo=$consultor->costo_individual;
          $success=true;
            if(!empty($request->individual_pareja)){

                if($request->individual_pareja=='Individual'){
                    $costo=$consultor->costo_individual;
                    $success=true;
                }elseif ($request->individual_pareja=='Pareja') {
                 $costo=$consultor->costo_familiar;
                 $success=true;
             }
         }
        
      }
      elseif ($request->tipo_cita=='diagnostico') {
        if($consultor->diagnostico=='S'){
           $costo=$consultor->diagnostico_costo;
           $success=true;
       }else{
        $costo='Este consultor no realiza diagnosticos';
        $success='Error';
    }

}elseif ($request->tipo_cita=='voluntariado') {
    if($consultor->voluntariado=='S'){
       $costo=$consultor->voluntariado_costo;
       $success=true;
   }else{
    $costo='Este consultor no es voluntario';
    $success='Error';
}
}elseif($request->tipo_cita=='retoralimentacion'){
    $costo=0;
     $success=true;
}

     $pacientes=DB::table('pacientes')->select('pacientes.*',DB::raw("concat(consultores.nombre,' ',consultores.apellido_pa) as nombreconsultor"),'consultores.color')->join('consultores','pacientes.consultor','=','consultores.id')
        ->join('casos','pacientes.id','=','casos.paciente_id')->where([['casos.status','!=','Alta'],['consultor_id',$consultor->id]])
        ->orderBy('nombreconsultor','asc')->orderBy('nombrecompleto','asc')->get();
 $pacientes_dif=DB::table('pacientes')->select('pacientes.*',DB::raw("concat(consultores.nombre,' ',consultores.apellido_pa) as nombreconsultor"),'consultores.color')->join('consultores','pacientes.consultor','=','consultores.id')
        ->join('casos','pacientes.id','=','casos.paciente_id')->where([['casos.status','!=','Alta'],['consultor_id','!=',$consultor->id]])
        ->orderBy('nombreconsultor','asc')->orderBy('nombrecompleto','asc')->get();

$dispo=false;
$disponible=false;
if(isset($request->fecha)){
    $date=$request->fecha;
    $date=date_create($date);
    $date= date_format($date,"l");
    switch ($date) {
       case 'Monday':
       $dia="lunes";
       break;

       case 'Tuesday':
       $dia="martes";
       break;

       case 'Wednesday':
       $dia="miercoles";
       break;

       case 'Thursday':
       $dia="jueves";
       break;

       case 'Saturday':
       $dia="sabado";
       break;

       case 'Sunday':
       $dia="domingos";
       break;

       case 'Friday':
       $dia="viernes";
       break;

       default:
       break;
   }
   $horario_entrada=$dia."_entrada";
   $horario_salida=$dia."_salida";
   $disponible=horario_consultor::where([['consultor_id',$consultor_id],[$horario_entrada,'<=',$request->start],[$horario_salida,'>=',$request->end]])->get();
   $start=$request->fecha." ".$request->start;
   $end=$request->fecha." ".$request->end;
   $end=date_create($end);
   date_modify($end,"+1 hour");
   $end=date_format($end,"Y-m-d H:m:s");
   $start=date_create($start);
   date_modify($start,"-1 hour");
   $start=date_format($start,"Y-m-d H:m:s");
   $cita_=Citas::where('consultor_id',$consultor_id)->whereBetween('start',[$start, $end])->get();

   if($disponible->count()>0){
    if($cita_->count()>0)
    {

        $dispo=false;
    }else{
        $dispo=true;
    }
}else{
    $dispo=false;
}
}
return response()->json([
    'costo' => $costo,
    'status' => $success,
    'disponible'=>$dispo,
    'pacientes'=>$pacientes,
    'pacientes_dif'=>$pacientes_dif,
    'horario'=>$disponible,
    'consultor'=>$consultor
]);
}

}
    public function corteDiario(){
       return view('reportes.corteDiario');
   }
   public function calculaa(Request $request){

    $inicio=$request->fecha_ini;
   $fin=$request->fecha_fin;
   $fin=date_create($fin);
   date_modify($fin,"+1 day");
   $fin=date_format($fin,"Y-m-d");
    $status_cita=status_cita::where('descripcion','pagada')->first();
    if(auth()->user()->hasRoles(['admin','recep'])){
    $citas_pagadas=Citas::with('paciente')->where('status_cita_id',$status_cita->id)->whereBetween('fecha_cita',[$inicio,$fin])->get();

    $citas_sin_paga=Citas::with('paciente')->whereNotIn('status_cita_id',[2,3])->whereBetween('fecha_cita',[$inicio,$fin])->get();
    $becas=Beca_descuento::orderBy('orden')->get();
  }elseif(auth()->user()->hasRoles(['direc']) ){
    if(Session::get('con_si',false)==false){
    $citas_pagadas=Citas::with('consultor','paciente','becas')->where('status_cita_id',$status_cita->id)->whereBetween('fecha_pago',[$inicio,$fin])->orderBy('fecha_cita','asc')->get();
    echo $inicio." ".$fin;
//dd($citas_pagadas);
    $citas_sin_paga=Citas::with('consultor','paciente','becas')->where([['status_cita_id','!=','2'],['status_cita_id','!=','3']])->whereBetween('fecha_cita',[$inicio,$fin])->get();
    $becas=Beca_descuento::orderBy('orden')->get();
   
    }else{
    $consultor_id=auth()->user()->consultor->id;
    $citas_pagadas=Citas::where([['status_cita_id',$status_cita->id],['consultor_id',$consultor_id]])->whereBetween('fecha_pago',[$inicio,$fin])->get();
      $citas_sin_paga=Citas::where('consultor_id',$consultor_id)->whereNotIn('status_cita_id',[2,3])->whereBetween('fecha_cita',[$inicio,$fin])->get();
    $becas=Beca_descuento::orderBy('orden')->get();
    }
  }elseif(auth()->user()->hasRoles(['consul'])){
$consultor_id=auth()->user()->consultor->id;
    $citas_pagadas=Citas::where([['status_cita_id',$status_cita->id],['consultor_id',$consultor_id]])->whereBetween('fecha_pago',[$inicio,$fin])->get();
    $citas_sin_paga=Citas::where('consultor_id',$consultor_id)->whereNotIn('status_cita_id',[2,3])->whereBetween('fecha_cita',[$inicio,$fin])->get();
    $becas=Beca_descuento::orderBy('orden')->get();
    
  }
    
    return view('reportes.cortelayout',compact('citas_pagadas','citas_sin_paga','becas'));
}
public function cita_pendiente(Request $request){
  $cita=Citas::with('paciente','consultor','status_cita')->where('id',$request->cita_id)->first();
    return response()->json([
        'event' =>$cita,
    ]);
}
public function recepdatos(){
  $datos=auth()->user();

  return view('reportes.recepDatos',compact('datos'));
}
public function changeDatos(Request $request){
    $user=User::where('email',$request->mail)->first();
  if(isset($request->pass)){
    $user->password=bcrypt($request->pass);
    $user->name=$request->nombre;
  }else{
    $user->name=$request->nombre;
  }
    if($user->save()){
      $msg="Exito al actulizar los datos";
      $status=true;
    }else{
      $msg="Error al actulizar los datos";
      $status=false;
    }
       return response()->json([
        'msg' =>$msg,
    ]);
}
    public function citas_pendientes(){
      $tipo_citas=Tipo_citas::orderBy('id')->pluck('descripcion', 'id');
    $pacientes=Pacientes::orderBy('id')->get();
    $consultorios=Consultorio::orderBy('id')->pluck('descripcion', 'id');
    $status_cita=status_cita::orderBy('id')->pluck('descripcion','id');
    $consultores=Consultores::with('Especialidades')->get();
    $becas_descuentos=Beca_descuento::orderBy('id')->get();
    $porcentaje_becas=range(0, 100, 10);
    
      $status_cita=status_cita::where('descripcion','pagada')->first();
      $citas=Citas::with('paciente','consultor')->where('status_cita_id','!=',$status_cita->id)->get();
      $status=status_cita::orderBy('id','asc')->get();
      return view('pacientes.citas_sinpago',compact('citas','status','tipo_citas','pacientes','porcentaje_becas','consultorios','consultores','consultores_select','becas_descuentos','status_cita'));
          }
    public function Detalladoporconsultor(){    
        $consultores=Consultores::orderBy('nombrecompleto','asc')->get();
        return view('reportes.Detalladoporconsultor',compact('consultores'));
    }
    public function DetalladoporconsultorFecha(Request $request){
       $inicio=$request->fecha_ini;
      $fin=$request->fecha_fin;
   $fin=date_create($fin);
   date_modify($fin,"+1 day");
   $fin=date_format($fin,"Y-m-d H:m:s");
   $consultor=$request->consultor;
       $status_cita=status_cita::where('descripcion','pagada')->first();

       $citas_pagadas=Citas::where([['status_cita_id',$status_cita->id],['consultor_id',$consultor]])->whereBetween('fecha_pago',[$inicio,$fin])->orderBy('consultor_id')->get();
       $citas_sin_paga=Citas::where([['status_cita_id','!=',$status_cita->id],['consultor_id',$consultor],['status_cita_id','!=','2']])->whereBetween('fecha_cita',[$inicio,$fin])->get();
       $becas=Beca_descuento::orderBy('orden')->get();
       $isResponse=true;
       $consultores=Consultores::orderBy('nombrecompleto','asc')->get();
       
       return view('reportes.Detalladoporconsultor',compact('citas_pagadas','inicio','citas_sin_paga','becas','isResponse','consultor','consultores'));
   }
   public function Registrodealtas(){
    $consultores=Consultores::orderBy('nombrecompleto','asc')->get();
    return view('reportes.Registrodealtas',compact('consultores'));
}
public function consultaAltas(Request $request){
   $inicio=$request->fecha_ini;
   $fin=$request->fecha_fin;
   $fin=date_create($fin);
   date_modify($fin,"+1 day");
   $fin=date_format($fin,"Y-m-d H:m:s");
   $pacientes=Pacientes::doesntHave('diagnosticos')->whereBetween('created_at',[$inicio,$fin])->get();
   $isResponse=true;
   return view('reportes.Registrodealtas',compact('pacientes','isResponse','inicio'));
}
public function damepacientes(Request $request){
  $consultor=$request->consultor;
  $pacientes=DB::table('pacientes')->select('pacientes.*',DB::raw("concat(consultores.nombre,' ',consultores.apellido_pa) as nombreconsultor"),'consultores.color')->join('consultores','pacientes.consultor','=','consultores.id')
        ->join('casos','pacientes.id','=','casos.paciente_id')->where([['casos.status','Seguimiento'],['consultor_id',$consultor]])
        ->orderBy('nombreconsultor','asc')->orderBy('nombrecompleto','asc')->get();
        return response()->json([
'pacientes'=>$pacientes
]);
}
public function PacientesDetallado(){
      $consultores=Consultores::orderBy('nombrecompleto','asc')->get();
     $pacientes=DB::table('pacientes')->select('pacientes.*',DB::raw("concat(consultores.nombre,' ',consultores.apellido_pa) as nombreconsultor"),'consultores.color')->join('consultores','pacientes.consultor','=','consultores.id')
        ->join('casos','pacientes.id','=','casos.paciente_id')->where('casos.status','!=','Alta')
        ->orderBy('nombreconsultor','asc')->orderBy('nombrecompleto','asc')->get();
    $paciente_response=null;
    return view('reportes.PacientesDetallado',compact('pacientes','paciente_response','consultores'));
}
public function consultaPacientes(Request $request){
 $paciente_id=$request->paciente;
 $consultor=$request->consultor;
  $inicio=$request->fecha_ini;
   $fin=$request->fecha_fin;
   $fin=date_create($fin);
   date_modify($fin,"+1 day");
   $fin=date_format($fin,"Y-m-d H:m:s");

 $paciente_response=Pacientes::with('diagnosticos')->whereHas('citas', function ($query) use ($inicio,$fin) {
     $query->whereBetween('fecha_cita',[$inicio,$fin])->orderBy('fecha_cita','ASC');
 })->where('id',$paciente_id)->first();
 $isResponse=true;
   $pacientes=DB::table('pacientes')->select('pacientes.*',DB::raw("concat(consultores.nombre,' ',consultores.apellido_pa) as nombreconsultor"),'consultores.color')->join('consultores','pacientes.consultor','=','consultores.id')
        ->join('casos','pacientes.id','=','casos.paciente_id')->where([['casos.status','!=','Alta'],['consultor_id',$consultor]])
        ->orderBy('nombreconsultor','asc')->orderBy('nombrecompleto','asc')->get();
          $consultores=Consultores::orderBy('nombrecompleto','asc')->get();

 return view('reportes.PacientesDetallado',compact('inicio','pacientes','paciente_response','isResponse','consultores','consultor'));
}
public function Detalleapoyo(){
    $apoyos=apoyo::all();
    return view('reportes.Detalleapoyo',compact('apoyos'));
}
public function consultaApoyos(Request $request){
    $inicio=$request->fecha_ini;
    $fin=$request->fecha_fin;
    $apoyo_id=$request->apoyo_id;
    $fin=date_create($fin);
    date_modify($fin,"+1 day");
    $fin=date_format($fin,"Y-m-d H:m:s");
     $apoyos=apoyo::all();
       $status_cita=status_cita::where('descripcion','pagada')->first();
    $registro_apoyos=apoyo_registros::with('becas_re','apoyo_re')->where([['cita_id','!=',0],['apoyo_id',$apoyo_id]])->whereHas('citas_re', function ($query) use ($inicio,$fin,$status_cita) {
     $query->where('status_cita_id',$status_cita->id)->whereBetween('fecha_pago',[$inicio,$fin])->orderBy('fecha_pago','Desc');
 })->get();
    $isResponse=true;
    return view('reportes.Detalleapoyo',compact('registro_apoyos','isResponse','apoyos','inicio','apoyo_id'));
}
public function Beneficiospaciente(){
    $pacientes=Pacientes::with('caso')->where('status_id','!=','3')->get();
         $pacientes=DB::table('pacientes')->select('pacientes.*',DB::raw("concat(consultores.nombre,' ',consultores.apellido_pa) as nombreconsultor"),'consultores.color')->join('consultores','pacientes.consultor','=','consultores.id')
        ->join('casos','pacientes.id','=','casos.paciente_id')->where('casos.status','!=','Alta')
        ->orderBy('nombreconsultor','asc')->orderBy('nombrecompleto','asc')->get();
    return view('reportes.Beneficiospaciente',compact('pacientes'));
}
public function beneficiosConsulta(Request $request){
    $inicio=$request->fecha_ini;
    $fin=$request->fecha_fin;
    $fin=date_create($fin);
    date_modify($fin,"+1 day");
    $paciente_id=$request->Paciente;
    $fin=date_format($fin,"Y-m-d H:m:s");
       $status_cita=status_cita::where('descripcion','pagada')->first();
    $becas_descuentos=beca_cita::with('becas_reg')->whereHas('citas_reg', function ($query) use ($inicio,$fin,$status_cita,$paciente_id) {
     $query->where([['status_cita_id',$status_cita->id],['paciente_id',$paciente_id]])->whereBetween('fecha_pago',[$inicio,$fin])->orderBy('fecha_pago','desc');})->get();
     $pacientes=DB::table('pacientes')->select('pacientes.*',DB::raw("concat(consultores.nombre,' ',consultores.apellido_pa) as nombreconsultor"),'consultores.color')->join('consultores','pacientes.consultor','=','consultores.id')
        ->join('casos','pacientes.id','=','casos.paciente_id')->where('casos.status','!=','Alta')
        ->orderBy('nombreconsultor','asc')->orderBy('nombrecompleto','asc')->get();
    $isResponse=true;
    return view('reportes.Beneficiospaciente',compact('becas_descuentos','isResponse','pacientes','paciente','inicio'));    
}
public function Casos(){
$reporte="Casos";
return view('reportes.reportesSelect',compact('reporte'));
}

public function Casos_post(Request $request){
$inicio=$request->fecha_ini;
$fin= $request->fecha_fin;
if(auth()->user()->hasRoles(['consul','direc'])){
            if(auth()->user()->hasRoles(['direc'])){
             $con_si=Session::get('con_si',false);
             if($con_si==true){

            $id_consultor=auth()->user()->consultor->id;
                $casos_seguimientos=Pacientes::with('citas','caso','diagnosticos')->whereHas('citas',function($query) use($inicio,$fin,$id_consultor){
      $query->whereBetween('fecha_cita',[$inicio,$fin])->where('consultor_id',$id_consultor);
    })->get();
    
 $casos_diagnosticos=Caso::with('consultor','paciente','diagnostico')->whereBetween('created_at',[$inicio,$fin])->where('consultor_id',$id_consultor)->get();


$disg_sin_seg=Pacientes::with('caso')->whereHas('diagnosticos', 
    function ($query) use ($inicio,$fin,$id_consultor){
        $query->whereBetween('fecha_diagnostico',[$inicio,$fin])->where('consultor_id',$id_consultor);})->doesntHave('seguimientos')->get();
    $inicio=date_create($inicio);
   date_modify($inicio,"-1 day");
    $inicio=date_format($inicio,'Y-m-d H:m:s');
 
   $fin=date_create($fin);
   date_modify($fin,"+1 day");
   $fin=date_format($fin,"Y-m-d H:m:s");
$casos_altas=Caso::with('consultor','paciente','diagnostico')->where([['status','Alta'],['consultor_id',$id_consultor]])->whereBetween('fecha_modificacion',[$inicio,$fin])->get();
}else{
      $casos_seguimientos=Pacientes::with('citas','caso','diagnosticos')->whereHas('citas',function($query) use($inicio,$fin){
      $query->whereBetween('fecha_cita',[$inicio,$fin]);
    })->get();
    
 $casos_diagnosticos=Caso::with('consultor','paciente','diagnostico')->whereBetween('created_at',[$inicio,$fin])->get();


$disg_sin_seg=Pacientes::with('caso')->whereHas('diagnosticos', 
    function ($query) use ($inicio,$fin){
        $query->whereBetween('fecha_diagnostico',[$inicio,$fin]);})->doesntHave('seguimientos')->get();
    $inicio=date_create($inicio);
   date_modify($inicio,"-1 day");
    $inicio=date_format($inicio,'Y-m-d H:m:s');
 
   $fin=date_create($fin);
   date_modify($fin,"+1 day");
   $fin=date_format($fin,"Y-m-d H:m:s");
$casos_altas=Caso::with('consultor','paciente','diagnostico')->where('status','Alta')->whereBetween('fecha_modificacion',[$inicio,$fin])->get();
}
}else{
              $id_consultor=auth()->user()->consultor->id;
                $casos_seguimientos=Pacientes::with('citas','caso','diagnosticos')->whereHas('citas',function($query) use($inicio,$fin,$id_consultor){
      $query->whereBetween('fecha_cita',[$inicio,$fin])->where('consultor_id',$id_consultor);
    })->get();
    
 $casos_diagnosticos=Caso::with('consultor','paciente','diagnostico')->whereBetween('created_at',[$inicio,$fin])->where('consultor_id',$id_consultor)->get();


$disg_sin_seg=Pacientes::with('caso')->whereHas('diagnosticos', 
    function ($query) use ($inicio,$fin,$id_consultor){
        $query->whereBetween('fecha_diagnostico',[$inicio,$fin])->where('consultor_id',$id_consultor);})->doesntHave('seguimientos')->get();
    $inicio=date_create($inicio);
   date_modify($inicio,"-1 day");
    $inicio=date_format($inicio,'Y-m-d H:m:s');
 
   $fin=date_create($fin);
   date_modify($fin,"+1 day");
   $fin=date_format($fin,"Y-m-d H:m:s");
$casos_altas=Caso::with('consultor','paciente','diagnostico')->where([['status','Alta'],['consultor_id',$id_consultor]])->whereBetween('fecha_modificacion',[$inicio,$fin])->get();
}
}else{
      $casos_seguimientos=Pacientes::with('citas','caso','diagnosticos')->whereHas('citas',function($query) use($inicio,$fin){
      $query->whereBetween('fecha_cita',[$inicio,$fin]);
    })->get();
    
 $casos_diagnosticos=Caso::with('consultor','paciente','diagnostico')->whereBetween('created_at',[$inicio,$fin])->get();


$disg_sin_seg=Pacientes::with('caso')->whereHas('diagnosticos', 
    function ($query) use ($inicio,$fin){
        $query->whereBetween('fecha_diagnostico',[$inicio,$fin]);})->doesntHave('seguimientos')->get();
    $inicio=date_create($inicio);
   date_modify($inicio,"-1 day");
    $inicio=date_format($inicio,'Y-m-d H:m:s');
 
   $fin=date_create($fin);
   date_modify($fin,"+1 day");
   $fin=date_format($fin,"Y-m-d H:m:s");
$casos_altas=Caso::with('consultor','paciente','diagnostico')->where('status','Alta')->whereBetween('fecha_modificacion',[$inicio,$fin])->get();
}

$reporte="Casos";
return view('reportes.responseReportes',compact('reporte','casos_seguimientos','casos_diagnosticos','casos_altas','disg_sin_seg'));
}

public function Diagnosticos(){
$reporte="Diagnosticos";
return view('reportes.reportesSelect',compact('reporte','casos_seguimientos','casos_diagnosticos','casos_altas','casos_sin_Diagnostico'));
}

public function Diagnosticos_post(Request $request){
$reporte="Diagnosticos";
}

public function Citas_turno(){
$reporte="Citas_turno";
return view('reportes.reportesSelect',compact('reporte'));
}

public function Citas_turno_post(Request $request){

$reporte="Citas_turno";
$inicio=$request->fecha_ini;
$fin= $request->fecha_fin;
$citas_manana=Citas::where('status_cita_id','!=',2)->whereBetween('fecha_cita',[$inicio,$fin])->whereBetween('hora',['07:00:00','15:30:00'])->get();
$citas_tarde=Citas::where('status_cita_id','!=',2)->whereBetween('fecha_cita',[$inicio,$fin])->whereBetween('hora',['16:00:00','21:00:00'])->where('status_cita_id','!=',2)->get();

return view('reportes.responseReportes',compact('reporte','citas_tarde','citas_manana'));
}

public function Horas_Consultor(){
$reporte="Horas_Consultor";
return view('reportes.reportesSelect',compact('reporte'));
}
public function Horas_Consultor_post(Request $request){
$reporte="Horas_Consultor";
$inicio=$request->fecha_ini;
$fin= $request->fecha_fin;

    $inicio=date_create($inicio);
   date_modify($inicio,"-1 day");
    $inicio=date_format($inicio,'Y-m-d H:m:s');
 
   $fin=date_create($fin);
   date_modify($fin,"+1 day");
   $fin=date_format($fin,"Y-m-d H:m:s");
$status_cita=status_cita::where('descripcion','cancelada')->first();
$consultores=Consultores::with('horario_consultor')->whereHas('citas_rr', function ($query) use ($inicio,$fin,$status_cita) {
      return $query->where('status_cita_id','!=',$status_cita->id)->whereBetween('start',[$inicio,$fin])->orderBy('fecha_cita','ASC');
 })->get();
return view('reportes.responseReportes',compact('reporte','consultores','inicio','fin'));
    
}
public function seguimientosConsultor(){
  $reporte='seguimiento_consultor';
  return view('reportes.reportesSelect',compact('reporte'));
}
public function seguimientosConsultorPost(Request $request){
              $inicio=$request->fecha_ini;
$fin=$request->fecha_fin;
   $inicio_=date_create($inicio);
   date_modify($inicio_,"-1 day");
    $inicio_=date_format($inicio_,'Y-m-d H:m:s');
 
   $fin_=date_create($fin);
   date_modify($fin_,"+1 day");
   $fin_=date_format($fin_,"Y-m-d H:m:s");
          if(auth()->user()->hasRoles(['consul','direc'])){
            if(auth()->user()->hasRoles(['direc'])){
             $con_si=Session::get('con_si',false);
             if($con_si==true){

            $id_consultor=auth()->user()->consultor->id;

$reporte='seguimiento_consultor';
 $consultor_citas=DB::table('citas')->select('consultores.nombrecompleto',DB::raw("count('consultor_id') as citas"),'consultor_id')->join('consultores','citas.consultor_id','=','consultores.id')->whereBetween('fecha_cita',[$inicio,$fin])->where([['status_cita_id','!=',2],['consultor_id',$id_consultor]])->groupBy('consultor_id')->orderBy('consultor_id')->get(); 
 
$pacientes_cita=DB::table('citas')->select('pacientes.nombrecompleto','citas.fecha_cita','consultor_id','citas.paciente_id','citas.ingreso_consultor',DB::raw("citas.id as cita"))->join('consultores','citas.consultor_id','=','consultores.id')->join('pacientes','citas.paciente_id','=','pacientes.id')->whereBetween('fecha_cita',[$inicio,$fin])->where([['status_cita_id','!=',2],['citas.consultor_id',$id_consultor]])->orderBy('consultor_id')->orderBy('paciente_id')->get();
 
 $seguimientos=DB::table('seguimientos')->select('seguimientos.created_at','seguimientos.consultor_id','seguimientos.paciente_id','seguimientos.cita_id')->join('citas','seguimientos.cita_id','=','citas.id')->whereBetween('fecha_cita',[$inicio,$fin])->where([['status_cita_id','!=',2],['seguimientos.consultor_id',$id_consultor],])->orderBy('seguimientos.consultor_id')->orderBy('seguimientos.paciente_id')->get();

 $graficas=DB::table('consultores')->select('consultores.nombrecompleto','consultores.id as erf',DB::raw("(select count(consultor_id)   from citas pa where pa.consultor_id=erf  and (fecha_cita between '".$inicio."' and '".$fin."') and status_cita_id!=2 group by pa.consultor_id) as citass"),DB::raw("(select count(s.consultor_id) from seguimientos s inner join citas on s.cita_id=citas.id where s.consultor_id=erf and (fecha_cita between '".$inicio."' and '".$fin."') and citas.status_cita_id!=2 group by s.consultor_id) as seguimientoss"))->join('citas','consultores.id','=','citas.consultor_id')->where('consultores.id',$id_consultor)->groupBy('consultores.id')->get();


        $result[] = ["Consultores","Citas","Seguimientos"];
        foreach ($graficas as $key => $value) {
          if($value->citass==null){
            $value->citass=0;
          }
          if($value->seguimientoss==null){
            $value->seguimientoss=0;
          }
            $result[++$key] = [$value->nombrecompleto,$value->citass,$value->seguimientoss];
          }
          $json_res = json_encode($result);

return view('reportes.responseReportes',compact('reporte','consultor_citas','pacientes_cita','seguimientos','json_res'));
          }else{
 
$reporte='seguimiento_consultor';
 $consultor_citas=DB::table('citas')->select('consultores.nombrecompleto',DB::raw("count('consultor_id') as citas"),'consultor_id')->join('consultores','citas.consultor_id','=','consultores.id')->whereBetween('fecha_cita',[$inicio,$fin])->where('status_cita_id','!=',2)->groupBy('consultor_id')->orderBy('consultor_id')->get(); 

 $pacientes_cita=DB::table('citas')->select('pacientes.nombrecompleto','citas.fecha_cita','consultor_id','citas.paciente_id','citas.ingreso_consultor',DB::raw("citas.id as cita"))->join('consultores','citas.consultor_id','=','consultores.id')->join('pacientes','citas.paciente_id','=','pacientes.id')->whereBetween('fecha_cita',[$inicio,$fin])->where('status_cita_id','!=',2)->orderBy('consultor_id')->orderBy('paciente_id')->get();
 
 $seguimientos=DB::table('seguimientos')->select('seguimientos.created_at','seguimientos.consultor_id','seguimientos.paciente_id','seguimientos.cita_id')->join('citas','seguimientos.cita_id','=','citas.id')->whereBetween('fecha_cita',[$inicio,$fin])->where('status_cita_id','!=',2)->orderBy('seguimientos.consultor_id')->orderBy('seguimientos.paciente_id')->get();
 
 $graficas=DB::table('consultores')->select('consultores.nombrecompleto','consultores.id as erf',DB::raw("(select count(consultor_id)   from citas pa where pa.consultor_id=erf  and (fecha_cita between '".$inicio."' and '".$fin."') and status_cita_id!=2 group by pa.consultor_id) as citass"),DB::raw("(select count(s.consultor_id) from seguimientos s inner join citas on s.cita_id=citas.id where s.consultor_id=erf and (fecha_cita between '".$inicio."' and '".$fin."') and citas.status_cita_id!=2 group by s.consultor_id) as seguimientoss"))->join('citas','consultores.id','=','citas.consultor_id')->groupBy('consultores.id')->get();


        $result[] = ["Consultores","Citas","Seguimientos"];
        foreach ($graficas as $key => $value) {
          if($value->citass==null){
            $value->citass=0;
          }
          if($value->seguimientoss==null){
            $value->seguimientoss=0;
          }
            $result[++$key] = [$value->nombrecompleto,$value->citass,$value->seguimientoss];
          }
          $json_res = json_encode($result);

return view('reportes.responseReportes',compact('reporte','consultor_citas','pacientes_cita','seguimientos','json_res'));
          } 
            }else{
               $id_consultor=auth()->user()->consultor->id;

$reporte='seguimiento_consultor';
 $consultor_citas=DB::table('citas')->select('consultores.nombrecompleto',DB::raw("count('consultor_id') as citas"),'consultor_id')->join('consultores','citas.consultor_id','=','consultores.id')->whereBetween('fecha_cita',[$inicio,$fin])->where([['status_cita_id','!=',2],['consultor_id',$id_consultor]])->groupBy('consultor_id')->orderBy('consultor_id')->get(); 
  
$pacientes_cita=DB::table('citas')->select('pacientes.nombrecompleto','citas.fecha_cita','consultor_id','citas.paciente_id','citas.ingreso_consultor',DB::raw("citas.id as cita"))->join('consultores','citas.consultor_id','=','consultores.id')->join('pacientes','citas.paciente_id','=','pacientes.id')->whereBetween('fecha_cita',[$inicio,$fin])->where([['status_cita_id','!=',2],['citas.consultor_id',$id_consultor]])->orderBy('consultor_id')->orderBy('paciente_id')->get();
 
 $seguimientos=DB::table('seguimientos')->select('seguimientos.created_at','seguimientos.consultor_id','seguimientos.paciente_id','seguimientos.cita_id')->join('citas','seguimientos.cita_id','=','citas.id')->whereBetween('fecha_cita',[$inicio,$fin])->where([['status_cita_id','!=',2],['seguimientos.consultor_id',$id_consultor],])->orderBy('seguimientos.consultor_id')->orderBy('seguimientos.paciente_id')->get();
 
 $graficas=DB::table('consultores')->select('consultores.nombrecompleto','consultores.id as erf',DB::raw("(select count(consultor_id)   from citas pa where pa.consultor_id=erf  and (fecha_cita between '".$inicio."' and '".$fin."') and status_cita_id!=2 group by pa.consultor_id) as citass"),DB::raw("(select count(s.consultor_id) from seguimientos s inner join citas on s.cita_id=citas.id where s.consultor_id=erf and (fecha_cita between '".$inicio."' and '".$fin."') and citas.status_cita_id!=2 group by s.consultor_id) as seguimientoss"))->join('citas','consultores.id','=','citas.consultor_id')->where('consultores.id',$id_consultor)->groupBy('consultores.id')->get();


        $result[] = ["Consultores","Citas","Seguimientos"];
        foreach ($graficas as $key => $value) {
          if($value->citass==null){
            $value->citass=0;
          }
          if($value->seguimientoss==null){
            $value->seguimientoss=0;
          }
            $result[++$key] = [$value->nombrecompleto,$value->citass,$value->seguimientoss];
          }
          $json_res = json_encode($result);

return view('reportes.responseReportes',compact('reporte','consultor_citas','pacientes_cita','seguimientos','json_res'));
            }
          }else{



$reporte='seguimiento_consultor';
 $consultor_citas=DB::table('citas')->select('consultores.nombrecompleto',DB::raw("count('consultor_id') as citas"),'consultor_id')->join('consultores','citas.consultor_id','=','consultores.id')->whereBetween('fecha_cita',[$inicio,$fin])->where('status_cita_id','!=',2)->groupBy('consultor_id')->orderBy('consultor_id')->get(); 

 $pacientes_cita=DB::table('citas')->select('pacientes.nombrecompleto','citas.fecha_cita','consultor_id','citas.paciente_id','citas.ingreso_consultor',DB::raw("citas.id as cita"))->join('consultores','citas.consultor_id','=','consultores.id')->join('pacientes','citas.paciente_id','=','pacientes.id')->whereBetween('fecha_cita',[$inicio,$fin])->where('status_cita_id','!=',2)->orderBy('consultor_id')->orderBy('paciente_id')->get();
 
 $seguimientos=DB::table('seguimientos')->select('seguimientos.created_at','seguimientos.consultor_id','seguimientos.paciente_id','seguimientos.cita_id')->join('citas','seguimientos.cita_id','=','citas.id')->whereBetween('fecha_cita',[$inicio,$fin])->where('status_cita_id','!=',2)->orderBy('seguimientos.consultor_id')->orderBy('seguimientos.paciente_id')->get();
 
 $graficas=DB::table('consultores')->select('consultores.nombrecompleto','consultores.id as erf',DB::raw("(select count(consultor_id)   from citas pa where pa.consultor_id=erf  and (fecha_cita between '".$inicio."' and '".$fin."') and status_cita_id!=2 group by pa.consultor_id) as citass"),DB::raw("(select count(s.consultor_id) from seguimientos s inner join citas on s.cita_id=citas.id where s.consultor_id=erf and (fecha_cita between '".$inicio."' and '".$fin."') and citas.status_cita_id!=2 group by s.consultor_id) as seguimientoss"))->join('citas','consultores.id','=','citas.consultor_id')->groupBy('consultores.id')->get();


        $result[] = ["Consultores","Citas","Seguimientos"];
        foreach ($graficas as $key => $value) {
          if($value->citass==null){
            $value->citass=0;
          }
          if($value->seguimientoss==null){
            $value->seguimientoss=0;
          }
            $result[++$key] = [$value->nombrecompleto,$value->citass,$value->seguimientoss];
          }
          $json_res = json_encode($result);

return view('reportes.responseReportes',compact('reporte','consultor_citas','pacientes_cita','seguimientos','json_res'));

}

}
public function citas_sin_enviar(){
  $citas->Citas::where([['consultor_id',17],['fecha_cita','2019-09-27']]);
  
  //dd($citas);
}

}
