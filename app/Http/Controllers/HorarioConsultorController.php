<?php

namespace App\Http\Controllers;

use App\horario_consultor;
use Illuminate\Http\Request;

class HorarioConsultorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\horario_consultor  $horario_consultor
     * @return \Illuminate\Http\Response
     */
    public function show(horario_consultor $horario_consultor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\horario_consultor  $horario_consultor
     * @return \Illuminate\Http\Response
     */
    public function edit(horario_consultor $horario_consultor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\horario_consultor  $horario_consultor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, horario_consultor $horario_consultor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\horario_consultor  $horario_consultor
     * @return \Illuminate\Http\Response
     */
    public function destroy(horario_consultor $horario_consultor)
    {
        //
    }
}
