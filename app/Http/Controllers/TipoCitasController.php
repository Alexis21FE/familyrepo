<?php

namespace App\Http\Controllers;

use App\Tipo_citas;
use Illuminate\Http\Request;

class TipoCitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tipo_citas  $tipo_citas
     * @return \Illuminate\Http\Response
     */
    public function show(Tipo_citas $tipo_citas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tipo_citas  $tipo_citas
     * @return \Illuminate\Http\Response
     */
    public function edit(Tipo_citas $tipo_citas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tipo_citas  $tipo_citas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tipo_citas $tipo_citas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tipo_citas  $tipo_citas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tipo_citas $tipo_citas)
    {
        //
    }
}
