<?php

namespace App\Http\Controllers;

use App\consultor_categoria;
use Illuminate\Http\Request;

class ConsultorCategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\consultor_categoria  $consultor_categoria
     * @return \Illuminate\Http\Response
     */
    public function show(consultor_categoria $consultor_categoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\consultor_categoria  $consultor_categoria
     * @return \Illuminate\Http\Response
     */
    public function edit(consultor_categoria $consultor_categoria)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\consultor_categoria  $consultor_categoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, consultor_categoria $consultor_categoria)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\consultor_categoria  $consultor_categoria
     * @return \Illuminate\Http\Response
     */
    public function destroy(consultor_categoria $consultor_categoria)
    {
        //
    }
}
