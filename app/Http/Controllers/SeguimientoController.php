<?php

namespace App\Http\Controllers;

use App\seguimiento;
use Illuminate\Http\Request;

class SeguimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\seguimiento  $seguimiento
     * @return \Illuminate\Http\Response
     */
    public function show(seguimiento $seguimiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\seguimiento  $seguimiento
     * @return \Illuminate\Http\Response
     */
    public function edit(seguimiento $seguimiento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\seguimiento  $seguimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, seguimiento $seguimiento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\seguimiento  $seguimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(seguimiento $seguimiento)
    {
        //
    }
}
